/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { ipcMain } = require('electron');

class ProgressDialog {
  constructor(window) {
    this._window = window;
    this._isDialogReady = false;
    this._summary = '';
    this._detail = '';
    this._progress = 0;
    this._isComplete = false;
    this._isFailed = false;
    if (!this._window.isDestroyed()) {
      this._window.setProgressBar(2); // Set progress to indeterminate initially
    }

    // It can take a moment for react and the progress dialog to load.
    // The dialog is coded to emit a message whenever it is ready.
    // Once it is loaded, we send it the current state of the dialog.
    ipcMain.once('progress-dialog-ready', () => {
      // Once the progress bar is ready, resend all of the IPC messages.
      this._isDialogReady = true;
      this.summary = this._summary;
      this.detail = this._detail;
      this.progress = this._progress;
      this.isFailed = this._isFailed;
      this.isComplete = this._isComplete;
    });
  }

  get summary() {
    return this._summary;
  }

  set summary(value) {
    this._summary = value;
    if (this._isDialogReady && !this._window.isDestroyed()) {
      this._window.webContents.send('progress-dialog-summary', {
        value: value
      });
    }
  }

  get detail() {
    return this._detail;
  }

  set detail(value) {
    this._detail = value;
    if (this._isDialogReady && !this._window.isDestroyed()) {
      this._window.webContents.send('progress-dialog-detail', { value: value });
    }
  }

  get progress() {
    return this._progress;
  }

  set progress(percent) {
    this._progress = percent;
    if (this._window.isDestroyed()) {
      return;
    }
    this._window.setProgressBar(percent / 100.0, {
      mode: this._isFailed ? 'error' : 'normal'
    }); // Sets progress bar in task bar
    if (this._isDialogReady) {
      this._window.webContents.send('progress-dialog-progress', {
        percent: percent
      });
    }
  }

  get isComplete() {
    return this._isComplete;
  }

  set isComplete(value) {
    this._isComplete = value;
    if (this._isDialogReady && !this._window.isDestroyed()) {
      this._window.webContents.send('progress-dialog-complete', {
        isComplete: value
      });
    }
  }

  get isFailed() {
    return this._isFailed;
  }

  set isFailed(value) {
    this._isFailed = value;
    if (this._isDialogReady && !this._window.isDestroyed()) {
      this._window.webContents.send('progress-dialog-failed', {
        isFailed: value
      });
    }
  }

  close() {
    if (!this._window.isDestroyed()) {
      this._window.removeAllListeners('close');
      this._window.close();
      this._window.destroy();
    }
  }
}

module.exports = ProgressDialog;
