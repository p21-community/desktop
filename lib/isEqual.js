/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { difference } = require('lodash');

/**
 * Performs a deep comparison between two values to determine if they are
 * equal. This comparison does not care what order the values appear in when
 * comparing arrays.
 *
 * Example: isEqual([1, 2], [2, 1]) returns true
 *
 * @param value The value to compare
 * @param other The other value to compare
 * @returns {boolean}
 */
function isEqual(value, other) {
  if (value === other) {
    return true;
  }

  if (typeof value === typeof other) {
    if (['string', 'number', 'boolean'].includes(typeof value)) {
      return false;
    }

    if (Object.keys(value).length === Object.keys(other).length) {
      if (difference(Object.keys(value), Object.keys(other))) {
        xLoop: for (const propX in value) {
          let isEq = false;
          if (value.hasOwnProperty(propX)) {
            for (const propY in other) {
              if (
                other.hasOwnProperty(propY) &&
                isEqual(value[propX], other[propY])
              ) {
                isEq = true;
                continue xLoop;
              }
            }
          }
          if (!isEq) {
            return false;
          }
        }
        return true;
      }
    }
  }
  return false;
}

module.exports = isEqual;
