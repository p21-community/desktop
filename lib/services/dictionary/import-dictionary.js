/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const fs = require('fs-extra');
const path = require('path');
const del = require('del');
const { DuplicateDictionaryError } = require('./duplicate-dictionary-error');

const importDictionary = async (src, dest, type, shouldOverwrite) => {
  const destExists = await fs.pathExists(dest);
  if (destExists) {
    if (!shouldOverwrite) {
      throw new DuplicateDictionaryError();
    }
    await del([dest], { force: true });
  }
  await fs.mkdirp(dest, { mode: 0o2777 });
  await copyDictionaryAsync(src, dest, type);
};

const copyDictionaryAsync = async (src, dest, type) => {
  const stats = await fs.stat(src);
  if (stats.isDirectory()) {
    await fs.copy(src, dest);
  } else {
    const extension = path.extname(src);
    const baseName = path.basename(src, extension);
    const fileName = type === 'SNOMED' ? `${baseName}-source${extension}` : `${baseName}${extension}`;
    const newFilePath = path.join(dest, fileName);
    await fs.copyFile(src, newFilePath);
  }
};

module.exports = { importDictionary };
