/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { ipcMain, webContents, app } = require('electron');
const JWTDecode = require('jwt-decode');
const Transport = require('winston-transport');
const axios = require('axios');
const Settings = require('../settings');

module.exports = class RemoteLoggingTransport extends Transport {
  constructor(opts) {
    super(opts);
  }

  getWebContents() {
    if (this.webcontents) {
      return this.webcontents;
    }
    const windows = webContents.getAllWebContents();
    for (let window of windows) {
      const windowOptions = window.browserWindowOptions;
      if (
        windowOptions &&
        windowOptions.webPreferences &&
        windowOptions.webPreferences.preload
      ) {
        if (windowOptions.webPreferences.preload.indexOf('preload.js') > -1) {
          this.windowcontents = window;
          break;
        }
      }
    }
    return this.windowcontents;
  }

  getMeta() {
    return new Promise(resolve => {
      const meta = {};
      ipcMain.once('preferences', (event, data) => {
        meta.preferences = data;
        // Try to get tool parameters for additional
        // info in bug report. If the listener doesn't respond after 500 ms,
        // likely a tool wasn't running and the parameters are insignificant
        const toolInfoPromise = new Promise(resolve => {
          const lastLocation = event.sender.history.pop();
          if (
            lastLocation &&
            (lastLocation.endsWith('validator') ||
            lastLocation.endsWith('converter') ||
            lastLocation.endsWith('miner'))
          ) {
            // Capture tool parameters
            ipcMain.once('toolParameters', (event, tool, parameters) => {
              resolve({ tool, parameters });
            });
            this.getWebContents().send('getToolParameters');
          } else {
            resolve();
          }
        });
        const wait500msPromise = new Promise(resolve => {
          let wait = setTimeout(() => {
            clearTimeout(wait);
            resolve();
          }, 500);
        });
        Promise.race([toolInfoPromise, wait500msPromise]).then(toolInfo => {
          if (toolInfo) {
            meta.tool = toolInfo.tool;
            meta.params = toolInfo.parameters;
          }
          resolve(meta);
        }).catch(e => {
          console.error(e);
          resolve(meta);
        });
      });
      this.getWebContents().send('getPreferences');
    });
  }

  getApplicationPrefs(info, meta) {
    if (meta && meta.preferences) {
      return meta.preferences;
    } else if (info && info['Application Preferences']) {
      return info['Application Preferences'];
    }
    return null;
  }

  getErrorId(info) {
    function rebuildJavaStackTrace(stackTraceArr) {
      if (stackTraceArr && stackTraceArr.length) {
        return stackTraceArr
          .reduce((acc, current) => {
            return (
              acc +
              `at ${current.className}(${current.fileName}:${
                current.lineNumber
              })\n`
            );
          }, '')
          .trim();
      }
      return '';
    }
    function computeErrorId(message) {
      const regex = /(at*.*\()|(\w+\.js:\d+.*\d+\))|(\w+\.java:\d+\))/gm;
      const matches = message.match(regex);
      const hasher = require('crypto').createHash('md5');
      if (matches && matches.length) {
        return hasher.update(matches.join('')).digest('hex');
      }
      return hasher.update(message).digest('hex');
    }

    try {
      if (info.errorId) {
        return info.errorId;
      } else if (info.stack) {
        return computeErrorId(info.stack);
      } else if (info.response) {
        // failed web request
        const url = info.response.config.url;
        const message = info.message;
        return computeErrorId(url + message);
      } else if (info.message) {
        if (typeof info.message === 'object') {
          const target = info.message.target;
          if (target && target.cause && target.cause.stackTrace) {
            return computeErrorId(rebuildJavaStackTrace(target.cause.stackTrace));
          } else if (info.message.error && info.message.error.code) {
            return computeErrorId(info.message.error.code);
          } else if (info.message.label) {
            return computeErrorId(info.message.label);
          } else {
            return computeErrorId(JSON.stringify(info.message));
          }
        } else {
          return computeErrorId(info.message);
        }
      }
    } catch (e) {
      console.error(e);
      return null;
    }
  }

  getMessage(info, meta) {
    if (meta && meta.params) {
      return { ...info, parameters: meta.params };
    } else if (info && typeof info === 'object') {
        return info;
    } else {
      return info;
    }
  }

  getTool(info, meta) {
    if (meta && meta.tool) {
      return meta.tool;
    } else if (info && info.tool) {
      return info.tool;
    }
    return null;
  }

  getUserId(preferences) {
    // Check if they have a Pinnacle ID
    if (preferences && preferences.identity) {
      try {
        const decodedIdentity = JWTDecode(preferences.identity.idToken);
        if (decodedIdentity) {
          return decodedIdentity.sub;
        }
      } catch (e) {
        console.log(
          `Failed to decode JWT token: '${preferences.identity.idToken}'.`
        );
      }
    }
    return null;
  }

  getAppId() {
    try {
      if (this.appId) {
        return Promise.resolve(this.appId);
      }
      return new Promise(resolve => {
        ipcMain.once('appId', (event, data) => {
          this.appId = data;
          resolve(data);
        });
        this.getWebContents().send('getAppId');
      });
    } catch (e) {
      console.error(e);
      return Promise.resolve(null);
    }
  }

  getVersion() {
    if (!this.version) {
      this.version = app.getVersion();
    }
    return this.version;
  }

  getService() {
    const version = this.getVersion();
    if (version.indexOf('snap') > -1) {
      return 'community-snapshot';
    } else if (version.indexOf('pre') > -1) {
      return 'community-pre-release';
    } else {
      return 'community-release';
    }
  };

  logRemote(dataObj) {
    if (!this.ws) {
      let baseURL;
      if (
        this.getVersion().indexOf('snap') > -1 ||
        this.getVersion().indexOf('pre') > -1
      ) {
        baseURL = 'https://p3pn4zxuhf.execute-api.us-east-1.amazonaws.com/test';
      } else {
        baseURL = 'https://ws.pinnacle21.com/community';
      }
      this.ws = axios.create({
        baseURL,
        timeout: 5000,
        headers: { 'Content-Type': 'application/javascript' }
      });
    }
    return this.ws.post('/log', dataObj);
  }

  async log(info, callback) {
    setImmediate(() => {
      this.emit('logged', info);
    });

    try {
      const meta = await this.getMeta();
      const appPrefs = this.getApplicationPrefs(info, meta);

      const logData = {
        errorId: this.getErrorId(info),
        message: this.getMessage(info, meta),
        action: null,
        tool: this.getTool(info, meta),
        pinnacleId: this.getUserId(appPrefs),
        appId: await this.getAppId(),
        version: this.getVersion(),
        service: this.getService()
      };

      if (Settings.isDevelopment()) {
        console.log('IN DEV MODE: Not logging error to remote');
        console.log(JSON.stringify(logData));
      } else {
        await this.logRemote(logData);
      }
    } catch (e) {
      console.error('Error while attempting to do remote logging: ', e);
    }

    callback();
  }
};
