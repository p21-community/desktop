/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const winston = require('winston');
require('winston-daily-rotate-file');
const RemoteLoggingTransport = require('./remote-logger-winston-transport');
const moment = require('moment');
const path = require('path');
const { combine, timestamp, printf } = winston.format;
const Settings = require('../settings');

let fileName = '';
const exportPath = Settings.getExportPath();
const rollingTransport = new winston.transports.DailyRotateFile({
  dirname: path.join(exportPath, 'logs'),
  filename: 'p21community-%DATE%.log',
  datePattern: 'YYYY-MM-DD',
  level: 'error',
  maxFiles: '30' // Keep the last 30 (should equate to about a month worth of logs)
});

rollingTransport.on('rotate', function(oldFilename, newFilename) {
  fileName = path.basename(newFilename);
});

fileName = rollingTransport.options.filename.replace(
  '%DATE%',
  moment().format(rollingTransport.options.datePattern)
);

const logger = winston.createLogger({
  format: combine(
    timestamp(),
    printf(info => {
      if (!info.message) {
        return `[${info.timestamp}] [${info.level}] An unknown error occurred.`;
      }
      let message = {};
      if (typeof info.message === 'object') {
        if (info.message.stack) {
          message = info.message.stack;
        } else if (info.message.label) {
          message.label = info.message.label;
          message.cause =
            info.message.target && info.message.target.cause
              ? info.message.target.cause
              : undefined;
          message = JSON.stringify(message);
        }
      } else {
        message = info.message.toString();
      }
      return `[${info.timestamp}] [${info.level}] ${message.replace(
        /\n/g,
        '\r\n'
      )}`;
    })
  ),
  transports: [
    rollingTransport,
    new winston.transports.Console({ level: 'info' }),
    new RemoteLoggingTransport({ level: 'error' })
  ]
});

module.exports = logger;
module.exports.fileName = () => fileName;
