/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { ipcMain } = require('electron');
const moment = require('moment');
const winston = require('../services/logger');
const path = require('path');
const checkInternetConnection = require('./online-check');

const { version } = require(path.join(
  path.dirname(require.main.filename),
  'package.json'
));

const SNAPSHOT = 'snapshot';
const PRE_RELEASE = 'pre-release';
const RELEASE = 'release';

const releaseLevel = version.includes(SNAPSHOT)
  ? SNAPSHOT
  : version.includes(PRE_RELEASE)
  ? PRE_RELEASE
  : RELEASE;

module.exports = async s3 => {
  const getLatestNotifications = async () => {
    return new Promise(async resolve => {
      try {
        if (await checkInternetConnection()) {
          s3.makeUnauthenticatedRequest(
            'getObject',
            {
              Bucket: 'p21-community-resources',
              Key: path.posix.join(releaseLevel, 'notifications.json')
            },
            (err, data) => {
              if (err) {
                winston.error(err);
              } else {
                const {
                  bannerNotifications,
                  standardNotifications
                } = JSON.parse(data.Body.toString('utf8'));

                resolve({
                  bannerNotifications: bannerNotifications
                    .slice(0, 2)
                    .filter(
                      ({ expiration_date }) =>
                        !expiration_date || moment(expiration_date).isAfter()
                    ),
                  standardNotifications: standardNotifications.slice(0, 5)
                });
              }
            }
          );
        }
      } catch (err) {
        winston.error(err);
      }
    });
  };

  ipcMain.on('getLatestNotifications', async event => {
    if (!event.sender.isDestroyed()) {
      event.sender.send('notifications', await getLatestNotifications());
    }
  });

  return getLatestNotifications();
};
