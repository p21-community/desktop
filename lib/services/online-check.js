/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const axios = require('axios');
const Settings = require('../settings');

const checkInternetConnection = async () => {
  try {
    const url =
      Settings.isProduction() && Settings.isRelease()
        ? 'https://ws.pinnacle21.com/community/ping'
        : 'https://p3pn4zxuhf.execute-api.us-east-1.amazonaws.com/test/ping';
    await axios.get(url, { timeout: 10000 });
    return true;
  } catch (err) {
    return false;
  }
};

module.exports = checkInternetConnection;
