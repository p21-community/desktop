/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const os = require('os');
const path = require('path');
const { app, ipcMain } = require('electron');
const winattr = require('winattr');

const isEqual = require('../isEqual');
const ListConfig = require('./cli/jobs/ListConfig');
const createConfigJson = require('../create-config-json');
const { isEmpty } = require('lodash');
const { promisify } = require('util');

const fs = require('fs-extra');

const filterOutLocalDictionaries = dictionaries =>
  dictionaries.filter(({ name }) => !['MedDRA', 'SNOMED'].includes(name));

const formatConfigs = (configs, customConfigs) => ({
  ...['SDTM', 'ADaM', 'SEND'].reduce((acc, standard) => {
    acc[standard] = configs.filter(({ displayName }) =>
      displayName.includes(standard)
    );
    if (customConfigs && customConfigs.length) {
      acc[standard].push(...customConfigs);
    }
    return acc;
  }, {}),
  'Define-XML': [
    ...configs.filter(({ displayName }) => displayName.includes('Define')),
    ...(customConfigs || [])
  ]
});

const parseConfigs = configs =>
  configs.sort().map(config => ({
    fileName: config,
    displayName: config.replace(/\.xml/g, '')
  }));

const sortEngines = engines =>
  engines.sort((engine1, engine2) =>
    engine1.version === engine2.version
      ? engine1.name > engine2.name
      : engine2.version > engine1.version
  );

class Config {
  constructor(CLI, win, configJson, logger) {
    this.CLI = CLI;
    this.win = win;
    this.libDir = path.join(this.CLI.PATH.COMPONENTS, 'lib');

    this.json = configJson;
    this.logger = logger;
  }

  log(type, msg) {
    if (this.logger) {
      this.logger[type](msg);
    }
  }

  async _makeDotFilesHiddenOnWindows(dirPath) {
    try {
      if (os.platform() === 'win32') {
        const files = await fs.readdir(dirPath);
        for (const file of files) {
          const filePath = path.join(dirPath, file);
          const stats = await fs.stat(filePath);
          if (stats.isDirectory()) {
            await this._makeDotFilesHiddenOnWindows(filePath);
          } else {
            if (file.startsWith('.') || file.endsWith('.eTag')) {
              await promisify(winattr.set)(filePath, { hidden: true });
            }
          }
        }
      }
    } catch (err) {
      this.log('error', err);
    }
  }

  async _copyFromLibTo(libDir, cliPath) {
    await fs.mkdirp(cliPath, {
      mode: 0o2777
    });
    const sourcePath = path.join(this.libDir, libDir);
    if (sourcePath !== cliPath) {
      await fs.copy(sourcePath, cliPath);
    } else {
      this.logger.error(`Copy paths are the same: ${sourcePath}, ${cliPath}; appPath: ${app.getAppPath()}`);
    }
    await this._makeDotFilesHiddenOnWindows(cliPath);
  }

  _getDictionaryVersions(dictionaryName, sortType) {
    if (!Object.keys(this.json).length) {
      return [];
    }

    let dictionary = this.json.dictionaries.find(
      ({ name }) => name === dictionaryName
    );

    if (!dictionary) {
      return [];
    }

    const { versions } = dictionary;
    return sortType === 'numeric'
      ? versions.sort((a, b) => b - a)
      : versions.sort().reverse();
  }

  get dictionaries() {
    return {
      cdiscCTVersions: {
        SDTM: this._getDictionaryVersions('SDTM'),
        ADaM: this._getDictionaryVersions('ADaM'),
        SEND: this._getDictionaryVersions('SEND')
      },
      medDRAVersions: this._getDictionaryVersions('MedDRA', 'numeric'),
      SNOMEDVersions: this._getDictionaryVersions('SNOMED'),
      UNIIVersions: this._getDictionaryVersions('UNII'),
      NDFRTVersions: this._getDictionaryVersions('NDF-RT')
    };
  }

  get engines() {
    return isEmpty(this.json) ? [] : sortEngines(this.json.engines);
  }

  get formattedConfigs() {
    return this.engines.reduce((acc, { name, configs, customConfigs }) => {
      const custom = parseConfigs(customConfigs);
      custom.forEach(conf => conf.custom = true);
      acc[name] = formatConfigs(
        parseConfigs(configs),
        custom
      );
      return acc;
    }, {});
  }

  /** This should exclude custom configs */
  get uniqueConfigs() {
    return this.engines
      .reduce((acc, { version, configs, customConfigs }) => {
        acc.push(
          ...configs
            .filter(config => !customConfigs.includes(config))
            .map(config => ({
              fileName: path.join(version, config),
              displayName: config
                .replace(/\(.*\)/, '')
                .replace(/.xml/g, '')
                .trim()
            }))
        );
        return acc;
      }, [])
      .sort((c1, c2) => c1.displayName.localeCompare(c2.displayName, 'en-u-kn'))
      .filter(
        (config, i, arr) =>
          !config.displayName.includes('Define') &&
          arr.findIndex(c => c.displayName === config.displayName) === i
      );
  }

  configChanged(newConfig) {
    return (
      !Object.keys(this.json).length ||
      !isEqual(
        {
          ...this.json,
          dictionaries: filterOutLocalDictionaries(this.json.dictionaries)
        },
        {
          ...newConfig,
          dictionaries: filterOutLocalDictionaries(newConfig.dictionaries)
        }
      )
    );
  }

  _appWasUpdated() {
    if (this.win.isDestroyed()) {
      return Promise.resolve(false);
    }
    return new Promise(resolve => {
      const APP_VERSION = 'app-version';
      ipcMain.once(APP_VERSION, (event, appVersion) => {
        const appWasUpdated = appVersion !== app.getVersion();
        if (appWasUpdated) {
          if (!this.win.isDestroyed()) {
            this.win.webContents.send(
              'save-to-local-storage',
              APP_VERSION,
              app.getVersion()
            );
          }
        }
        resolve(appWasUpdated);
      });
      this.win.webContents.send('get-from-local-storage', APP_VERSION);
    });
  }

  async _fetchLocalConfig() {
    try {
      const generatedJson = await createConfigJson(
        this.CLI.PATH.CONFIGS,
        this.CLI.PATH.ENGINES
      );
      this.json = generatedJson;
      await this._saveToLocalStorage(this.json);
      return this.configChanged(generatedJson);
    } catch (err) {
      this.log('error', err);
      return false;
    }
  }

  async _fetchRemoteConfig(preferences) {
    try {
      const downloadedJson = await ListConfig(this.CLI, preferences);
      this.json = downloadedJson;
      await this._saveToLocalStorage(this.json);
      return this.configChanged(downloadedJson);
    } catch (err) {
      this.log('error', err);
      return false;
    }
  }

  async fetchConfig(isLoggedIn, preferences) {
    const appWasUpdated = await this._appWasUpdated();
    const configsNeedToBeCopied = async () => {
      const folderIsEmpty = async () => {
        try {
          let files = await fs.readdir(this.CLI.PATH.CONFIGS);
          files = files.filter(file => !file.startsWith('.') && file !== 'config.json');
          return !files.length;
        } catch (e) {
          return true;
        }
      };
      return (
        isEmpty(this.json) || (appWasUpdated && isLoggedIn) || await folderIsEmpty()
      );
    };
    const enginesNeedToBeCopied = async () => {
      const folderIsEmpty = async () => {
        try {
          const files = await fs.readdir(this.CLI.PATH.ENGINES);
          let versionsExist = false;
          let modulesExist = false;
          for (const file of files) {
            if (file === 'versions') {
              versionsExist = true;
            }
            if (file === 'modules') {
              modulesExist = true;
            }
          }
          return !(versionsExist && modulesExist);
        } catch (e) {
          return true;
        }
      };
      return appWasUpdated || await folderIsEmpty();
    };
    if (await configsNeedToBeCopied()) {
      await this._copyFromLibTo('configs', this.CLI.PATH.CONFIGS);
    }
    if (await enginesNeedToBeCopied()) {
      await this._copyFromLibTo('engines', this.CLI.PATH.ENGINES);
    }
    try {
      return await this._fetchRemoteConfig(preferences);
    } catch (err) {
      this.log('error', err);
      return await this._fetchLocalConfig();
    }
  }

  _saveToLocalStorage(configsJson) {
    if (this.win.isDestroyed()) {
      return Promise.resolve();
    }
    return new Promise(resolve => {
      ipcMain.on('saved-configs', resolve);
      this.win.webContents.send(
        'save-to-local-storage',
        'configs',
        configsJson
      );
    });
  }
}

module.exports = { Config };
