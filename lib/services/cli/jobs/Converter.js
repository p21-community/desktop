/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

module.exports = CLI => {
  const { ipcMain } = require('electron');
  const winston = require('../../logger');

  ipcMain.removeAllListeners('convert');
  ipcMain.on('convert', (event, obj) => {
    const ipcStatus = {
      text: 'Starting'
    };

    const {
      definePath,
      preferences,
      outputFormat,
      outputPath,
      paths,
      sourceFormat
    } = obj;
    CLI.start(
      preferences,
      JSON.stringify({
        action: 'CONVERT_DATA',
        sources: {
          paths,
          type: sourceFormat
        },
        define: definePath,
        type: outputFormat,
        target: outputPath
      })
    )
      .on('TASK_PROCESSING', message => {
        const total = message.total;
        const current = message.current;
        const ds = message.label.split('->', 1);
        ipcStatus.progress = Math.min(100 * (current / total), 99);
        ipcStatus.text = `
                    Converting ${ds} (dataset ${current} of ${total})
                    `;
        if (!event.sender.isDestroyed()) {
          event.sender.send('status', ipcStatus);
        }
      })
      .on('COMPLETED', message => {
        const response = message.target;
        ipcStatus.progress = 100;
        ipcStatus.isDone = true;
        ipcStatus.target = response.outputPath;

        if (!!response.truncation) {
          ipcStatus.text = `Complete! ${response.truncation}`;
        } else {
          ipcStatus.text = `Complete!`;
        }

        setTimeout(() => {
          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
        }, 3000);
      })
      .on('EXCEPTION', message => {
        const response = message.target;

        winston.error(message);
        ipcStatus.text = `Error code: ${response.code} message: ${
          response.message
        }`;
        ipcStatus.isError = true;
        ipcStatus.progress = 100;
        ipcStatus.exception = response;
        ipcStatus.parameters = obj;
        if (!event.sender.isDestroyed()) {
          event.sender.send('status', ipcStatus);
        }
      });
  });
};
