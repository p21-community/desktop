/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

module.exports = CLI => {
  const path = require('path');
  const { ipcMain } = require('electron');
  const winston = require('../../logger');

  ipcMain.removeAllListeners('generateExcelSpec');
  ipcMain.on(
    'generateExcelSpec',
    (event, { paths, config, preferences, type }) => {
      const ipcStatus = {
        text: 'Starting'
      };
      CLI.start(
        preferences,
        JSON.stringify({
          action: 'CREATE_SPEC',
          sources: {
            paths,
            type
          },
          config: config == null ? null : path.join(CLI.PATH.CONFIGS, config),
          engineVersion:
            config == null ? null : path.basename(path.dirname(config)),
          generate: 'spec',
          target: CLI.PATH.DEFINES
        })
      )
        .on('STARTED', () => {
          if (type === 'Define') {
            ipcStatus.progress = 100;
            ipcStatus.text = 'Parsing define.xml';
          }
          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
        })
        .on('TASK_PROCESSING', message => {
          const total = message.total;
          const current = message.current;
          const labels = message.label.split('->', 2);

          ipcStatus.text =
            type === 'SasTransport'
              ? `${labels[0]}: ${labels[1]} (dataset ${current} of ${total})`
              : `${labels[1]}`;

          ipcStatus.progress = Math.min(100 * (current / total), 99);

          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
        })
        .on('COMPLETED', message => {
          const response = message.target;
          ipcStatus.progress = 100;
          ipcStatus.isDone = true;
          ipcStatus.target = response.outputPath;

          ipcStatus.text = `Complete!`;

          setTimeout(() => {
            if (!event.sender.isDestroyed()) {
              event.sender.send('status', ipcStatus);
            }
          }, 3000);
        })
        .on('EXCEPTION', message => {
          const response = message.target;

          winston.error(message);
          ipcStatus.text = `Error code: ${response.code} message: ${
            response.message
          }`;
          ipcStatus.isError = true;
          ipcStatus.progress = 100;
          ipcStatus.exception = response;
          ipcStatus.parameters = {
            paths,
            config,
            preferences,
            type
          };

          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
        });
    }
  );
};
