/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const path = require('path');
const { ipcMain } = require('electron');
const winston = require('../../logger');

module.exports = CLI => {
  const getTime = ms => {
    const totalSeconds = Math.floor(ms / 1000),
      totalMinutes = Math.floor(totalSeconds / 60),
      hours = Math.floor(totalMinutes / 60),
      minutes = totalMinutes - hours * 60,
      seconds = totalSeconds - minutes * 60;

    if (hours !== 0) {
      return `${hours} hour(s), ${minutes} minute(s) and ${seconds} second(s)`;
    } else if (minutes !== 0) {
      return `${minutes} minute(s) and ${seconds} second(s)`;
    } else if (seconds !== 0) {
      return `${seconds} second(s)`;
    } else {
      return 'less than a second';
    }
  };

  ipcMain.removeAllListeners('validate');
  ipcMain.on(
    'validate',
    (
      event,
      {
        cdiscCT,
        config,
        define,
        defineStandard,
        delimiter,
        dictionary,
        engineVersion,
        isFromDataPackage,
        qualifier,
        paths,
        preferences,
        sourceFormatName,
        standard,
        isCustom
      }
    ) => {
      const standardIsSDTM = standard === 'SDTM';
      const standardIsSEND = standard === 'SEND';
      const standardIsADaM = standard === 'ADaM';
      const standardIsDefine = standard === 'Define-XML';

      const configFolder = isCustom ? 'custom' : engineVersion;
      const configPath = path.join(CLI.PATH.CONFIGS, configFolder, config);

      const parameters = {
        isDefine: standardIsDefine,
        action: 'VALIDATE_DATA',
        threads: Math.max(
          Math.min(Math.floor(require('os').cpus().length * 0.75), 3),
          1
        ),
        config: {
          standard: standard,
          path: configPath,
          define,
          terminologies: {
            MedDRA:
              standardIsSDTM && dictionary.MedDRA
                ? dictionary.MedDRA
                : '',
            SNOMED:
              standardIsSDTM && dictionary.SNOMED ? dictionary.SNOMED : '',
            UNII: standardIsSDTM ? dictionary.UNII : '',
            'NDF-RT': standardIsSDTM ? dictionary.NDFRT : ''
          }
        },
        engineVersion,
        report: {
          path: CLI.PATH.REPORTS,
          messageLimit: preferences.excelMessageLimit
        },
        sources: {
          paths,
          type: sourceFormatName,
          delimiter,
          qualifier
        },
        generate: 'define',
        target: CLI.PATH.REPORTS
      };

      Object.assign(parameters.config.terminologies, cdiscCT);

      const ipcStatus = {
        text: 'Starting',
        additionalInfo: [],
        subProcessingLabels: []
      };

      let metrics = null;
      const startDate = new Date();

      if (standardIsDefine) {
        parameters.sources.type = 'Validate';
        parameters.config.standard = defineStandard;
      }

      CLI.start(preferences, JSON.stringify(parameters))
        .on('STARTED', () => {
          ipcStatus.text = 'Validation has started...';
          ipcStatus.progress = 0;
          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
        })
        .on('TASK_PROCESSING', message => {
          const total = message.total;
          const current = message.current;
          const labels = message.label.split('->');

          ipcStatus.text = !standardIsDefine
            ? 'Validating files...'
            : `Validating ${path.basename(define)}...`;

          ipcStatus.additionalInfo = [];
          if (total > 0 && !standardIsDefine) {
            ipcStatus.additionalInfo.push([
              `Processing Dataset ${current} of ${total}`
            ]);
          }

          const subProcessingLabels = [];
          for (let num = 1; num < labels.length; ++num) {
            if (labels[num].includes('(')) {
              subProcessingLabels.push(`${labels[0]}: ${labels[num]}`);
            }
          }
          ipcStatus.subProcessingLabels =
            subProcessingLabels.length > 0
              ? subProcessingLabels
              : ipcStatus.subProcessingLabels;
          ipcStatus.additionalInfo = [
            ...ipcStatus.additionalInfo,
            ...ipcStatus.subProcessingLabels
          ];

          ipcStatus.progress = Math.min(100 * (current / total), 99);

          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
          ipcStatus.additionalInfo = [];
        })
        .on('TASK_SUBPROCESSING', message => {
          if (!message || !message.label) {
            return;
          }
          let label = message.label;
          const labels = label.split('->', 2);
          if (labels.length === 2) {
            label = `${labels[0]} ${labels[1]}`;
          }
          metrics = message.target;
          ipcStatus.subProcessingLabels = [label];
          ipcStatus.additionalInfo = [
            ...ipcStatus.additionalInfo,
            ...ipcStatus.subProcessingLabels
          ];

          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
        })
        .on('COMPLETED', message => {
          const response = message.target;
          ipcStatus.progress = 100;
          ipcStatus.isDone = true;
          ipcStatus.target = response.outputPath;
          ipcStatus.text = `Validation Complete!`;
          const endDate = new Date();
          const duration = endDate - startDate; // in milliseconds
          ipcStatus.additionalInfo = [[`Validation took ${getTime(duration)}`]];

          const recs = metrics.recordCount;

          let metricsGroup = [];

          if (recs > 0) {
            metricsGroup = [
              `${metrics.recordCount.toLocaleString()} records were examined across all datasets`,
              `${metrics.datasetsTotal.toLocaleString()} of ${metrics.datasetsTotal.toLocaleString()} datasets were validated`
            ];
          }

          if (!standardIsDefine) {
            metricsGroup = [
              ...metricsGroup,
              `${metrics.messageCount.toLocaleString()} messages were generated`
            ];
          }

          const checks = metrics.checkCount;
          if (checks > 0) {
            const message = standardIsDefine ? 'messages were generated' : 'checks were performed';
            metricsGroup = [
              ...metricsGroup,
              metrics.checkCount.toLocaleString() + ' ' + message
            ];
          }

          ipcStatus.additionalInfo = [
            ...ipcStatus.additionalInfo,
            metricsGroup,
            `The full report is stored in: ${CLI.PATH.REPORTS}`
          ];

          setTimeout(() => {
            if (!event.sender.isDestroyed()) {
              event.sender.send('status', ipcStatus);
            }
          }, 3000);
        })
        .on('EXCEPTION', message => {
          const response = message.target;

          winston.error(message);
          ipcStatus.text = `Error code: ${response.code} message: ${
            response.message
          }`;
          ipcStatus.isError = true;
          ipcStatus.progress = 100;
          ipcStatus.exception = response;
          ipcStatus.parameters = parameters;

          if (!event.sender.isDestroyed()) {
            event.sender.send('status', ipcStatus);
          }
        });
    }
  );
};
