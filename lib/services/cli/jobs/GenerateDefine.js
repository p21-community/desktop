/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { ipcMain } = require('electron');
const winston = require('../../logger');
const copyDefineStylesheet = require('../../../copy-define-stylesheet');

module.exports = CLI => {
  ipcMain.removeAllListeners('generateDefine');
  ipcMain.on('generateDefine', (event, { path, preferences }) => {
    const ipcStatus = {
      text: 'Starting'
    };

    CLI.start(
      preferences,
      JSON.stringify({
        action: 'GENERATE_DEFINE',
        sources: {
          paths: [path],
          type: 'Excel'
        },
        config: '',
        generate: 'define',
        target: CLI.PATH.DEFINES
      })
    )
      .on('STARTED', () => {
        if (!event.sender.isDestroyed()) {
          event.sender.send('status', {
            text: 'Parsing Excel template'
          });
        }
      })
      .on('TASK_PROCESSING', message => {
        const total = message.total;
        const current = message.current;

        if (!event.sender.isDestroyed()) {
          event.sender.send('status', {
            text: 'Generating define.xml',
            progress: Math.min(100 * (current / total), 99)
          });
        }
      })
      .on('COMPLETED', async message => {
        // Copy define stylesheet to defines report directory of user's file system
        await Promise.all([
          copyDefineStylesheet(CLI.PATH),
          new Promise((resolve) => setTimeout(resolve, 3000))
        ]);
        if (!event.sender.isDestroyed()) {
          event.sender.send('status', {
            text: 'Complete!',
            progress: 100,
            target: message.target.outputPath,
            isDone: true
          });
        }
      })
      .on('EXCEPTION', message => {
        winston.error(message);

        const response = message.target;
        if (!event.sender.isDestroyed()) {
          event.sender.send('status', {
            text: `Error code: ${response.code} message: ${response.message}`,
            progress: 100,
            isError: true,
            exception: response,
            parameters: {
              path,
              preferences
            }
          });
        }
      });
  });
};
