/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const fs = require('fs-extra');
const path = require('path');
const winston = require('../../logger');

module.exports = (CLI, preferences) => {
  const configFilePath = path.join(CLI.PATH.CONFIGS, 'config.json');

  return new Promise(resolve => {
    CLI.start(
      preferences,
      JSON.stringify({
        action: 'LIST_CONFIG',
        config: CLI.PATH.CONFIGS,
        target: configFilePath
      })
    )
      .on('STARTED', () => {
        winston.info('LIST_CONFIG action status: STARTED');
      })
      .on('TASK_PROCESSING', () => {
        winston.info('LIST_CONFIG action status: TASK_PROCESSING');
      })
      .on('COMPLETED', async () => {
        winston.info('LIST_CONFIG action status: COMPLETED');
        const contents = await fs.readFile(configFilePath, 'utf8');
        resolve(JSON.parse(contents));
      })
      .on('EXCEPTION', message => {
        winston.error(message);
      });
  });
};
