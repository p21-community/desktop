/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const path = require('path');
const { app } = require('electron');
const { EventEmitter } = require('events');
const os = require('os');
const { exec, spawn } = require('child_process');
const { values } = require('lodash');
const JWTDecode = require('jwt-decode');
const readline = require('readline');
const winston = require('../logger');
const GenericEventEmitter = require('../../generic-event-emitter');
const Settings = require('../../settings');
const Configuration = require(Settings.getPackageJsonPath());

const PATH = {
  EXPORT: Settings.getExportPath(),
  COMPONENTS: Settings.getComponentsPath(),
  JOBS: Settings.getJobsPath(),
  CONFIGS: Settings.getConfigsPath(),
  ENGINES: Settings.getEnginesPath(),
  REPORTS: Settings.getReportsPath(),
  DEFINES: Settings.getDefinesPath()
};
const MIN_INITIAL_MEMORY = 256;
const MAX_MEMORY_32BIT = 1250;
const MAX_MEMORY_64BIT = 4096;
const PENDING_STATUS = 'Pending';
const RUNNING_STATUS = 'Running';
const COMPLETED_STATUS = 'Completed';

class CLIUtils extends EventEmitter {
  constructor() {
    super();
    this.cliProc = null;
    this.currentJob = null;
    this.pendingJobs = [];
    this.actualArchitecture = null;
  }

  start(preferences, json) {
    const eventEmitter = new EventEmitter();
    const job = {
      preferences,
      json,
      eventEmitter,
      status: PENDING_STATUS
    };
    const completedHandled = () => {
      if (job.status !== COMPLETED_STATUS) {
        job.status = COMPLETED_STATUS;
        job.eventEmitter.removeAllListeners();
        this._next();
      }
    };
    eventEmitter.on('COMPLETED', completedHandled);
    eventEmitter.on('EXCEPTION', completedHandled);
    this.pendingJobs.push(job);
    if (this.currentJob == null) {
      // There are currently no other running jobs, so just start this one.
      this._next();
    }
    return eventEmitter;
  }

  exit() {
      if (this.cliProc != null) {
          this.pendingJobs.splice(0);
          this.cliProc.stdin.end(); // Signal no more jobs
          this.cliProc = null;
      }
  }

  _next() {
    if (this.pendingJobs.length > 0) {
      this.currentJob = this.pendingJobs.shift();
      this.currentJob.status = RUNNING_STATUS;
      this._start(this.currentJob);
    } else {
      this.currentJob = null;
    }
  }

  _start(job) {
    winston.info('Received request to start cli');
    this.getActualArchitecture().then((arch) => {
      return CLIUtils.checkOnlineStatus().then(online => {
        const cliProc = this._createCLIProcess(arch);
        const command = CLIUtils.getJsonCommand(
          online,
          job.preferences,
          job.json
        );
        winston.info('Sending JSON to stdin: ' + command);
        cliProc.stdin.write(command + os.EOL);
      });
    });
  }

  getActualArchitecture() {
    if (this.actualArchitecture) {
      return Promise.resolve(this.actualArchitecture);
    }
    return new Promise((resolve) => {
      if (!Settings.isWindows()) {
        // Apple will always be 64-bit
        resolve('x64');
        return;
      }
      if (Settings.is64Bit()) {
        // If node is telling us it's 64-bit, it must be 64-bit
        resolve('x64');
        return;
      }
      // Otherwise, try to run the 64-bit java. We must wrap the exe name in quotes.
      const command = `"${Settings.getJavaExecutablePath('x64')}" -version`;
      // If there was an error, then it must be 32-bit.
      exec(command, (error) => {
        resolve(error ? 'x32' : 'x64');
      });
    }).then((arch) => {
      this.actualArchitecture = arch;
      return arch;
    });
  }

  _createCLIProcess(arch) {
    if (this.cliProc != null) {
      return this.cliProc; // Reuse the same CLI process
    }
    const cliProc = spawn(
      Settings.getJavaExecutablePath(arch),
      CLIUtils.getProcessArgs(arch),
      CLIUtils.getProcessOptions()
    );

    cliProc.on('exit', code => {
      this.cliProc = null; // Flag that the process needs recreated
      winston.info(`child java process exited with code: ${code}`);
      if (this.currentJob != null && this.currentJob.status !== 'Completed') {
        this.currentJob.eventEmitter.emit('EXCEPTION', {
          label: 'Exception: The Java process exited abnormally.',
          target: {
            code: 999,
            message: 'The Java process exited abnormally.'
          }
        });
      }
    });

    cliProc.stdin.setEncoding('utf8');
    cliProc.stdout.setEncoding('utf8');
    cliProc.stderr.setEncoding('utf8');

    const lineReader = readline.createInterface({
      input: cliProc.stdout,
      terminal: false
    });
    lineReader.on('line', line => {
      winston.info(`message from java process stdout: ${line}`);
      if (this.currentJob != null && line.startsWith('{')) {
        try {
          const message = JSON.parse(line);
          if (message != null && message.state != null) {
            this.currentJob.eventEmitter.emit(message.state, message);
          }
        } catch (e) {
          // Prevent JSON parse errors from breaking application
        }
      }
    });

    cliProc.stderr.on('data', data => {
      winston.info(`message from java process stderr: ${data}`);
    });
    this.cliProc = cliProc;
    return cliProc;
  }

  static checkOnlineStatus() {
    const promise = new Promise(resolve => {
      // Safely resolve if 'online-status' doesn't get sent for some reason
      const id = setTimeout(() => resolve(false), 2000);
      GenericEventEmitter.once('online-status', online => {
        clearTimeout(id);
        resolve(online);
      });
    });
    GenericEventEmitter.emit('get-online-status');
    return promise;
  }

  static getUserId(preferences) {
    // Check if they have a Pinnacle ID
    if (preferences.identity) {
      try {
        const decodedIdentity = JWTDecode(preferences.identity.idToken);
        if (decodedIdentity) {
          return decodedIdentity.sub;
        }
      } catch (e) {
        winston.info(
          `Failed to decode JWT token: '${preferences.identity.idToken}'.`
        );
      }
    }
    return null;
  }

  static getProcessArgs(arch) {
    const options = {
      remoteLogging: '-Dpinnacle.software.logging.remote=' + !Settings.isDevelopment(),
      softwareVersion: '-Dpinnacle.software.version=' + app.getVersion(),
      logFilePath:
        '-Dcli.log.file=' +
        path.join(Settings.getLogDirectoryPath(), winston.fileName()),
      appendLog: '-Dcli.append.log=true',
      minMemory: '-Xms' + CLIUtils.getInitialMemory(arch) + 'm',
      maxMemory: '-Xmx' + CLIUtils.getMaxMemory(arch) + 'm',
      userCountry: '-Duser.country=US',
      headless: '-Djava.awt.headless=true',
      json: '-Djson=true',
      clientId: '-Dpinnacle.software.clientId=gui'
    };
    if (Settings.isDevelopment()) {
      options.debug = '-Xdebug';
      options.logLevel = '-Dcli.log.level=debug';
      options.debugParams =
        '-Xrunjdwp:transport=dt_socket,server=y,suspend=n,quiet=y,address=9987';
    }
    const p21ClientVersion = Configuration['p21-client-version'];
    return [...values(options), '-jar', `p21-client-${p21ClientVersion}.jar`];
  }

  static getProcessOptions() {
    return {
      cwd: path.join(Settings.getComponentsPath(), 'lib'),
      detached: false, // Kill the child when electron exits
      shell: false // Do not run within a shell
    };
  }

  /*
  If Xms is not specified, it defaults to 25% of free physical memory (up to
  16 MB, so we'll do the same except we'll cap it at 2 GB and at least
  256 MB)
 */
  static getInitialMemory(arch) {
    return Math.max(
      Math.min(
        parseInt(((os.freemem() / 1000000) * 0.25).toFixed()),
        (Settings.is64Bit(arch) ? MAX_MEMORY_64BIT : MAX_MEMORY_32BIT) / 2
      ),
      MIN_INITIAL_MEMORY
    );
  }

  /*
  If Xmx is not specified, it defaults to 75% of total physical memory (up
  to 1-2 GB, depending on the OS, so we'll do the same except we'll cap it
  at 4 GB)
   */
  static getMaxMemory(arch) {
    return Math.min(
      parseInt(((os.totalmem() / 1000000) * 0.75).toFixed()),
      Settings.is64Bit(arch) ? MAX_MEMORY_64BIT : MAX_MEMORY_32BIT
    );
  }

  static getJsonCommand(online, preferences, json) {
    return JSON.stringify({
      ...JSON.parse(json),
      engineFolder: PATH.ENGINES,
      online,
      email: preferences.email,
      userAgent: preferences.userAgent,
      userId: CLIUtils.getUserId(preferences),
      anonymousId: preferences.anonymousId,
      applicationVersion: app.getVersion()
    });
  }
}

const cli = new CLIUtils();
process.on('exit', () => cli.exit()); // clean up the child process
module.exports = {
  start: (...args) => cli.start(...args),
  PATH
};
