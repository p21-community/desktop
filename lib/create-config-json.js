/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const path = require('path');
const fs = require('fs-extra');
const { flattenDeep, orderBy } = require('lodash');

const MEDDRA = 'MEDDRA';
const SNOMED = 'SNOMED';

async function create(configsDir, enginesDir) {
  const enginesJson = await getEnginesJson(enginesDir);
  const engines = await createEngines(configsDir, enginesJson);
  const dictionaries = await createDictionaries(configsDir);
  const configJson = {
    engines: orderBy(engines, ['version', 'filter'], ['desc', 'asc']),
    dictionaries: orderBy(dictionaries, ['name'], ['asc'])
  };
  await writeConfigJson(configsDir, configJson);
  return configJson;
}

async function createEngines(configsDir, enginesJson) {
  const customConfigs = await getCustomConfigs(configsDir);
  return await Promise.all(enginesJson.map(engine =>
    createEngineDetails(configsDir, engine, customConfigs)));
}

async function createEngineDetails(configsDir, engine, customConfigs) {
  const engineConfigs = await getListOfConfigsForEngine(configsDir, engine);
  engineConfigs.sort();
  return {
    version: engine.version,
    name: `${engine.name} (${engine.version})`,
    filter: engine.agency,
    configs: engineConfigs,
    customConfigs: customConfigs
  };
}

async function getCustomConfigs(configsDir) {
  const customConfigsPath = path.join(configsDir, 'custom');
  try {
    const files = await fs.readdir(customConfigsPath);
    const customConfigs = files.filter(file => file.endsWith('.xml'));
    customConfigs.sort();
    return customConfigs;
  } catch (e) {
    return [];
  }
}

async function getEnginesJson(enginesDir) {
  try {
      const contents = await fs.readFile(path.join(enginesDir, 'engines.json'), 'utf8');
      return JSON.parse(contents);
  } catch (e) {
      return [];
  }
}

async function getListOfConfigsForEngine(configsDir, engine) {
  const engineConfigsPath = path.join(configsDir, engine.version);
  try {
    const files = await fs.readdir(engineConfigsPath);
    return files
        .filter(file => file.endsWith('.xml'))
        .filter(file => file.includes(`(${engine.agency})`));
  } catch (e) {
    console.error(`An error occurred while reading: ${engineConfigsPath}`, e);
    return [];
  }
}

async function createDictionaries(configsDir) {
  const hasCodeListFiles = async (path) => {
    try {
      const files = await fs.readdir(path);
      return files.some(file => file.endsWith('.xml') || file.endsWith('.txt'));
    } catch (e) {
      return false;
    }
  };
  const walkDirForDictionaries = async (cwd, dir) => {
    // Ignore any folders that start with a leading '.' (hidden)
    if (dir.startsWith('.')) {
      return [];
    }
    const completeDirPath = path.join(cwd, dir);
    let files;
    try {
      files = await fs.readdir(completeDirPath);
    } catch (e) {
      return [];
    }
    // Ignore any folders/files that start with a leading '.' (hidden)
    files = files.filter(f => !f.startsWith('.'));
    let isTerminologyFolder = false;
    let isImportedDictionary = [MEDDRA, SNOMED].includes(dir.toUpperCase());
    if (!isImportedDictionary) {
      const results = await Promise.all(files.map(file =>
          hasCodeListFiles(path.join(completeDirPath, file))));
      isTerminologyFolder = results.some(x => x);
    }
    if (isTerminologyFolder || isImportedDictionary) {
      if (dir.toUpperCase() === MEDDRA) {
        files.sort((a, b) => {
          return b - a;
        });
      } else {
        files.sort((a, b) => {
          const aa = a.split('-').join('');
          const bb = b.split('-').join('');
          return bb.localeCompare(aa);
        });
      }
      return [
        {
          name: dir,
          versions: files
        }
      ];
    } else {
      return await Promise.all(files.map(file => walkDirForDictionaries(completeDirPath, file)));
    }
  };

  const dictionaries = await walkDirForDictionaries(configsDir, 'data');
  return flattenDeep(dictionaries);
}

async function writeConfigJson(configsDir, configJson) {
  try {
    await fs.writeFile(
      path.join(configsDir, 'config.json'),
      JSON.stringify(configJson)
    );
  } catch (e) {
    console.error(e);
  }
}

module.exports = create;
