/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { release, arch, platform, freemem, totalmem, cpus } = require('os');
const { app, dialog, ipcMain, shell } = require('electron');
const { values } = require('lodash');
const util = require('util');
const winston = require('../lib/services/logger');

function getEnvInfo() {
  const execSync = require('child_process').execSync;
  let isRdp;
  try {
    const rdpStdout = execSync('tasklist');
    isRdp = rdpStdout.indexOf('rdp') !== -1 || rdpStdout.indexOf('RDP') !== -1;
  } catch (ignore) {
    isRdp = false;
  }
  const isCitrix = !!process.env['citrix_version'];

  return {
    appVersion: { label: 'Application Version', value: app.getVersion() },
    platform: { label: 'Platform', value: platform() },
    architecture: { label: 'Architecture', value: arch() },
    release: { label: 'Release', value: release() },
    freeMemory: { label: 'Free Memory', value: freemem() },
    totalMemory: { label: 'Total Memory', value: totalmem() },
    cpus: { label: 'CPUs', value: cpus().length },
    rdp: { label: 'RDP', value: isRdp },
    citrix: { label: 'Citrix', value: isCitrix }
  };
}

function sendBugReport(exception, message, meta) {
  const envInfo = getEnvInfo();
  const bugData = [
    {
      label: 'Exception',
      value:
        typeof exception === 'object' ? JSON.stringify(exception) : exception
    },
    ...values(envInfo)
  ];
  if (meta) {
    bugData.push({ label: 'Meta', value: JSON.stringify(meta) });
  }
  const bugString = bugData.map(val => `${val.label}: ${val.value}`).join('\n');
  const bugReport = `${message ? message + '\n' + '\n' : ''}${bugString}`;
  let openMailClientCommand = `mailto:community@pinnacle21.com?subject=Pinnacle 21 Community Bug Report&body=${encodeURI(
    bugReport
  )}`;
  // What we send to shell.openExternal can only be a Max of 2081 characters on Windows
  // going to trim the bug report on Windows for big stack traces
  if (envInfo.platform.value === 'win32') {
    openMailClientCommand = openMailClientCommand.substring(0, 2080);
  }
  winston.info('Opening mail client with: ');
  winston.info(openMailClientCommand);
  const emailAppWasAvailable = shell.openExternal(openMailClientCommand);
  if (!emailAppWasAvailable) {
    winston.error('Email app was not available to send bug report...');
    winston.error(bugReport);
  }
}

function UnknownExceptionRejectHandler(getWindow, meta) {
  let unknownExceptionPreviouslyThrown = false;
  const handleUnknownException = err => {
    if (unknownExceptionPreviouslyThrown) {
      // Ignore additional errors that occur while app is exiting
      return;
    }
    const win = getWindow();

    if (win && !win.isDestroyed()) {
      win.hide();
    }
    winston.error(util.format(err));
    // TODO: Wait for error to log before showing error message. Waiting for response from winston developers
    const response = dialog.showMessageBox(null, {
      type: 'error',
      buttons: ['Close Application', 'Send Bug Report'],
      message: 'Pinnacle 21 Community has stopped working',
      detail:
        'We apologize for the inconvenience, and would like to prevent this from happening in the future.\n\n' +
        'To help us with diagnosing what went wrong, please send us the error by clicking "Send Bug Report".'
    });
    unknownExceptionPreviouslyThrown = true;
    if (response === 0) {
      app.exit();
    } else {
      sendBugReport(err);
      app.exit();
    }
  };

  const exceptionHandler = err => {
    const win = getWindow();
    if (!win || win.isDestroyed()) {
      handleUnknownException(err);
    } else {
      const { webContents } = win;

      // Capture app preferences
      ipcMain.once('preferences', (event, preferences) => {
        meta['Application Preferences'] = preferences;
        // Try to get tool parameters for additional
        // info in bug report. If the listener doesn't respond after 500 ms,
        // likely a tool wasn't running and the parameters are insignificant
        const toolInfoPromise = new Promise(resolve => {
          const lastLocation = event.sender.history.pop();
          if (
            lastLocation.endsWith('validator') ||
            lastLocation.endsWith('converter') ||
            lastLocation.endsWith('miner')
          ) {
            // Capture tool parameters
            ipcMain.once('toolParameters', (event, tool, parameters) => {
              resolve({ tool, parameters });
            });
            webContents.send('getToolParameters');
          } else {
            resolve();
          }
        });
        const wait500msPromise = new Promise(resolve => {
          let wait = setTimeout(() => {
            clearTimeout(wait);
            resolve();
          }, 500);
        });
        Promise.race([toolInfoPromise, wait500msPromise])
          .then(toolInfo => {
            if (toolInfo) {
              meta[toolInfo.tool] = toolInfo.parameters;
            }
            winston.error(util.format(err), meta);
            webContents.send('set-error', err, meta);
          })
          .catch(e => {
            console.log(e);
            winston.error(util.format(err), meta);
            webContents.send('set-error', err, meta);
          });

        ipcMain.once('closeApp', (event, relaunch) => {
          if (relaunch) {
            app.relaunch();
          }
          app.quit();
        });
      });
      webContents.send('getPreferences');
    }
  };

  return {
    handle: handleUnknownException,
    listen: () => {
      process.once('unhandledRejection', err => {
        exceptionHandler(err);
      });
      process.once('uncaughtException', err => {
        exceptionHandler(err);
      });
    }
  };
}

module.exports = {
  sendBugReport,
  UnknownExceptionRejectHandler
};
