const { app } = require('electron');
const path = require('path');

const CDN_BASE_URL = 'https://d3oewpyuscgo0v.cloudfront.net/';
const S3_ROOTS = ['snapshot', 'pre-release'];

class Settings {
  static getUserDataPath() {
    return process.platform === 'win32'
      ? path.resolve(app.getPath('appData'), '..', 'Local', app.getName())
      : app.getPath('userData');
  }

  static getExportPath() {
    return path.join(app.getPath('documents'), app.getName());
  }

  static getComponentsPath() {
    const appPath = app.getAppPath();
    const isProd = !process.defaultApp;
    return isProd
      ? path.resolve(appPath, '..', 'app.asar.unpacked', 'components')
      : path.resolve(appPath, 'components');
  }

  static getJobsPath() {
    return path.join(app.getAppPath(), 'lib', 'services', 'cli', 'jobs');
  }

  static getConfigsPath() {
    return path.join(Settings.getExportPath(), 'configs');
  }

  static getEnginesPath() {
    return path.join(Settings.getUserDataPath(), 'engines');
  }

  static getReportsPath() {
    return path.join(Settings.getExportPath(), 'reports');
  }

  static getDefinesPath() {
    return path.join(Settings.getExportPath(), 'defines');
  }

  static getLogDirectoryPath() {
    return path.join(Settings.getExportPath(), 'logs');
  }

  static isWindows() {
    return process.platform === 'win32';
  }

  static is64Bit(hint) {
    return (hint ? hint : process.arch) === 'x64';
  }

  static getJava32DirectoryPath() {
    return path.join(Settings.getComponentsPath(), 'java32/bin');
  }

  static getJava64DirectoryPath() {
    return path.join(Settings.getComponentsPath(), 'java64/bin');
  }

  static getJavaDirectoryPath(hint) {
    if (Settings.isWindows()) {
      return Settings.is64Bit(hint)
          ? Settings.getJava64DirectoryPath()
          : Settings.getJava32DirectoryPath();
    } else {
      return path.join(Settings.getComponentsPath(), 'java/bin');
    }
  }

  static getJavaExecutablePath(hint) {
    return path.join(Settings.getJavaDirectoryPath(hint), 'java');
  }

  static getPackageJsonPath() {
    const appPath = app.getAppPath();
    return path.join(appPath, 'package.json');
  }

  static isDevelopment() {
    return !!process.defaultApp;
  }

  static isProduction() {
    return !process.defaultApp;
  }
  
  static isRelease() {
    const version = app.getVersion().toLowerCase();
    return !version.includes('snapshot') && !version.includes('pre-release');
  }

  static getCdnBaseUrl() {
    const version = app.getVersion().toLowerCase();

    let root = S3_ROOTS.find(root => version.includes(root));
    if (!root) {
      root = 'release';
    }

    return CDN_BASE_URL + root;
  }
}

module.exports = Settings;
