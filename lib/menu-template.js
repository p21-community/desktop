/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

module.exports = (win, exportPath) => {
  const { app, shell } = require('electron');
  const path = require('path');
  const Menu = [];

  const aboutMenuItem = {
    label: 'About Pinnacle 21 Community',
    click: () => {
      win.webContents.send('open-about-modal');
    }
  };

  const preferencesMenuItem = {
    label: 'Preferences',
    click: () => {
      win.webContents.send('open-preferences');
    }
  };

  if (process.platform === 'darwin') {
    Menu.push({
      label: app.getName(),
      submenu: [
        aboutMenuItem,
        { type: 'separator' },
        preferencesMenuItem,
        { type: 'separator' },
        { role: 'hide' },
        { role: 'hideothers' },
        { role: 'unhide' },
        { type: 'separator' },
        { role: 'quit' }
      ]
    });
  }

  const fileMenu = {
    label: 'File',
    submenu: [
      {
        label: 'New Validation',
        click: () => {
          win.webContents.send('redirect', '/validator');
        }
      },
      {
        label: 'New Specification',
        click: () => {
          win.webContents.send('redirect', '/generate/excel');
        }
      },
      { type: 'separator' },
      {
        label: 'Generate Define.xml',
        click: () => {
          win.webContents.send('redirect', '/generate/xml');
        }
      },
      {
        label: 'Convert Dataset',
        click: () => {
          win.webContents.send('redirect', '/converter');
        }
      }
      // { type: 'separator' },
      // {
      //   label: 'Query ClinicalTrials.gov'
      // }
    ]
  };

  const viewMenu = {
    label: 'View',
    submenu: [
      { role: 'zoomin' },
      { role: 'zoomout' },
      { role: 'togglefullscreen' }
    ]
  };

  const helpMenu = {
    role: 'help',
    submenu: [
      {
        label: 'Pinnacle 21 Community Documentation',
        click: shell.openExternal.bind(
          null,
          'https://www.pinnacle21.com/documentation'
        )
      },
      {
        label: 'Show Log File',
        click: shell.openItem.bind(null, path.join(exportPath, 'logs'))
      },
      {
        label: 'Get Support',
        click: shell.openExternal.bind(null, 'https://www.pinnacle21.com/forum')
      },
      {
        label: 'Send Feedback',
        click: shell.openExternal.bind(
          null,
          'mailto:community@pinnacle21.com?subject=Pinnacle 21 Community Feedback'
        )
      },
      {
        label: 'Upgrade to Pinnacle 21 Enterprise',
        click: shell.openExternal.bind(
          null,
          'https://www.pinnacle21.com/products/validation'
        )
      }
    ]
  };

  if (process.platform !== 'darwin') {
    fileMenu.submenu.push(
      { type: 'separator' },
      { label: 'Quit', role: 'quit' }
    );
    helpMenu.submenu.unshift(aboutMenuItem, preferencesMenuItem);
  }

  Menu.push(
    fileMenu,
    //{ role: 'editMenu' },
    viewMenu,
    //{ role: 'windowMenu' },
    helpMenu
  );

  return Menu;
};
