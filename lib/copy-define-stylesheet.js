/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const fs = require('fs-extra');
const winston = require('./services/logger');
const path = require('path');

module.exports = async PATH => {
  try {
    const stylesheetFileName = 'define2-0-0.xsl';
    const targetStylesheetPath = path.resolve(
      path.join(PATH.DEFINES, stylesheetFileName)
    );
    const projectPath = path.dirname(require.main.filename);
    const sourceStylesheetPath = path.resolve(
      path.join(projectPath, 'resources', stylesheetFileName)
    );

    // If no op has been run yet, the reports directory doesn't exist yet
    if (!(await fs.exists(PATH.EXPORT))) {
      await fs.mkdir(PATH.EXPORT);
    }
    // If a define op hasn't been run yet, the defines report directory doesn't exist yet
    if (!(await fs.exists(PATH.DEFINES))) {
      await fs.mkdir(PATH.DEFINES);
    }

    // If the file already exists, check for an update
    if (await fs.exists(targetStylesheetPath)) {
      const resourceFileStat = await fs.stat(sourceStylesheetPath);
      const existingFileStat = await fs.stat(targetStylesheetPath);

      if (new Date(resourceFileStat.mtime) > new Date(existingFileStat.mtime)) {
        await fs.copyFile(sourceStylesheetPath, targetStylesheetPath);
      }
    } else {
      await fs.copyFile(sourceStylesheetPath, targetStylesheetPath);
    }
  } catch (err) {
    // If this fails, it shouldn't affect normal behavior of application
    winston.error(
      'Unable to copy define stylesheet to report directory\n' + err.toString()
    );
  }
};
