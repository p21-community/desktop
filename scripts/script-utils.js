/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const fs = require('fs-extra');
const AWS = require('aws-sdk');

const AWS_ACCESS_KEY = process.env.AWS_ACCESS_KEY_ID;
const AWS_SECRET_KEY = process.env.AWS_SECRET_ACCESS_KEY;
const s3Config = { apiVersion: '2006-03-01' };
if (AWS_ACCESS_KEY) {
  s3Config.accessKeyId = AWS_ACCESS_KEY;
}
if (AWS_SECRET_KEY) {
  s3Config.secretAccessKey = AWS_SECRET_KEY;
}
const s3 = new AWS.S3(s3Config);

const determineReleaseType = version => {
  if (version.toLowerCase().indexOf('snapshot') > -1) {
    return 'snapshot';
  } else if (version.toLowerCase().indexOf('pre-') > -1) {
    return 'pre-release';
  } else {
    return 'release';
  }
};

const getObjectFromS3 = config => {
  const { bucket, key } = config;
  return new Promise((resolve, reject) => {
    s3.getObject({ Bucket: bucket, Key: key }, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

const lsS3 = (bucket, startAt) => {
  return new Promise((resolve, reject) => {
    s3.listObjectsV2({ Bucket: bucket, StartAfter: startAt }, (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
};

const getS3 = (bucket, key) => {
  return new Promise((resolve, reject) => {
    s3.getObject({ Bucket: bucket, Key: key }, (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
};

const downloadS3Object = async (bucket, s3Key, filePath) => {
  try {
    const data = await getS3(bucket, s3Key);
    await fs.outputFile(filePath, data.Body);
  } catch (e) {
    console.error(`Error downloading: ${bucket}/${s3Key}`, e);
  }
};

const downloadS3Bucket = async config => {
  const {
    bucket,
    startAt,
    downloadPath,
    exclusions,
    exclusionThunk,
    pathTranslator
  } = config;

  const shouldExclude = (objKey, exclusionFn) => {
    if (exclusions != null) {
      for (let exc of exclusions) {
        const excludedS3Path = `${startAt}/${exc}`;
        if (objKey.startsWith(excludedS3Path)) {
          return true;
        }
      }
    }
    if (exclusionFn != null && typeof exclusionFn === 'function') {
      return exclusionFn(objKey);
    }
    return false;
  };
  const translator =
    pathTranslator == null || typeof pathTranslator !== 'function'
      ? path => path
      : pathTranslator;
  const s3Data = await lsS3(bucket, startAt);
  console.log(`${s3Data.Contents.length} files found in ${bucket} bucket`);

  let exclusionFn = null;
  if (exclusionThunk && typeof exclusionThunk === 'function') {
    exclusionFn = exclusionThunk(s3Data);
  }

  const downloadPromises = [];

  for (const s3Obj of s3Data.Contents) {
    if (
      !s3Obj.Key.startsWith(startAt) ||
      s3Obj.Key.endsWith('/') ||
      shouldExclude(s3Obj.Key, exclusionFn)
    ) {
      continue;
    }
    const filePath =
      downloadPath + '/' + translator(s3Obj.Key.replace(startAt + '/', ''));
    const fileExists = await fs.exists(filePath);
    if (fileExists) {
      console.log(`Skipping: ${s3Obj.Key}, it already exists.`);
    } else {
      downloadPromises.push(downloadS3Object(bucket, s3Obj.Key, filePath));
    }
  }
  await Promise.all(downloadPromises);
};

module.exports = {
  determineReleaseType,
  getObjectFromS3,
  downloadS3Bucket
};
