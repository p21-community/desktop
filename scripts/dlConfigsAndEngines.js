/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const del = require('del');
const fs = require('fs-extra');
const path = require('path');
const { mapValues } = require('lodash');
const { downloadS3Bucket, determineReleaseType } = require('./script-utils');
const createConfigJson = require('../lib/create-config-json');
const config = require('../package.json');

const appVersion = config['version'];
const releaseType = determineReleaseType(appVersion);

const configExclusionThunk = function(responseFromS3) {
  // The dictionaries that we include with the build are in the following directories:
  //
  // s3://p21-community-resources/${releaseType}/engine/configs/data/CDISC/ADaM
  // s3://p21-community-resources/${releaseType}/engine/configs/data/CDISC/SDTM
  // s3://p21-community-resources/${releaseType}/engine/configs/data/CDISC/SEND
  // s3://p21-community-resources/${releaseType}/engine/configs/data/NDF-RT
  // s3://p21-community-resources/${releaseType}/engine/configs/data/UNII
  //
  // They are in directories that are labeled in YYYY-MM-DD date format and
  // we only want to include the most recent 5 using that date
  const NUMBER_OF_DICTIONARIES_TO_INCLUDE = 5;
  const dictionariesPath = `${releaseType}/engine/configs/data`;
  const s3DictionariesMeta = [
    {
      type: 'adam',
      path: '/CDISC/ADaM/'
    },
    {
      type: 'sdtm',
      path: '/CDISC/SDTM/'
    },
    {
      type: 'send',
      path: '/CDISC/SEND/'
    },
    {
      type: 'ndfrt',
      path: '/NDF-RT/'
    },
    {
      type: 'unii',
      path: '/UNII/'
    }
  ];
  const dictionaryRegex = /\d\d\d\d-\d\d-\d\d/;
  const isDate = str => dictionaryRegex.test(str);

  const dictionariesFoundByType = {
    adam: new Set(),
    sdtm: new Set(),
    send: new Set(),
    ndfrt: new Set(),
    unii: new Set()
  };

  for (const s3Obj of responseFromS3.Contents) {
    for (const dictionaryMeta of s3DictionariesMeta) {
      const path = dictionariesPath + dictionaryMeta.path;
      if (
        s3Obj.Key.startsWith(dictionariesPath + dictionaryMeta.path) &&
        (s3Obj.Key.endsWith('.xml') || s3Obj.Key.endsWith('.txt'))
      ) {
        const dictionaryDate = s3Obj.Key.slice(path.length).split('/')[0];
        if (isDate(dictionaryDate)) {
          dictionariesFoundByType[dictionaryMeta.type].add(dictionaryDate);
        }
      }
    }
  }

  // Sort the dictionaryArrays by most recent, then
  // limit them to the predefined size allow
  const trimmedDictionariesFoundByType = mapValues(
    dictionariesFoundByType,
    dictSet => {
      const dictArr = Array.from(dictSet);

      dictArr.sort((a, b) => {
        const aa = a.split('-').join();
        const bb = b.split('-').join();
        return aa < bb ? 1 : aa > bb ? -1 : 0;
      });

      return dictArr.slice(0, NUMBER_OF_DICTIONARIES_TO_INCLUDE);
    }
  );

  return s3ObjKey => {
    for (const dictionaryMeta of s3DictionariesMeta) {
      const path = dictionariesPath + dictionaryMeta.path;
      if (s3ObjKey.startsWith(dictionariesPath + dictionaryMeta.path)) {
        const dictionariesAllowed =
          trimmedDictionariesFoundByType[dictionaryMeta.type];
        const dictionaryDate = s3ObjKey.slice(path.length).split('/')[0];
        const shouldInclude = dictionariesAllowed.includes(dictionaryDate);
        return !shouldInclude;
      }
    }
  };
};

async function prep(configsPath, enginesPath) {
  await del([configsPath, enginesPath]);
  await fs.mkdir(configsPath);
  await fs.mkdir(enginesPath);
}

async function downloadConfigsAndEngines(configsPath, enginesPath) {
  function etagPathTranslator(path) {
    try {
      const splitPath = path.split('/');
      const fileName = splitPath[splitPath.length - 1];
      if (!fileName.startsWith('.')) {
        splitPath[splitPath.length - 1] = '.' + fileName;
        return splitPath.join('/');
      }
      return path;
    } catch (e) {
      console.error(e);
      return path;
    }
  }
  const configPromise = downloadS3Bucket({
    bucket: 'p21-community-resources',
    startAt: `${releaseType}/engine/configs`,
    downloadPath: configsPath,
    exclusions: ['dictionaries'],
    exclusionThunk: configExclusionThunk,
    pathTranslator: path => {
      if (path.endsWith('eTag')) {
        return etagPathTranslator(path);
      }
      return path;
    }
  });
  const enginePromise = downloadS3Bucket({
    bucket: 'p21-community-resources',
    startAt: `${releaseType}/engine`,
    downloadPath: enginesPath,
    exclusions: ['configs', 'converter', 'engines.eTag'],
    exclusionThunk: null,
    pathTranslator: path => {
      if (path.startsWith('modules/')) {
        path = 'modules/pinnacle21/validation/' + path.substring(8);
      }
      if (path.endsWith('eTag')) {
        return etagPathTranslator(path);
      }
      return path;
    }
  });
  await Promise.all([configPromise, enginePromise]);
}

async function run() {
  const configsPath = path.resolve(path.join('components', 'lib', 'configs'));
  const enginesPath = path.resolve(path.join('components', 'lib', 'engines'));

  await prep(configsPath, enginesPath);
  await downloadConfigsAndEngines(configsPath, enginesPath);
  await createConfigJson(configsPath, enginesPath);
}

run()
  .then(() => null)
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
