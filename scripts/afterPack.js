/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

module.exports = () => {
  const fs = require('fs');
  const destPath64 = 'dist/win-unpacked/';
  const destPath32 = 'dist/win-ia32-unpacked/';
  const resources = '/resources';
  const targetPath64 = destPath64 + resources;
  const targetPath32 = destPath32 + resources;
  const licensePath = '/LICENSE.txt';
  const changeLogPath = '/CHANGELOG.txt';

  // Copy CHANGELOG.txt, LICENSE.txt
  // Can't rely on OS here since we build both dist's on OSX. This should only fire for the windows build
  // based on file path
  // Move for both 32 bit and 64 bit windows builds
  moveFiles(destPath64, targetPath64);
  moveFiles(destPath32, targetPath32);

  function moveFiles(destPath, targetPath) {
    if (fs.existsSync(targetPath + licensePath)) {
      fs.renameSync(targetPath + licensePath, destPath + licensePath);
    }
    if (fs.existsSync(targetPath + changeLogPath)) {
      fs.renameSync(targetPath + changeLogPath, destPath + changeLogPath);
    }
  }
};
