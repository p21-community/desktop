/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const childProcess = require('child_process');
const fs = require('fs');
const path = require('path');
const unzip = require('unzip-stream');
const os = require('os');
const del = require('del');
const config = require('../package.json');
const util = require('util');
const { getObjectFromS3 } = require('./script-utils');

const componentsDir = path.resolve('components');
const LOCAL_SNAPSHOTS = 'libs-snapshots-local';
const LOCAL_RELEASES = 'local-releases';

if (process.argv.length >= 4) {
  const arg = process.argv[3];
  if (arg !== 'java-only') {
    throw new Error(`Unknown argument: ${arg}`);
  }
} else {
  run()
    .then(() => null)
    .catch((err) => {
      console.error(err);
      process.exit(1);
    });
}

async function run() {
  await del([componentsDir]);
  const mkdir = util.promisify(fs.mkdir);
  await mkdir(componentsDir);

  const p21ClientVersion = config['p21-client-version'];
  const split = p21ClientVersion.split('-', 2);
  const repo = (split.length < 2 || split[1] === 'PRE')
    ? LOCAL_RELEASES
    : LOCAL_SNAPSHOTS;
  const exists = util.promisify(fs.exists);
  const doesJFrogConfigExist = await exists(
    path.join(os.homedir(), '.jfrog', 'jfrog-cli.conf')
  );
  const jfrogCommand = path.resolve('node_modules/.bin/jfrog');
  const exec = util.promisify(childProcess.exec);
  if (!doesJFrogConfigExist) {
    await exec(
      `${jfrogCommand} rt config --url=${process.env.ARTIFACTORY_URL} --apikey=${
        process.env.ARTIFACTORY_API_KEY
      } --interactive=false`
    );
  }
  const listAllBuilds = `"${repo}/net/pinnacle21/cli/${p21ClientVersion}/*.jar"`;
  const listAllBuildsCommand = `${jfrogCommand} rt s  ${listAllBuilds} --sort-by=modified --sort-order=desc`;
  const { stdout: listStdOut, stderr: listStdErr } = await exec(listAllBuildsCommand, {
    env: { ...process.env, JFROG_CLI_LOG_LEVEL: 'ERROR' }
  });
  console.log(listAllBuildsCommand);
  console.log(`stdout = ${listStdOut}`);
  console.log(`stderr = ${listStdErr}`);
  const latestBuild = JSON.parse(listStdOut)[0].path;
  const cliJar = path.basename(latestBuild);

  console.log(`Pulling ${cliJar} into ${componentsDir}/lib directory...`);
  const { stdout, stderr } = await exec(
    `${jfrogCommand} rt dl --flat=true ${latestBuild} ${componentsDir}/lib/`
  );
  const downloadedItemCount = parseInt(JSON.parse(stdout).totals.success);

  if (downloadedItemCount === 0) {
    throw new Error(`Unable to download ${cliJar} from ${latestBuild}`);
  } else if (downloadedItemCount !== 1) {
    throw new Error(
      `Expected download count of 1 but got ${downloadedItemCount}`
    );
  }

  console.log(`Renaming ${cliJar} to p21-client-${p21ClientVersion}.jar`);
  const rename = util.promisify(fs.rename);
  await rename(
    `${componentsDir}/lib/${cliJar}`,
    `${componentsDir}/lib/p21-client-${p21ClientVersion}.jar`
  );

  /* Download and package java libs */

  console.log('Downloading Java Runtime Environment 8u181...');

  const osArg = process.argv[2];

  if (!['win', 'mac', 'linux'].includes(osArg)) {
    throw new Error('No valid osArg was passed in');
  }
  const data = await getObjectFromS3({
    bucket: 'p21-community-resources',
    key: `java/${osArg}/java.zip`
  });
  const zip = path.join(componentsDir, 'java.zip');
  const writeFile = util.promisify(fs.writeFile);
  await writeFile(zip, data.Body);
  const zipStream = fs.createReadStream(zip);
  zipStream.on('end', async function() {
    if (osArg !== 'win') {
      console.log('Changing permissions to make java executable...');
      const javaExePath = path.join(componentsDir, 'java', 'bin', 'java');
      const chmod = util.promisify(fs.chmod);
      await chmod(javaExePath, 0o755);
    }
    await del([zip]);
  });
  zipStream.pipe(unzip.Extract({ path: componentsDir }));
}
