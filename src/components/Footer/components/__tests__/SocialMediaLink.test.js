/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import renderer from 'react-test-renderer';
import SocialMediaLink from '../SocialMediaLink';

test('SocialMediaLink renders correctly with its given props', () => {
  const socialMediaLink = renderer.create(
    <SocialMediaLink name="Facebook" href="/facebook" icon="fa fa-facebook" />
  );
  expect(socialMediaLink.toJSON()).toMatchSnapshot();
});
