/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import SocialMediaLink from './components/SocialMediaLink';

function Footer() {
  return (
    <div className="footer" style={{ position: 'static' }}>
      <div className="container">
        <div className="row">
          <span className="col-md-6">
            &copy; {new Date().getFullYear()} Pinnacle 21 LLC
          </span>
          <span className="col-md-6 social-icons">
            <ul>
              <li>Follow us:</li>
              <SocialMediaLink
                name="Linkedin"
                href="https://www.linkedin.com/company/pinnacle-21"
                icon="fa fa-linkedin-square"
              />
              <SocialMediaLink
                name="Twitter"
                href="https://twitter.com/pinnacle_21"
                icon="fa fa-twitter-square"
              />
              <SocialMediaLink
                name="Facebook"
                href="https://www.facebook.com/pinnacle21"
                icon="fa fa-facebook-square"
              />
              <SocialMediaLink
                name="Email"
                href="http://eepurl.com/Shm9D"
                icon="fa fa-envelope"
              />
            </ul>
          </span>
        </div>
      </div>
    </div>
  );
}

export default Footer;
