/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import { mount } from 'enzyme';
import Nav from '../Nav';
import SubMenu from '../SubMenu';
import NavItem from '../NavItem';
import { MemoryRouter, StaticRouter } from 'react-router';

test('SubMenu renderers correctly with its given props', () => {
  const subMenuRenderer = new ReactShallowRenderer();
  subMenuRenderer.render(<SubMenu text="Text" icon="Icon" />);
  expect(subMenuRenderer.getRenderOutput()).toMatchSnapshot();
});

describe('SubMenu with an active state', () => {
  const subMenuComponent = mount(
    <StaticRouter context={{}}>
      <SubMenu text="Text" icon="Icon" location={{ pathname: 'test' }}>
        <li />
      </SubMenu>
    </StaticRouter>
  );

  subMenuComponent.find('a').simulate('click');

  test("has a <NavLink /> with a class of 'active'", () => {
    expect(subMenuComponent.find('.active').exists()).toBe(true);
  });

  test('has a down-arrow icon', () => {
    expect(subMenuComponent.find('i.fa-angle-down').exists()).toBe(true);
  });

  test('renders its children', () => {
    expect(subMenuComponent.find('ul.sub-menu > li').exists()).toBe(true);
  });
});

test("SubMenu's state is set to active on mount", () => {
  const subMenuComponent = mount(
    <StaticRouter context={{}}>
      <SubMenu text="Text" icon="Icon" location={{ pathname: 'test' }}>
        <NavItem to="test" icon="Icon" text="Text" />
      </SubMenu>
    </StaticRouter>
  );
  expect(subMenuComponent.find('.active').exists()).toBe(true);
});

test(`SubMenu's state is still active after clicking on its child component`, () => {
  const subMenuComponent = mount(
    <MemoryRouter initialEntries={['/home']} initialIndex={0}>
      <div>
        <Nav>
          <SubMenu text="Text" icon="Icon">
            <NavItem to="/generate/xml" icon="Icon" text="Text" />
          </SubMenu>
        </Nav>
      </div>
    </MemoryRouter>
  );

  expect(subMenuComponent.find('.active').exists()).toBe(false);

  subMenuComponent.find('a').simulate('click');

  expect(subMenuComponent.find('.active').exists()).toBe(true);

  subMenuComponent
    .find('a[href="/generate/xml"]')
    .simulate('click', { button: 0 });

  expect(subMenuComponent.find('.active').exists()).toBe(true);
});
