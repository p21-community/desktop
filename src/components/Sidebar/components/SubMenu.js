/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

const propTypes = {
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  minified: PropTypes.bool.isRequired
};

class SubMenu extends Component {
  constructor(props) {
    super(props);
    this.state = { isActive: false };
  }

  componentDidMount = () => {
    React.Children.forEach(this.props.children, child => {
      if (this.props.location.pathname === child.props.to) {
        this.setIsActiveToTrue();
      }
    });
  };

  componentDidUpdate = prevProps => {
    React.Children.forEach(this.props.children, child => {
      const { pathname } = this.props.location;
      if (
        prevProps.location.pathname !== pathname &&
        pathname === child.props.to
      ) {
        this.setIsActiveToTrue();
      }
    });
  };

  handleClick = () => {
    this.setState({ isActive: !this.state.isActive });
  };

  renderSubMenu = () =>
    this.state.isActive && !this.props.minified ? (
      <ul className="sub-menu open">{this.props.children}</ul>
    ) : (
      ''
    );

  renderHiddenSubMenu = () =>
    this.props.minified ? (
      <ul className="sub-menu">{this.props.children}</ul>
    ) : (
      ''
    );

  setIsActiveToTrue = () => {
    if (!this.state.isActive) {
      this.setState({ isActive: true });
    }
  };

  render() {
    const arrowIcon = (
      <i
        className={`toggle-icon fa fa-angle-${
          this.state.isActive ? 'down' : 'left'
        }`}
      />
    );
    return (
      <li>
        <NavLink
          to="#"
          className={this.state.isActive ? 'active' : ''}
          onClick={this.handleClick}
        >
          <i className={this.props.icon} />
          <span className="text">{this.props.text}</span>
          {arrowIcon}
        </NavLink>
        <CSSTransitionGroup
          transitionName="slide"
          transitionEnterTimeout={300}
          transitionLeaveTimeout={300}
        >
          {this.renderSubMenu()}
        </CSSTransitionGroup>

        {/* CSSTransitionGroup works only when elements are removed and added.
         * For the hover display of "sub-menu" items to work, the "sub-menu"
         * items need to rendered into the DOM. Therefore, we are rendering
         * a second "sub-menu" <ul> that will be hidden to the user.
         */}
        {this.renderHiddenSubMenu()}
      </li>
    );
  }
}

SubMenu.propTypes = propTypes;

export default SubMenu;
