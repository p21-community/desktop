/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';

const BannerNotifications = props => {
  const renderNotifications = () =>
    props.notifications.map(({ message, link, link_text }, i) => {
      return (
        <li key={i}>
          <span
            className={`quick-access-item bg-color-${
              i === 0 ? 'blue' : 'green'
            }`}
          >
            <i className={`fa fa-${i === 0 ? 'users' : 'desktop'} bg`} />
            <h5 className="caps">{message.title}</h5>
            <h6>{message.description}</h6>
            <p className="not-last">{message.date}</p>
            <p>
              <a
                rel="noopener noreferrer"
                target="_blank"
                href={link}
                className="fink"
              >
                {link_text}
              </a>
            </p>
          </span>
        </li>
      );
    });

  return (
    <div className="sidebar-content">
      <ul className="list-inline quick-access">{renderNotifications()}</ul>
    </div>
  );
};

export default BannerNotifications;
