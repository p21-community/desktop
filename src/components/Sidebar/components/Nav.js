/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';

const propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  location: PropTypes.object.isRequired
};

function Nav(props) {
  return (
    <nav>
      <ul className="main-menu">
        {React.Children.map(props.children, child => {
          if (child) {
            return child && child.type === 'li'
              ? child
              : React.cloneElement(child, {
                  location: props.location
                });
          } else {
            return '';
          }
        })}
      </ul>
    </nav>
  );
}

Nav.propTypes = propTypes;

export default withRouter(Nav);
