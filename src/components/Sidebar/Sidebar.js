/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { /*NavLink,*/ withRouter } from 'react-router-dom';
// import { Button } from 'react-bootstrap';

import Nav from './components/Nav';
import NavItem from './components/NavItem';
import BannerNotifications from './components/BannerNotifications';
import SubMenu from './components/SubMenu';

const propTypes = {
  notifications: PropTypes.array.isRequired,
  environment: PropTypes.object.isRequired,
  minified: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  onDisconnect: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

const Sidebar = props => {
  // const onCancelOrDisconnect = event => {
  //   event.preventDefault();
  //
  //   if (props.environment.name && !props.environment.loggedIn) {
  //     props.onCancel();
  //   } else {
  //     props.onDisconnect();
  //   }
  //   props.history.push('/');
  // };
  //
  // const renderConnectButton = () => {
  //   if (props.environment.name) {
  //     if (props.environment.loggedIn) {
  //       if (props.minified) {
  //         return (
  //           <li>
  //             <div className="minified">
  //               <i
  //                 className="fa fa-bolt success"
  //                 dataToggle="tooltip"
  //                 title={props.environment.name}
  //               />
  //             </div>
  //           </li>
  //         );
  //       } else {
  //         return (
  //           <li>
  //             <div>
  //               <i className="fa fa-bolt success" />
  //               <span className="text">{props.environment.name}</span>
  //             </div>
  //           </li>
  //         );
  //       }
  //     } else {
  //       return (
  //         <NavItem
  //           to="/connect"
  //           icon="fa fa-bolt"
  //           text={props.environment.name}
  //         />
  //       );
  //     }
  //   } else {
  //     return (
  //       <li>
  //         <NavLink
  //           className="connect"
  //           exact
  //           to="/connect"
  //           activeClassName="active"
  //         >
  //           {showPlugIcon()}
  //           <span className="text">{showConnectToEnterprise()}</span>
  //         </NavLink>
  //       </li>
  //     );
  //   }
  // };
  //
  // const renderCancelOrDisconnectButton = () => {
  //   const { name, loggedIn } = props.environment;
  //   if (loggedIn) {
  //     return renderWarningButton('Disconnect', 'fa fa-chain-broken');
  //   } else if (name && !loggedIn) {
  //     return renderWarningButton('Cancel', 'fa fa-times');
  //   }
  // };
  //
  // const renderWarningButton = (name, icon) => {
  //   if (props.minified) {
  //     return (
  //       <li>
  //         <a href="/" onClick={onCancelOrDisconnect}>
  //           <i className={icon} />
  //           <span className="text">{name}</span>
  //         </a>
  //       </li>
  //     );
  //   } else {
  //     return (
  //       <li className="disconnect">
  //         <Button bsStyle="warning" onClick={onCancelOrDisconnect}>
  //           {name}
  //         </Button>
  //       </li>
  //     );
  //   }
  // };
  //
  // const showConnectToEnterprise = () => {
  //   if (!props.environment.name) {
  //     return props.minified ? (
  //       'Connect to Enterprise'
  //     ) : (
  //       <Button bsStyle="warning">Connect to Enterprise</Button>
  //     );
  //   }
  // };
  //
  // const showPlugIcon = () => {
  //   if (props.minified) {
  //     return <i className="fa fa-bolt" />;
  //   }
  // };

  return (
    <div className={`left-sidebar ${props.minified ? 'minified' : ''}`}>
      <Nav>
        <NavItem to="/" icon="fa fa-home" text="Home" />
        <NavItem to="/validator" icon="fa fa-check" text="Validator" />
        <SubMenu
          icon="fa fa-pencil"
          text="Define.xml"
          minified={props.minified}
        >
          <NavItem to="/generate/excel" text="Create Spec" />
          <NavItem to="/generate/xml" text="Generate Define" />
        </SubMenu>
        <NavItem to="/converter" icon="fa fa-cogs" text="Converter" />
        <NavItem to="/miner" icon="fa fa-flask" text="ClinicalTrials.gov" />
        {/*{renderConnectButton()}*/}
        {/*{renderCancelOrDisconnectButton()}*/}
      </Nav>
      <div className="sidebar-minified">
        <i
          className={`fa fa-angle-${props.minified ? 'right' : 'left'}`}
          onClick={props.onClick}
        />
      </div>
      <BannerNotifications notifications={props.notifications} />
    </div>
  );
};

Sidebar.propTypes = propTypes;

export default withRouter(Sidebar);
