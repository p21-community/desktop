/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { Alert } from 'react-bootstrap';

const ConnectionStatus = props =>
  !props.online && (
    <Alert className="top-general-alert" bsStyle="warning">
      <b>No internet connection</b>
      <div>
        Reconnect to get the latest updates, configs, and notifications.
      </div>
    </Alert>
  );

export default ConnectionStatus;
