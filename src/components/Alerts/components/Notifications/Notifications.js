/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isEqual, some } from 'lodash';
import StandardNotification from './components/StandardNotification';

const propTypes = {
  ipcRenderer: PropTypes.object.isRequired,
  standardNotifications: PropTypes.array.isRequired
};

export default class Notifications extends Component {
  constructor(props) {
    super(props);

    this.state = {
      standardNotifications: []
    };
  }

  componentDidUpdate(prevProps) {
    if (
      !isEqual(
        prevProps.standardNotifications,
        this.props.standardNotifications
      )
    ) {
      this.setState({
        standardNotifications: this.props.standardNotifications.filter(
          notification => {
            return !some(prevProps.standardNotifications, notification);
          }
        )
      });
    }
  }

  render() {
    return (
      !!this.state.standardNotifications.length && (
        <StandardNotification
          notification={this.state.standardNotifications[0]}
        />
      )
    );
  }
}

Notifications.propTypes = propTypes;
