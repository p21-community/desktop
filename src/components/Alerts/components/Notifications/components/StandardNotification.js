/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';

export default class StandardNotification extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAlert: true
    };
  }

  onAlertDismiss = () => {
    this.setState({ showAlert: false });
  };

  render() {
    const { message, link, link_text } = this.props.notification;
    return (
      this.state.showAlert && (
        <Alert
          className="top-general-alert"
          bsStyle="info"
          onDismiss={this.onAlertDismiss}
        >
          <div>
            {message}{' '}
            <a rel="noopener noreferrer" target="_blank" href={link}>
              {link_text ? link_text : ''}
            </a>
          </div>
        </Alert>
      )
    );
  }
}
