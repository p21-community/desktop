/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'react-bootstrap';
import p21Img from '../../images/pinnacle21-community.png';

const propTypes = {
  ipcRenderer: PropTypes.object.isRequired
};

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = { show: false };
  }

  componentDidMount() {
    this.props.ipcRenderer.on('open-about-modal', this.showModal);
  }

  componentWillUnmount() {
    this.props.ipcRenderer.removeAllListeners('open-about-modal');
  }

  showModal = () => {
    this.setState({ show: true });
  };

  hideModal = () => {
    this.setState({ show: false });
  };

  render() {
    return (
      <Modal dialogClassName="about-modal" show={this.state.show}>
        <Modal.Header>
          <Modal.Title>About Pinnacle 21 Community</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div>
            <img src={p21Img} alt="Pinnacle 21 Community" />
            <p>Version: {window.app.getVersion()}</p>
            <p>
              Pinnacle 21 Community is free software licensed under the
              <a
                href="http://www.pinnacle21.com/license"
                rel="noopener noreferrer"
                target="_blank"
              >
                {' '}
                Pinnacle 21 Open Source Software License
              </a>
            </p>
            <p>
              <a
                href="http://www.pinnacle21.com/"
                rel="noopener noreferrer"
                target="_blank"
              >
                www.pinnacle21.com
              </a>
            </p>
            <p>
              &copy; 2008-
              {new Date().getFullYear()} Pinnacle 21 LLC
              <br />
              All Rights Reserved
            </p>
          </div>
        </Modal.Body>

        <Modal.Footer>
          <Button className="btn btn-primary" onClick={this.hideModal}>
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

About.propTypes = propTypes;
