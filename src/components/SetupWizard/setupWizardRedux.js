/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import { isEmpty, mapValues } from 'lodash';
import {
  hasNumber,
  validateEmail,
  validatePassword,
  trimValues
} from '../../utils/utils';
import PinnacleID from '../../utils/pinnacleIdRequests';
import {
  DISALLOWED_EMAILS,
  EU_COUNTRIES
} from '../../utils/formValidationConstants';
import { loadKey, saveKey, savePreferences } from '../../utils/localStorage';
import Logger from '../../utils/logger';

const { ipcRenderer, app } = window;
const pinnacleId = new PinnacleID(new Logger(ipcRenderer), app.getVersion());

//CONSTANTS
export const PINNACLE_ID_SCREENS = {
  LOGIN: 'LOGIN',
  REGISTER: 'REGISTER',
  COMMUNICATION_PREFS: 'COMMUNICATION_PREFS',
  ACCOUNT_VERIFICATION: 'ACCOUNT_VERIFICATION',
  ABOUT_PINNACLE_ID: 'ABOUT_PINNACLE_ID',
  SKIP_PINNACLE_ID: 'SKIP_PINNACLE_ID',
  FORGOT_PASSWORD: 'FORGOT_PASSWORD',
  CONFIRM_FORGOT_PASSWORD: 'CONFIRM_FORGOT_PASSWORD'
};
export const JOB_OPTIONS = [
  { value: 'sas_programmer', label: 'SAS Programmer' },
  { value: 'statistician', label: 'Statistician' },
  { value: 'data_manager', label: 'Data Manager' },
  { value: 'standards_manager', label: 'Standards Manager' },
  { value: 'other', label: 'Other' }
];

// ACTION CONSTANTS
const COMPLETE_SETUP = 'COMPLETE_SETUP';
const SET_PINNACLE_ID_SCREEN = 'SET_PINNACLE_ID_SCREEN';
const TOGGLE_SHOW_ABOUT_PINNACLE_ID = 'TOGGLE_SHOW_ABOUT_PINNACLE_ID';
const SET_FORM_FIELD_VALUE = 'SET_FORM_FIELD_VALUE';
const TOGGLE_SELF_EMPLOYED_MODAL = 'TOGGLE_SELF_EMPLOYED_MODAL';
const SET_FORM_ERRORS = 'SET_FORM_ERRORS';
const SET_IS_LOGGING_IN = 'SET_IS_LOGGING_IN';
const SET_IS_REGISTERING = 'SET_IS_REGISTERING';
const SET_IS_RESENDING = 'SET_IS_RESENDING';
const SET_CODE_DELIVERED_TO = 'SET_CODE_DELIVERED_TO';
const SET_VERIFICATION_CODE_DIGIT = 'SET_VERIFICATION_CODE_DIGIT';
const SET_IS_VERIFYING = 'SET_IS_VERIFYING';
const SET_IS_REQUESTING_FORGOT_PASSWORD = 'SET_IS_REQUESTING_FORGOT_PASSWORD';
const SET_IS_CONFIRMING_FORGOT_PASSWORD = 'SET_IS_CONFIRMING_FORGOT_PASSWORD';
const SET_IS_VERIFYING_BECAUSE_OF_REGISTRATION =
  'SET_IS_VERIFYING_BECAUSE_OF_REGISTRATION';
const LAUNCH_FROM_PREFERENCES = 'LAUNCH_FROM_PREFERENCES';
const RESET_REDUCER = 'RESET_REDUCER';

// UTILITY FUNCTIONS
const removeFormErrors = forms => {
  return mapValues(forms, form => {
    return { ...form, errors: {} };
  });
};

export const generateAppId = async tokens => {
  const res = await pinnacleId.generate(tokens);
  if (res.success) {
    const { data } = res;
    saveKey('app-id', data.id);
    return res;
  } else {
    return res;
  }
};
const assignTokensToAppId = async (tokens, appId) => {
  return await pinnacleId.assign(tokens, appId);
};
const loginToPinnacleId = async (email, password, onError) => {
  const res = await pinnacleId.login({
    email,
    password
  });
  if (res.success) {
    const { data } = res;
    savePreferences({ identity: data });
    const appId = loadKey('app-id');
    if (appId) {
      await assignTokensToAppId(data, appId);
    } else {
      await generateAppId(data);
    }
    // Now that they're logged in, let's update their configs
    ipcRenderer.send('updateConfigs');
  } else {
    onError(res.error);
  }
  return res;
};
const validateConfirmingForgotPasswordForm = form => {
  const errors = validateVerificationDigits(form.verificationDigits) || {};
  const requiredFieldMessage = 'This field is required';
  const requiredFields = ['password', 'verifiedPassword'];
  for (const field of requiredFields) {
    if (!form[field] || !form[field].length) {
      errors[field] = requiredFieldMessage;
    }
  }
  if (!isEmpty(errors)) {
    return errors;
  }
};

const validateForgotPasswordForm = form => {
  form = trimValues(form);
  const errors = {};
  if (!form.email || !form.email.length || !validateEmail(form.email)) {
    errors.email = 'Please enter a valid email address';
  }
  if (!isEmpty(errors)) {
    return errors;
  }
};
const validateRegisterForm = (form, nonWorkEmail) => {
  form = trimValues(form, ['password', 'verifiedPassword']);

  const errors = {};
  const requiredFieldMessage = 'This field is required';
  if (nonWorkEmail) {
    errors.nonWorkEmail = true;
  } else if (!form.email || !form.email.length) {
    errors.email = requiredFieldMessage;
  } else if (!validateEmail(form.email)) {
    errors.email = 'The email entered is not valid';
  }
  const justRequiredFields = [
    'firstName',
    'lastName',
    'job',
    'country',
    'password',
    'verifiedPassword'
  ];
  for (const field of justRequiredFields) {
    if (!form[field] || !form[field].length) {
      errors[field] = requiredFieldMessage;
    }
  }
  if (errors.firstName || errors.lastName) {
    errors.name = 'First and last name are required fields';
  }

  // Check for complexity, check if it matches
  if (form.password && form.password !== form.verifiedPassword) {
    errors.verifiedPassword = 'This field must match the password field';
  } else if (validatePassword(form.password)) {
    errors.password = validatePassword(form.password);
  }

  if (!isEmpty(errors)) {
    return errors;
  }
};
const validateLoginForm = form => {
  form = trimValues(form, ['password']);

  const errors = {};
  if (!form.email || !form.email.length) {
    errors.email = 'This field is required';
  } else if (!validateEmail(form.email)) {
    errors.email = 'The email entered is not valid';
  }
  if (!form.password || !form.password.length) {
    errors.password = 'This field is required';
  }
  if (!isEmpty(errors)) {
    return errors;
  }
};
const validateVerificationDigits = verificationDigits => {
  if (!verificationDigits || !verificationDigits.length) {
    return {
      form: 'Invalid verification code'
    };
  }
  const verificationCode = verificationDigits.join('');
  if (verificationCode.length < 6) {
    return {
      form: 'Please enter all 6 digits of the verification code'
    };
  }
  const isNumeric = /^\d+$/.test(verificationCode);
  if (!isNumeric) {
    return {
      form: 'Invalid verification code. Non-numeric characters detected.'
    };
  }
};

// ACTIONS
export const launchFromPreferences = () => ({
  type: LAUNCH_FROM_PREFERENCES
});
export const setVerificationFormDigit = (form, digit, keyPressed) => ({
  type: SET_VERIFICATION_CODE_DIGIT,
  form,
  digit,
  keyPressed
});
const setFormErrors = (form, errors) => ({
  type: SET_FORM_ERRORS,
  form,
  errors
});
export const setFormFieldValue = (form, field, value) => ({
  type: SET_FORM_FIELD_VALUE,
  form,
  field,
  value
});
export const resetSetupWizardReducer = () => ({
  type: RESET_REDUCER
});
export const toggleShowAboutPinnacleId = (show, skipPinnacleId) => ({
  type: TOGGLE_SHOW_ABOUT_PINNACLE_ID,
  show,
  skipPinnacleId: !!skipPinnacleId
});
export const toggleShowSelfEmployedModal = show => ({
  type: TOGGLE_SELF_EMPLOYED_MODAL,
  show
});
export const completeSetup = () => ({
  type: COMPLETE_SETUP,
  complete: true
});
export const setScreen = screen => ({
  type: SET_PINNACLE_ID_SCREEN,
  screen
});
export const setIsLoggingIn = isLoggingIn => ({
  type: SET_IS_LOGGING_IN,
  isLoggingIn
});
export const setIsRegistering = isRegistering => ({
  type: SET_IS_REGISTERING,
  isRegistering
});
export const setIsVerifying = isVerifying => ({
  type: SET_IS_VERIFYING,
  isVerifying
});
export const setIsVerifyingBecauseOfRegistration = isVerifyingBecauseOfRegistration => ({
  type: SET_IS_VERIFYING_BECAUSE_OF_REGISTRATION,
  isVerifyingBecauseOfRegistration
});

export const setIsResending = isResending => ({
  type: SET_IS_RESENDING,
  isResending
});
export const setIsRequestingForgotPassword = isRequesting => ({
  type: SET_IS_REQUESTING_FORGOT_PASSWORD,
  isRequesting
});
export const setIsConfirmingForgotPassword = isConfirming => ({
  type: SET_IS_CONFIRMING_FORGOT_PASSWORD,
  isConfirming
});
export const setCodeDeliveredTo = (form, codeDeliveredTo) => ({
  type: SET_CODE_DELIVERED_TO,
  form,
  codeDeliveredTo
});
export const resend = registrationForm => {
  return async dispatch => {
    dispatch(setIsResending(true));
    const res = await pinnacleId.resend({
      email: registrationForm.email
    });
    if (res.success) {
      dispatch(setCodeDeliveredTo('verification', registrationForm.email));
      dispatch(setIsResending(false));
      return res;
    } else {
      dispatch(
        setFormErrors('verification', {
          form: res.error
        })
      );
      dispatch(setIsResending(false));
      return res;
    }
  };
};
export const verify = (email, password, verificationDigits) => {
  return async dispatch => {
    dispatch(setIsVerifying(true));
    let validationErrors = validateVerificationDigits(verificationDigits);
    if (validationErrors) {
      dispatch(setFormErrors('verification', validationErrors));
      dispatch(setIsVerifying(false));
      return {
        success: false
      };
    }
    const res = await pinnacleId.confirmRegistration({
      email: email,
      code: verificationDigits.join('')
    });
    if (res.success) {
      // We actually need to log them in now to get our
      // precious tokens. I considered doing this without awaiting
      // to speed this up, but it's a bad situation
      // when it fails in that scenario
      return await loginToPinnacleId(email, password, () => {
        // Since it failed, let's send them back to the login screen to login
        dispatch(
          setFormErrors('login', {
            form:
              'Your account was confirmed, but login failed. Please login with your newly confirmed account.'
          })
        );
        dispatch(setIsVerifying(false));
        dispatch(setScreen(PINNACLE_ID_SCREENS.LOGIN));
      });
    } else {
      dispatch(setIsVerifying(false));
      if (res.error.indexOf('User is already confirmed') > -1) {
        return {
          success: true
        };
      } else {
        dispatch(
          setFormErrors('verification', {
            form: res.error
          })
        );
        return res;
      }
    }
  };
};

export const register = (form, nonWorkEmail) => {
  return async dispatch => {
    dispatch(setIsRegistering(true));
    // In theory, we shouldn't get here if the form was invalid...but let's check anyway
    let validationErrors = validateRegisterForm(form, nonWorkEmail);
    if (validationErrors) {
      dispatch(setFormErrors('register', validationErrors));
      dispatch(setIsRegistering(false));
      dispatch(setScreen(PINNACLE_ID_SCREENS.REGISTER));
      return {
        success: false
      };
    }
    // Ok, we should be good now. Let's attempt to Register with Pinnacle ID!
    const requestData = {
      firstName: form.firstName,
      lastName: form.lastName,
      email: form.email,
      password: form.password,
      job: form.job,
      country: form.country,
      newsletter: form.okToContact ? 1 : 0
    };
    const res = await pinnacleId.register(requestData);
    if (res.success) {
      dispatch(setCodeDeliveredTo('verification', requestData.email));
      dispatch(setIsRegistering(false));
      dispatch(setIsVerifyingBecauseOfRegistration(true));
      dispatch(setScreen(PINNACLE_ID_SCREENS.ACCOUNT_VERIFICATION));
      return res;
    } else {
      // Registering failed...let's send them back
      // to the login screen and let them know why
      dispatch(
        setFormErrors('login', {
          form: res.error
        })
      );
      dispatch(setIsRegistering(false));
      dispatch(setScreen(PINNACLE_ID_SCREENS.LOGIN));
    }
    return res;
  };
};
export const login = form => {
  return async dispatch => {
    dispatch(setIsLoggingIn(true));
    const validationErrors = validateLoginForm(form);
    if (validationErrors) {
      dispatch(setFormErrors('login', validationErrors));
      dispatch(setIsLoggingIn(false));
      return {
        success: false
      };
    } else {
      const loginResult = await loginToPinnacleId(
        form.email,
        form.password,
        async err => {
          if (err === 'User is not confirmed.') {
            const res = await pinnacleId.resend({
              email: form.email
            });
            dispatch(setIsLoggingIn(false));
            if (res.success) {
              dispatch(setIsVerifyingBecauseOfRegistration(false));
              const { data } = res;
              if (data.codeDeliveredTo === 'EMAIL') {
                dispatch(setCodeDeliveredTo('verification', form.email));
              } else {
                dispatch(setCodeDeliveredTo('verification', data.destination));
              }
              dispatch(setScreen(PINNACLE_ID_SCREENS.ACCOUNT_VERIFICATION));
            } else {
              if (res.error.indexOf('Too many requests') > -1) {
                dispatch(
                  setFormErrors('login', {
                    form:
                      'Your account is not confirmed and too many requests have been made. Please try again later.'
                  })
                );
              } else {
                dispatch(
                  setFormErrors('login', {
                    form:
                      'Your account is not confirmed, and an error occurred while attempting to send you a verification code. Please contact support.'
                  })
                );
              }
            }
          } else {
            dispatch(setFormErrors('login', { form: err }));
            dispatch(setIsLoggingIn(false));
          }
        }
      );
      if (loginResult.success) {
        dispatch(setIsLoggingIn(false));
      }
      return loginResult;
    }
  };
};
export const registerNext = (form, nonWorkEmail) => {
  return dispatch => {
    const validationErrors = validateRegisterForm(form, nonWorkEmail);
    if (validationErrors) {
      dispatch(setFormErrors('register', validationErrors));
    } else {
      dispatch(setScreen(PINNACLE_ID_SCREENS.COMMUNICATION_PREFS));
    }
  };
};

export const forgotPasswordNext = form => {
  return async dispatch => {
    dispatch(setIsRequestingForgotPassword(true));
    const validationErrors = validateForgotPasswordForm(form);
    if (validationErrors) {
      dispatch(setFormErrors('forgotPassword', validationErrors));
      dispatch(setIsRequestingForgotPassword(false));
      return {
        success: false
      };
    } else {
      const res = await pinnacleId.sendForgotPassword(form.email);
      dispatch(setIsRequestingForgotPassword(false));
      if (res.success) {
        const { data } = res;
        if (data.codeDeliveredTo === 'SMS') {
          dispatch(setCodeDeliveredTo('forgotPassword', data.destination));
        } else {
          dispatch(setCodeDeliveredTo('forgotPassword', form.email));
        }
        dispatch(setScreen(PINNACLE_ID_SCREENS.CONFIRM_FORGOT_PASSWORD));
      } else {
        dispatch(
          setFormErrors('forgotPassword', {
            form: res.error
          })
        );
      }
      return res;
    }
  };
};

export const confirmForgotPasswordNext = form => {
  return async dispatch => {
    dispatch(setIsConfirmingForgotPassword(true));
    const validationErrors = validateConfirmingForgotPasswordForm(form);
    if (validationErrors) {
      dispatch(setFormErrors('confirmForgotPassword', validationErrors));
      dispatch(setIsConfirmingForgotPassword(false));
      return {
        success: false
      };
    } else {
      const res = await pinnacleId.confirmForgotPassword({
        email: form.email,
        code: form.verificationDigits.join(''),
        password: form.password,
        confirmedPassword: form.verifiedPassword
      });
      dispatch(setIsConfirmingForgotPassword(false));
      if (res.success) {
        dispatch(setScreen(PINNACLE_ID_SCREENS.LOGIN));
      } else {
        dispatch(
          setFormErrors('confirmForgotPassword', {
            form: res.error
          })
        );
      }
      return res;
    }
  };
};

const getInitialState = () => ({
  complete: false,
  cameFromPreferences: false,
  pinnacleIdScreen: PINNACLE_ID_SCREENS.LOGIN,
  showAboutPinnacleId: false,
  skipPinnacleId: false,
  showSelfEmployedModal: false,
  nonWorkEmail: false,
  isLoggingIn: false,
  isRegistering: false,
  isResending: false,
  isVerifying: false,
  isRequestingForgotPassword: false,
  isConfirmingForgotPassword: false,
  isVerifyingBecauseOfRegistration: true,
  forms: {
    login: {
      email: '',
      password: '',
      errors: {}
    },
    register: {
      firstName: '',
      lastName: '',
      email: '',
      job: null,
      country: null,
      password: '',
      verifiedPassword: '',
      okToContact: true,
      errors: {}
    },
    verification: {
      codeDeliveredTo: null,
      verificationDigits: ['', '', '', '', '', ''],
      errors: {}
    },
    forgotPassword: {
      email: '',
      errors: {},
      codeDeliveredTo: null
    },
    confirmForgotPassword: {
      email: '',
      password: '',
      verifiedPassword: '',
      verificationDigits: ['', '', '', '', '', ''],
      errors: {}
    }
  }
});
// INITIAL STATE
const initialState = getInitialState();

// REDUCER
const setupWizardReducer = (state = initialState, action) => {
  switch (action.type) {
    case LAUNCH_FROM_PREFERENCES: {
      const resetState = getInitialState();
      resetState.cameFromPreferences = true;
      return resetState;
    }
    case COMPLETE_SETUP: {
      return {
        ...state,
        complete: action.complete,
        cameFromPreferences: false
      };
    }
    case SET_CODE_DELIVERED_TO: {
      const forms = { ...state.forms };
      const form = forms[action.form];
      form.codeDeliveredTo = action.codeDeliveredTo;
      return {
        ...state,
        forms
      };
    }
    case SET_IS_VERIFYING_BECAUSE_OF_REGISTRATION: {
      return {
        ...state,
        isVerifyingBecauseOfRegistration:
          action.isVerifyingBecauseOfRegistration
      };
    }
    case SET_IS_LOGGING_IN: {
      return { ...state, isLoggingIn: action.isLoggingIn };
    }
    case SET_IS_REQUESTING_FORGOT_PASSWORD: {
      return { ...state, isRequestingForgotPassword: action.isRequesting };
    }
    case SET_IS_CONFIRMING_FORGOT_PASSWORD: {
      return { ...state, isConfirmingForgotPassword: action.isConfirming };
    }
    case SET_IS_REGISTERING: {
      return { ...state, isRegistering: action.isRegistering };
    }
    case SET_IS_VERIFYING: {
      return { ...state, isVerifying: action.isVerifying };
    }
    case SET_IS_RESENDING: {
      return { ...state, isResending: action.isResending };
    }
    case SET_PINNACLE_ID_SCREEN: {
      if (
        state.isLoggingIn ||
        state.isRegistering ||
        state.isVerifying ||
        state.isResending ||
        state.isRequestingForgotPassword
      ) {
        // We don't want to change the screen while any of these things are happening!
        return { ...state };
      }
      const newState = { ...state };
      newState.pinnacleIdScreen = action.screen;
      if (
        state.screen !== action.screen &&
        action.screen !== PINNACLE_ID_SCREENS.LOGIN
      ) {
        newState.forms = removeFormErrors(newState.forms);
      }
      switch (action.screen) {
        case PINNACLE_ID_SCREENS.LOGIN: {
          newState.forms.forgotPassword = getInitialState().forms.forgotPassword;
          return newState;
        }
        case PINNACLE_ID_SCREENS.FORGOT_PASSWORD: {
          newState.forms.forgotPassword.email = state.forms.login.email;
          newState.forms.confirmForgotPassword = getInitialState().forms.confirmForgotPassword;
          newState.forms.confirmForgotPassword.email =
            newState.forms.forgotPassword.email;
          return newState;
        }
        default: {
          if (
            (state.pinnacleIdScreen === PINNACLE_ID_SCREENS.FORGOT_PASSWORD ||
              state.pinnacleIdScreen ===
                PINNACLE_ID_SCREENS.CONFIRM_FORGOT_PASSWORD) &&
            action.screen !== PINNACLE_ID_SCREENS.CONFIRM_FORGOT_PASSWORD
          ) {
            newState.forms.forgotPassword = getInitialState().forms.forgotPassword;
            newState.forms.confirmForgotPassword = getInitialState().forms.confirmForgotPassword;
          }
          return newState;
        }
      }
    }
    case TOGGLE_SHOW_ABOUT_PINNACLE_ID: {
      return {
        ...state,
        showAboutPinnacleId: action.show,
        skipPinnacleId: action.skipPinnacleId
      };
    }
    case SET_VERIFICATION_CODE_DIGIT: {
      const forms = { ...state.forms };
      const form = forms[action.form];
      const digits = form.verificationDigits;
      if (hasNumber(action.keyPressed)) {
        digits[action.digit - 1] = action.keyPressed;
        form.errors = {};
      }
      if (action.keyPressed === 'Backspace' || action.keyPressed === 'Delete') {
        digits[action.digit - 1] = '';
        form.errors = {};
      }
      return {
        ...state,
        forms
      };
    }
    case SET_FORM_ERRORS: {
      const forms = { ...state.forms };
      const form = forms[action.form];
      form.errors = action.errors;
      return {
        ...state,
        forms
      };
    }
    case SET_FORM_FIELD_VALUE: {
      const forms = { ...state.forms };
      const form = forms[action.form];
      form[action.field] = action.value;
      form.errors[action.field] = null;
      form.errors.form = null;
      if (action.form === 'register') {
        if (action.field === 'email') {
          if (typeof action.value === 'string' && validateEmail(action.value)) {
            const emailDomain = action.value.substring(
              action.value.lastIndexOf('@') + 1
            );
            if (DISALLOWED_EMAILS.has(emailDomain.toLowerCase())) {
              return {
                ...state,
                nonWorkEmail: true,
                forms
              };
            }
          }
          return {
            ...state,
            nonWorkEmail: false,
            forms
          };
        }
        if (action.field === 'country') {
          if (EU_COUNTRIES.indexOf(action.value.toUpperCase()) > -1) {
            form.okToContact = false;
          }
        }
        if (action.field === 'firstName' || action.field === 'lastName') {
          if (
            form.firstName &&
            form.firstName.length &&
            form.lastName &&
            form.lastName.length
          ) {
            form.errors.name = null;
          }
        }
      }
      return {
        ...state,
        forms
      };
    }
    case TOGGLE_SELF_EMPLOYED_MODAL: {
      return { ...state, showSelfEmployedModal: action.show };
    }
    case RESET_REDUCER: {
      const resetState = getInitialState();
      resetState.cameFromPreferences = state.cameFromPreferences;
      return resetState;
    }
    default: {
      return state;
    }
  }
};

export default setupWizardReducer;
