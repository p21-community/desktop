/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';

const Complete = () => {
  return (
    <div>
      <h3>Completed Pinnacle 21 Community Setup</h3>
      <p>
        Pinnacle 21 Community has been successfully set up on your computer.
      </p>
      <p>
        Click <b>Finish</b> to start using the application.
      </p>
    </div>
  );
};

export default Complete;
