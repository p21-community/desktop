/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';

export const Benefits = ({ showAsList }) => {
  const Benefit = ({ header, detail }) => {
    const benefit = (
      <div>
        <span className="benefit-header">
          {header}
          {showAsList && <span className="benefit-separator"> - </span>}
        </span>
        {showAsList ? (
          <span className="benefit-detail">{detail}</span>
        ) : (
          <div className="benefit-detail">{detail}</div>
        )}
      </div>
    );
    return showAsList ? (
      <li className="benefit">{benefit}</li>
    ) : (
      <span className="benefit">{benefit}</span>
    );
  };
  const benefits = [
    <Benefit
      key={1}
      header="Forum"
      detail="Get your questions answered quickly by the community and Pinnacle 21 experts"
    />,
    <Benefit
      key={2}
      header="Webinars"
      detail="Register with one click for our educational webinars"
    />,
    <Benefit
      key={3}
      header="Terminology Updates"
      detail="Receive automatic updates to the latest controlled terminology releases"
    />
  ];
  return (
    <div className="pinnacle-id-benefits">
      {showAsList && (
        <div className="pinnacle-id-benefits-header">
          Your Pinnacle ID is the account you use to access all Pinnacle 21
          services. With a single email and password you have access to the
          following:
        </div>
      )}
      {showAsList ? <ul>{benefits}</ul> : <div>{benefits}</div>}
    </div>
  );
};

const ButtonOption = ({ buttonText, subText, onClick }) => (
  <div className="button-option">
    <Button onClick={onClick}>{buttonText}</Button>
    <div>{subText}</div>
  </div>
);

const AboutPinnacleId = ({ skipping, cancel, skip }) => (
  <Row className="row-eq-height about-pinnacle-id">
    <Col sm={12}>
      {!skipping && (
        <div className="close-div">
          <button type="button" className="close" onClick={cancel}>
            <span aria-hidden="true">×</span>
            <span className="sr-only">Close</span>
          </button>
        </div>
      )}
      <h4>{skipping ? 'Are you sure?' : 'What is a Pinnacle ID?'}</h4>
      {skipping && (
        <h4 className="skipping-subheader">
          By skipping Pinnacle ID, you are missing out on some great benefits!
        </h4>
      )}
      <Benefits showAsList />
      {skipping && (
        <div className="skipping-buttons">
          <h4 className="skipping-subheader">
            You can create a Pinnacle ID at any time by going to Preferences
          </h4>
          <div>
            <ButtonOption
              buttonText="I changed my mind"
              subText="Create Pinnacle ID Now"
              onClick={cancel}
            />
            <ButtonOption
              buttonText="Yes, I'm sure"
              subText="Create Pinnacle ID Later"
              onClick={skip}
            />
          </div>
        </div>
      )}
    </Col>
  </Row>
);

export default AboutPinnacleId;
