/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { Form, FormGroup, Col, FormControl, HelpBlock } from 'react-bootstrap';

const onEnterPress = e => {
  if (e.keyCode === 13 && e.shiftKey === false) {
    e.preventDefault();
  }
};
const ForgotPassword = props => {
  const { form, isRequesting, setFormFieldValue, showAbout } = props;
  return (
    <div className="login forgot-password">
      <div className="pinnacle-id-header">
        <h3>Forgot Password</h3>
        <a onClick={showAbout}>What is a Pinnacle ID?</a>
      </div>
      <div className="main-content">
        <div className="widget">
          <div className="widget-content">
            <Form horizontal>
              <div>
                <p>
                  Please enter the email address associated with your Pinnacle
                  ID
                </p>
              </div>
              <div className="login-field">
                <FormGroup
                  controlId="formHorizontalEmail"
                  validationState={form.errors.email && 'error'}
                >
                  <Col sm={12}>
                    <FormControl
                      type="email"
                      autoFocus
                      disabled={isRequesting}
                      onKeyDown={onEnterPress}
                      onChange={e => {
                        setFormFieldValue(
                          'forgotPassword',
                          'email',
                          e.target.value
                        );
                        setFormFieldValue(
                          'confirmForgotPassword',
                          'email',
                          e.target.value
                        );
                      }}
                      onFocus={function(e) {
                        const val = e.target.value;
                        e.target.value = '';
                        e.target.value = val;
                      }}
                      value={form.email}
                    />
                  </Col>
                </FormGroup>
                <div className="form-input-footer">
                  {form.errors.email && (
                    <span className="has-error">
                      <FormControl.Feedback />
                      <HelpBlock>{form.errors.email}</HelpBlock>
                    </span>
                  )}
                </div>
              </div>
              {form.errors.form && (
                <div className="has-error">
                  <p>{form.errors.form}</p>
                </div>
              )}
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
