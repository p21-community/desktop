/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { Form } from 'react-bootstrap';
import { hasNumber } from '../../../../../utils/utils';

const VerificationDigitInput = props => (
  <div className="field-wrap">
    <input
      ref={props.refCallback}
      id={`digit${props.digit}`}
      value={props.value}
      className="char-field"
      aria-label={`Digit ${props.digit}`}
      placeholder=""
      autoFocus={props.autoFocus}
      onKeyDown={props.onKeyDown}
      disabled={props.disabled}
    />
  </div>
);

export class VerificationForm extends Component {
  constructor(props) {
    super(props);
    this.digitInputs = [];
    this.state = {
      successfulResend: false,
      error: null
    };
  }

  resend = async () => {
    this.setState({ successfulResend: false });
    const res = await this.props.resend();
    if (res) {
      if (res.success) {
        this.setState({ successfulResend: true });
        setTimeout(() => this.setState({ successfulResend: false }), 5000);
      } else {
        this.setState({ error: res.error });
      }
    }
  };

  renderStatus = () => {
    const statuses = [];
    if (this.state.successfulResend) {
      statuses.push(
        <p key={1} className="success">
          Resent verification code!
        </p>
      );
    }
    if (this.props.form.errors.form) {
      statuses.push(
        <p key={2} className="error">
          {this.props.form.errors.form}
        </p>
      );
    } else if (this.state.error) {
      statuses.push(
        <p key={2} className="error">
          {this.state.error}
        </p>
      );
    }
    return statuses;
  };

  render() {
    const {
      form,
      isResending,
      setVerificationFormDigit,
      precontent,
      autoFocus,
      disabled
    } = this.props;
    return (
      <div className="widget">
        <div className="widget-content">
          {precontent || (
            <div>
              <p>
                A verification code has been sent to{' '}
                <b>{form.codeDeliveredTo}</b>
              </p>
              <p>Please enter the code below.</p>
              <p>
                Didn't get the code? Check your spam folder.
                <br />
                {isResending ? (
                  <i className="fa fa-spinner fa-pulse fa-fw" />
                ) : (
                  <a onClick={this.resend}>Resend verification code</a>
                )}
              </p>
            </div>
          )}
          <Form>
            <div id="2fa">
              <div id="security-code-container">
                {[1, 2, 3, 4, 5, 6].map(x => (
                  <VerificationDigitInput
                    refCallback={ref => this.digitInputs.push(ref)}
                    key={x}
                    disabled={disabled}
                    autoFocus={autoFocus && x === 1}
                    digit={x}
                    value={form.verificationDigits[x - 1]}
                    digits={form.verificationDigits}
                    onKeyDown={e => {
                      if (x < 6) {
                        if (hasNumber(e.key)) {
                          this.digitInputs[x].focus();
                        }
                      }
                      setVerificationFormDigit(x, e.key);
                    }}
                  />
                ))}
              </div>
              <div className="status">{this.renderStatus()}</div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

const VerifyAccount = props => (
  <div className="password-recovery verify-account">
    <div className="pinnacle-id-header">
      <h3>Create Your Pinnacle ID</h3>
      <a onClick={props.showAbout}>What is a Pinnacle ID?</a>
    </div>
    <div className="main-content">
      <VerificationForm {...props} autoFocus />
      <div className="main-content-footer">
        <a
          href="https://www.pinnacle21.com/privacy"
          target="_blank"
          rel="noopener noreferrer"
        >
          Privacy Policy
        </a>
      </div>
    </div>
  </div>
);

export default VerifyAccount;
