/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { Form, Checkbox } from 'react-bootstrap';

const CommunicationPrefs = props => {
  return (
    <div className="password-recovery communication-prefs">
      <div className="pinnacle-id-header">
        <h3>Create Your Pinnacle ID</h3>
        <a onClick={props.showAbout}>What is a Pinnacle ID?</a>
      </div>
      <div className="main-content">
        <div className="widget">
          <div className="widget-content">
            <h3>Communication Preferences</h3>
            <div>
              <p>
                Pinnacle 21 will only use the information you provide on this
                form to be in touch with you about software updates, webinars
                and events. Please confirm that you'd like to stay in touch.
              </p>
              <p>
                You can update this at any time by going to Preferences ->
                Pinnacle ID
              </p>
            </div>
            <Form>
              <Checkbox
                checked={props.form.okToContact}
                autoFocus
                onChange={e =>
                  props.setFormFieldValue(
                    'register',
                    'okToContact',
                    e.target.checked
                  )
                }
              >
                Yes, let's stay in touch
              </Checkbox>
            </Form>
          </div>
        </div>
        <div className="main-content-footer">
          <a
            href="https://www.pinnacle21.com/privacy"
            target="_blank"
            rel="noopener noreferrer"
          >
            Privacy Policy
          </a>
        </div>
      </div>
    </div>
  );
};

export default CommunicationPrefs;
