/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import {
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  HelpBlock
} from 'react-bootstrap';
import { validatePassword } from '../../../../../utils/utils';

const PasswordInput = props => {
  const getValidationState = password => {
    if (!password || !password.length) {
      return;
    }
    if (!validatePassword(password)) {
      return 'success';
    } else {
      return 'error';
    }
  };
  const getValidationText = password => {
    if (password) {
      return validatePassword(password);
    }
    return null;
  };
  let initGetValidationState = getValidationState(props.password);
  let validationState = initGetValidationState;
  if (!validationState && props.error) {
    validationState = 'error';
  }
  let errorMessage = getValidationText(props.password);
  if (!errorMessage && props.error) {
    errorMessage = props.error;
  }

  return props.columns ? (
    <FormGroup
      controlId="formHorizontalPassword"
      validationState={validationState}
    >
      <Col componentClass={ControlLabel} sm={3}>
        Password:
      </Col>
      <Col sm={6}>
        <FormControl
          autoFocus={props.autoFocus}
          disabled={props.disabled}
          value={props.password}
          placeholder={props.placeholder}
          onChange={props.onChange}
          type="password"
        />
        {initGetValidationState && <FormControl.Feedback />}
        {errorMessage && <HelpBlock>{errorMessage}</HelpBlock>}
      </Col>
      <Col sm={3} className="form-hint" />
    </FormGroup>
  ) : (
    <FormGroup
      controlId="formHorizontalPassword"
      validationState={validationState}
    >
      <FormControl
        autoFocus={props.autoFocus}
        disabled={props.disabled}
        value={props.password}
        placeholder={props.placeholder}
        onChange={props.onChange}
        type="password"
      />
      {initGetValidationState && <FormControl.Feedback />}
      {errorMessage && <HelpBlock>{errorMessage}</HelpBlock>}
    </FormGroup>
  );
};

export default PasswordInput;
