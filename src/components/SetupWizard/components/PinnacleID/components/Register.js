/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import {
  Form,
  FormGroup,
  Col,
  ControlLabel,
  FormControl,
  Modal,
  HelpBlock
} from 'react-bootstrap';
import { JOB_OPTIONS } from '../../../setupWizardRedux';
import { COUNTRIES } from '../../../../../utils/formValidationConstants';
import Select from 'react-select';
import PasswordInput from './PasswordInput';

const SelfEmployedModal = props => (
  <Modal
    show={props.show}
    container={props.container}
    backdrop={false}
    keyboard
    autoFocus
    restoreFocus
    enforceFocus={false}
    bsSize="small"
    dialogClassName="self-employed-modal"
    onHide={props.close}
  >
    <Modal.Header closeButton>
      <h4>Self Employed?</h4>
    </Modal.Header>
    <Modal.Body>
      <p>
        Please email{' '}
        <a href="mailto:support@pinnacle21.com">support@pinnacle21.com</a> to
        request a Pinnacle ID. For now, please select <b>"Skip for now"</b>{' '}
        below.
      </p>
    </Modal.Body>
  </Modal>
);

// This is a component because 'this' is used
// to declare the component as a container
// for a modal
class Register extends Component {
  render() {
    return (
      <div className="register modal-container">
        <div className="pinnacle-id-header">
          <h3>Create Your Pinnacle ID</h3>
          <a onClick={this.props.showAbout}>What is a Pinnacle ID?</a>
        </div>
        <div className="main-content">
          <div className="widget">
            <div className="widget-content">
              <Form horizontal>
                <div className="login-field">
                  <FormGroup
                    controlId="formHorizontalEmail"
                    validationState={this.props.form.errors.name && 'error'}
                  >
                    <Col componentClass={ControlLabel} sm={3}>
                      Name:
                    </Col>
                    <Col sm={6} className="login-name">
                      <FormControl
                        value={this.props.form.firstName}
                        onChange={e =>
                          this.props.setFormFieldValue(
                            'register',
                            'firstName',
                            e.target.value
                          )
                        }
                        placeholder="First"
                      />
                      <FormControl
                        value={this.props.form.lastName}
                        onChange={e =>
                          this.props.setFormFieldValue(
                            'register',
                            'lastName',
                            e.target.value
                          )
                        }
                        placeholder="Last"
                      />
                    </Col>
                    {this.props.form.errors.name && (
                      <span>
                        <HelpBlock>{this.props.form.errors.name}</HelpBlock>
                      </span>
                    )}
                    <Col sm={3} className="form-hint" />
                  </FormGroup>
                </div>
                <div className="login-field">
                  <FormGroup
                    controlId="formHorizontalEmail"
                    validationState={
                      (this.props.nonWorkEmail ||
                        this.props.form.errors.email) &&
                      'error'
                    }
                  >
                    <Col componentClass={ControlLabel} sm={3}>
                      Work Email:
                    </Col>
                    <Col sm={6}>
                      <FormControl
                        value={this.props.form.email}
                        placeholder="This will be your Pinnacle ID"
                        onChange={e =>
                          this.props.setFormFieldValue(
                            'register',
                            'email',
                            e.target.value
                          )
                        }
                        type="email"
                      />
                      {this.props.nonWorkEmail && (
                        <div className="input-msg">
                          <div className="error">
                            Non-work email detected. To help prevent fake
                            accounts, we kindly ask that you provide your work
                            email.
                          </div>
                          <a
                            onClick={() =>
                              this.props.toggleShowSelfEmployedModal(true)
                            }
                          >
                            Self Employed? Click Here.
                          </a>
                        </div>
                      )}
                      {!this.props.nonWorkEmail &&
                        this.props.form.errors.email && (
                          <div className="input-msg">
                            <div className="error">
                              {this.props.form.errors.email}
                            </div>
                          </div>
                        )}
                    </Col>
                    <Col sm={3} className="form-hint" />
                  </FormGroup>
                </div>
                <div className="login-field">
                  <FormGroup
                    controlId="formHorizontalEmail"
                    validationState={this.props.form.errors.job && 'error'}
                  >
                    <Col componentClass={ControlLabel} sm={3}>
                      Job:
                    </Col>
                    <Col sm={6}>
                      <Select
                        options={JOB_OPTIONS}
                        value={this.props.form.job}
                        onChange={opt => {
                          if (opt && opt.value) {
                            this.props.setFormFieldValue(
                              'register',
                              'job',
                              opt.value
                            );
                          }
                        }}
                        clearable={false}
                        backspaceRemoves={false}
                      />
                      {this.props.form.errors.job && (
                        <HelpBlock>{this.props.form.errors.job}</HelpBlock>
                      )}
                    </Col>
                    <Col sm={3} className="form-hint" />
                  </FormGroup>
                </div>
                <div className="login-field">
                  <FormGroup
                    controlId="formHorizontalEmail"
                    validationState={this.props.form.errors.country && 'error'}
                  >
                    <Col componentClass={ControlLabel} sm={3}>
                      Country:
                    </Col>
                    <Col sm={6}>
                      <Select
                        options={COUNTRIES}
                        value={this.props.form.country}
                        onChange={opt => {
                          if (opt && opt.value) {
                            this.props.setFormFieldValue(
                              'register',
                              'country',
                              opt.value
                            );
                          }
                        }}
                        clearable={false}
                        backspaceRemoves={false}
                      />
                      {this.props.form.errors.country && (
                        <HelpBlock>{this.props.form.errors.country}</HelpBlock>
                      )}
                    </Col>
                    <Col sm={3} className="form-hint" />
                  </FormGroup>
                </div>
                <div className="login-field">
                  <PasswordInput
                    columns
                    password={this.props.form.password}
                    error={this.props.form.errors.password}
                    onChange={e =>
                      this.props.setFormFieldValue(
                        'register',
                        'password',
                        e.target.value
                      )
                    }
                  />
                  <FormGroup
                    controlId="formHorizontalPassword"
                    className="verify-password"
                    validationState={
                      this.props.form.errors.verifiedPassword && 'error'
                    }
                  >
                    <Col xsOffset={3} sm={6}>
                      <FormControl
                        value={this.props.form.verifiedPassword}
                        onChange={e =>
                          this.props.setFormFieldValue(
                            'register',
                            'verifiedPassword',
                            e.target.value
                          )
                        }
                        type="password"
                        placeholder="Verify Password"
                      />
                      {this.props.form.errors.verifiedPassword && (
                        <HelpBlock>
                          {this.props.form.errors.verifiedPassword}
                        </HelpBlock>
                      )}
                    </Col>
                    <Col sm={3} className="form-hint" />
                  </FormGroup>
                </div>
              </Form>
            </div>
          </div>
          <div className="main-content-footer">
            <a
              href="https://www.pinnacle21.com/privacy"
              target="_blank"
              rel="noopener noreferrer"
            >
              Privacy Policy
            </a>
          </div>
        </div>
        <SelfEmployedModal
          show={this.props.showSelfEmployedModal}
          close={() => this.props.toggleShowSelfEmployedModal(false)}
          container={this}
        />
      </div>
    );
  }
}

export default Register;
