/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { FormGroup, FormControl, HelpBlock } from 'react-bootstrap';
import { VerificationForm } from './VerifyAccount';
import PasswordInput from './PasswordInput';

export const PasswordFields = props => {
  return (
    <div>
      <div className="confirmation-password-fields">
        <PasswordInput
          autoFocus
          disabled={props.disabled}
          password={props.form.password}
          placeholder="Password"
          error={props.form.errors.password}
          onChange={e =>
            props.setFormFieldValue(
              'confirmForgotPassword',
              'password',
              e.target.value
            )
          }
        />
        <FormGroup
          controlId="formHorizontalPassword"
          className="verify-password"
          validationState={props.form.errors.verifiedPassword && 'error'}
        >
          <FormControl
            value={props.form.verifiedPassword}
            onChange={e =>
              props.setFormFieldValue(
                'confirmForgotPassword',
                'verifiedPassword',
                e.target.value
              )
            }
            type="password"
            disabled={props.disabled}
            placeholder="Verify Password"
          />
          {props.form.errors.verifiedPassword && (
            <HelpBlock>{props.form.errors.verifiedPassword}</HelpBlock>
          )}
        </FormGroup>
      </div>
    </div>
  );
};

const ConfirmForgotPassword = props => {
  const {
    form,
    codeDeliveredTo,
    isVerifying,
    setFormFieldValue,
    setVerificationFormDigit
  } = props;
  return (
    <div className="password-recovery verify-account">
      <h3>Set New Password</h3>
      <div className="main-content">
        <VerificationForm
          form={form}
          disabled={isVerifying}
          setVerificationFormDigit={setVerificationFormDigit}
          precontent={
            <div className="confirm-forgot-password-content">
              <p>Please set your password.</p>
              <br />
              <PasswordFields
                form={form}
                disabled={isVerifying}
                setFormFieldValue={setFormFieldValue}
              />
              <p>
                Please enter the verification code sent to{' '}
                <b>{codeDeliveredTo}</b>.
              </p>
            </div>
          }
        />
      </div>
    </div>
  );
};

export default ConfirmForgotPassword;
