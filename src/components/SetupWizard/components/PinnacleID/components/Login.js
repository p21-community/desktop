/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { PINNACLE_ID_SCREENS } from '../../../setupWizardRedux';
import {
  Form,
  FormGroup,
  Col,
  ControlLabel,
  FormControl,
  HelpBlock
} from 'react-bootstrap';

const Login = props => {
  const {
    setScreen,
    showAbout,
    form,
    isLoggingIn,
    setFormFieldValue,
    login
  } = props;
  const handleRegister = () => {
    setScreen(PINNACLE_ID_SCREENS.REGISTER);
  };
  const onEnterPress = e => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault();
      login();
    }
  };
  return (
    <div className="login">
      <div className="pinnacle-id-header">
        <h3>Login with Pinnacle ID</h3>
        <a onClick={showAbout}>What is a Pinnacle ID?</a>
      </div>
      <div className="main-content">
        <div className="widget">
          <div className="widget-content">
            <Form horizontal>
              <div className="login-field">
                <FormGroup
                  controlId="formHorizontalEmail"
                  validationState={form.errors.email && 'error'}
                >
                  <Col componentClass={ControlLabel} sm={2}>
                    Email:
                  </Col>
                  <Col sm={10}>
                    <FormControl
                      type="email"
                      autoFocus
                      disabled={isLoggingIn}
                      onKeyDown={onEnterPress}
                      onChange={e =>
                        setFormFieldValue('login', 'email', e.target.value)
                      }
                      value={form.email}
                    />
                  </Col>
                </FormGroup>
                <div className="form-input-footer">
                  {form.errors.email && (
                    <span className="has-error">
                      <FormControl.Feedback />
                      <HelpBlock>{form.errors.email}</HelpBlock>
                    </span>
                  )}
                  <a onClick={handleRegister}>Create new Pinnacle ID</a>
                </div>
              </div>
              <div className="login-field">
                <FormGroup
                  controlId="formHorizontalPassword"
                  validationState={form.errors.password && 'error'}
                >
                  <Col componentClass={ControlLabel} sm={2}>
                    Password:
                  </Col>
                  <Col sm={10}>
                    <FormControl
                      type="password"
                      disabled={isLoggingIn}
                      onKeyDown={onEnterPress}
                      onChange={e =>
                        setFormFieldValue('login', 'password', e.target.value)
                      }
                      value={form.password}
                    />
                  </Col>
                </FormGroup>
                <div className="form-input-footer">
                  {form.errors.password && (
                    <span className="has-error">
                      <FormControl.Feedback />
                      <HelpBlock>{form.errors.password}</HelpBlock>
                    </span>
                  )}
                  <a
                    onClick={() =>
                      setScreen(PINNACLE_ID_SCREENS.FORGOT_PASSWORD)
                    }
                  >
                    Forgot Password
                  </a>
                </div>
              </div>
              {form.errors.form && (
                <div className="has-error">
                  <p>{form.errors.form}</p>
                </div>
              )}
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
