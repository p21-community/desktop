/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import {
  PINNACLE_ID_SCREENS,
  toggleShowSelfEmployedModal,
  setFormFieldValue,
  resend,
  setVerificationFormDigit
} from '../../setupWizardRedux';
import Login from './components/Login';
import Register from './components/Register';
import CommunicationPrefs from './components/CommunicationPrefs';
import VerifyAccount from './components/VerifyAccount';
import ForgotPassword from './components/ForgotPassword';
import ConfirmForgotPassword from './components/ConfirmForgotPassword';

const PinnacleID = props => {
  switch (props.pinnacleIdScreen) {
    case PINNACLE_ID_SCREENS.REGISTER:
      return (
        <Register
          form={props.forms.register}
          nonWorkEmail={props.nonWorkEmail}
          showSelfEmployedModal={props.showSelfEmployedModal}
          setFormFieldValue={props.setFormFieldValue}
          toggleShowSelfEmployedModal={props.toggleShowSelfEmployedModal}
          showAbout={() => props.toggleShowAboutPinnacleId(true, false)}
          setScreen={props.setScreen}
        />
      );
    case PINNACLE_ID_SCREENS.COMMUNICATION_PREFS:
      return (
        <CommunicationPrefs
          form={props.forms.register}
          showAbout={() => props.toggleShowAboutPinnacleId(true, false)}
          setFormFieldValue={props.setFormFieldValue}
          setScreen={props.setScreen}
        />
      );
    case PINNACLE_ID_SCREENS.ACCOUNT_VERIFICATION:
      const form = props.isVerifyingBecauseOfRegistration
        ? props.forms.register
        : props.forms.login;
      return (
        <VerifyAccount
          form={props.forms.verification}
          showAbout={() => props.toggleShowAboutPinnacleId(true, false)}
          resend={() => props.resend(form)}
          isResending={props.isResending}
          disabled={props.isVerifying}
          setFormFieldValue={props.setFormFieldValue}
          setVerificationFormDigit={(digit, keyPressed) =>
            props.setVerificationFormDigit('verification', digit, keyPressed)
          }
          setScreen={props.setScreen}
        />
      );
    case PINNACLE_ID_SCREENS.FORGOT_PASSWORD:
      return (
        <ForgotPassword
          form={props.forms.forgotPassword}
          isRequesting={props.isRequestingForgotPassword}
          setFormFieldValue={props.setFormFieldValue}
          showAbout={() => props.toggleShowAboutPinnacleId(true)}
        />
      );
    case PINNACLE_ID_SCREENS.CONFIRM_FORGOT_PASSWORD:
      return (
        <ConfirmForgotPassword
          form={props.forms.confirmForgotPassword}
          codeDeliveredTo={props.forms.forgotPassword.codeDeliveredTo}
          isVerifying={props.isConfirmingForgotPassword}
          setFormFieldValue={props.setFormFieldValue}
          setVerificationFormDigit={(digit, keyPressed) =>
            props.setVerificationFormDigit(
              'confirmForgotPassword',
              digit,
              keyPressed
            )
          }
        />
      );
    default:
      return (
        <Login
          form={props.forms.login}
          login={props.login}
          isLoggingIn={props.isLoggingIn}
          setScreen={props.setScreen}
          setFormFieldValue={props.setFormFieldValue}
          showAbout={() => props.toggleShowAboutPinnacleId(true)}
        />
      );
  }
};
const mapDispatchToProps = dispatch => ({
  setFormFieldValue: (form, field, value) =>
    dispatch(setFormFieldValue(form, field, value)),
  toggleShowSelfEmployedModal: show =>
    dispatch(toggleShowSelfEmployedModal(show)),
  resend: registrationForm => dispatch(resend(registrationForm)),
  setVerificationFormDigit: (form, digit, value) =>
    dispatch(setVerificationFormDigit(form, digit, value))
});

export default connect(
  undefined,
  mapDispatchToProps
)(PinnacleID);
