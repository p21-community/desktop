/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';

const License = () => (
  <div className="content terms-text">
    <p>
      By exercising the Licensed Rights (defined below), You accept and agree to
      be bound by the terms and conditions of this Pinnacle 21 Open Source
      Software License ("License Agreement"). You are granted the Licensed
      Rights in consideration of Your acceptance of these terms and conditions,
      and the Licensor grants You such rights in consideration of benefits the
      Licensor receives from making the Licensed Material available under these
      terms and conditions. If You download a new version or a new copy of the
      Licensed Material, this License Agreement applies to Your use of all
      versions of the Licensed Material, including previously downloaded
      versions. If you do not accept this License Agreement, you must
      discontinue your use of all Licensed Material.
    </p>
    <h4>Section 1 – Definitions.</h4>
    <ol type="a">
      <li>
        <b>Adapted Material</b> means material subject to Copyright and Similar
        Rights that is derived from or based upon the Licensed Material and in
        which the Licensed Material is translated, altered, arranged,
        transformed, or otherwise modified in a manner requiring permission
        under the Copyright and Similar Rights held by the Licensor. In
        addition, and without limiting the foregoing, any software created by
        You or on Your behalf that integrates with the Licensed Material
        (including, without limitation, by any public or other interface,
        sharing of the same computer memory space, dynamically or statically
        linking, execution within the same classpath or java virtual machine or
        automated execution via a separate process) will be deemed to be Adapted
        Material.
      </li>
      <li>
        <b>Copyright and Similar Rights</b> means copyright and/or similar
        rights closely related to copyright including, without limitation,
        performance, broadcast, sound recording, and Sui Generis Database
        Rights, without regard to how the rights are labeled or categorized. For
        purposes of this License Agreement, the rights specified in Section
        2(b)(1)-(2) are not Copyright and Similar Rights.
      </li>
      <li>
        <b>Commercial Use</b> means charging a fee, royalty, or any other form
        of consideration for any products or services derived from or based upon
        the Licensed Material or that use, reference or integrate with the
        Licensed Material (including, without limitation, by any public or other
        interface, sharing of the same computer memory space, dynamically or
        statically linking, execution within the same classpath or java virtual
        machine or automated execution via a separate process).
      </li>
      <li>
        <b>Covered Materials</b> means Licensed Material or Adapted Material,
        combined or separately, and any part thereof.
      </li>
      <li>
        <b>Effective Technological Measures</b> means those measures that, in
        the absence of proper authority, may not be circumvented under laws
        fulfilling obligations under Article 11 of the WIPO Copyright Treaty
        adopted on December 20, 1996, and/or similar international agreements.
      </li>
      <li>
        <b>Licensed Material</b> means the Pinnacle 21 Validator, Define.xml
        Generator, Data Converter, and ClinicalTrials.gov Miner, Desktop
        Interface, and Command Line Interface, in object and
        source code form, and any related tools, files, documentations,
        validation rules and/or material (including, but not limited to,
        Licensor's customized version of the SDTM, SEND, ADaM, and Define.xml
        rules) and the Proprietary Materials, made available to You by Licensor.
      </li>
      <li>
        <b>Licensed Rights</b> means the rights granted to You subject to the
        terms and conditions of this License Agreement, which are limited to all
        Copyright and Similar Rights that apply to Your use of the Licensed
        Material, Adapted Material and Proprietary Material, and that the Licensor has authority to
        license.
      </li>
      <li>
        <b>Licensor</b> means Pinnacle 21 LLC.
      </li>
      <li>
        <b>Non-Commercial Use</b> means any personal use other than a Commercial
        Use.
      </li>
      <li>
        <b>Proprietary Material</b> means certain components in compiled or
        binary form, including installer and application bootstrap components,
        distributed with the Licensed Materials but made available solely as
        specified in this License Agreement.
      </li>
      <li>
        <b>Share</b> means to provide material to others by any means or process
        that requires permission under Copyright and Similar Rights or the
        Licensed Rights, such as reproduction, public display, public
        performance, distribution, dissemination, communication, or importation,
        and to make material available to the public including in ways that
        members of the public may access the material from a place and at a time
        individually chosen by them.
      </li>
      <li>
        <b>Sui Generis Database Rights</b> means rights other than copyright
        resulting from Directive 96/9/EC of the European Parliament and of the
        Council of 11 March 1996 on the legal protection of databases, as
        amended and/or succeeded, as well as other essentially equivalent rights
        anywhere in the world.
      </li>
      <li>
        <b>You</b> means the individual or entity exercising the Licensed Rights
        under this License Agreement. Your has a corresponding meaning.
      </li>
    </ol>
    <h4>Section 2 – Scope.</h4>
    <ol type="a">
      <li>
        <b>License grant.</b>
        <ol type="1">
          <li>
            Subject to the terms and conditions of this License Agreement
            (including Section 2(a)(2) below), the Licensor hereby grants You a
            worldwide, royalty-free, non-sublicensable, non-exclusive license to
            exercise the Licensed Rights in the Licensed Material, excluding the
            Proprietary Material (the license to which is governed by
            Section 2(a)(3) below), to:
            <ol type="A">
              <li>
                reproduce and use the Licensed Material, in whole or in part,
                solely for Your own Non-Commercial Use;
              </li>
              <li>
                use the Licensed Material solely in connection with consulting
                or other professional services provided by You to Your
                customers, provided that You never permit your customers to
                directly or indirectly use the Covered Materials as or in
                conjunction with any software or technology product or service
                (including, without limitation, any software-as-a-service or
                platform-as-a-service offering), and provided further that You
                never charge a fee to Your customers for the Licensed Material;
              </li>
              <li>
                create, reproduce, and use Adapted Material solely for Your own
                Non-Commercial Use; and
              </li>
              <li>
                share the Licensed Material, but not any Adapted Material.
              </li>
            </ol>
          </li>
          <li>
            <span style={{ textDecoration: 'underline' }}>
              Exceptions and Limitations
            </span>
            . As a condition of granting you the Licensed Rights, you shall not:
            <ol type="A">
              <li>
                sell, rent, loan license or sublicense the Covered Materials or
                otherwise market, share or use the Covered Materials for any
                Commercial Use;
              </li>
              <li>Share any Adapted Material;</li>
              <li>
                directly or indirectly market, Share, or offer to any other
                person the Covered Materials as or in conjunction with any
                software or technology product or service (including, without
                limitation, any software-as-a-service or platform-as-a-service
                offering);
              </li>
              <li>
                use or automate the use of the Covered Materials as part of or
                in conjunction with any software or technology product or
                service (including, without limitation, any
                software-as-a-service or platform-as-a-service offering);
              </li>
              <li>
                market, Share or offer the Covered Materials as or in
                conjunction with any time-sharing or service-bureau product or
                service, or otherwise use the Covered Materials to provide
                services to any third party other than as a component of study
                management services being provided to a third party client; and
              </li>
              <li>
                use, or assist a third party to use, the Covered Materials to
                circumvent or attempt to circumvent any terms of this License
                Agreement, such as by using the Covered Materials to develop
                methods of circumventing the terms of Sections  2C, 2D or 2E
                above by allowing or facilitating integration by a third party
                of the Covered Materials with a software-as-a-service or
                platform-as-a-service offering, or by requesting or providing
                instructions about how to integrate the Covered Materials with
                a software-as-a-service or platform-as-a-service offering in
                violation of Sections  2C, 2D or 2E above;
              </li>
              <li>
                block, disable or bypass the Proprietary Material or the
                operation of the Proprietary Material;
              </li>
              <li>
                remove or exclude the Proprietary Material from the
                Covered Material;
              </li>
              <li>
                copy, modify, distribute or use the Covered Material
                without the Proprietary Material;
              </li>
              <li>
                reverse engineer, decompile, disassemble or otherwise
                attempt to derive the source code, techniques, processes,
                algorithms, know-how or other information from the
                Proprietary Material.
              </li>
            </ol>
          </li>
          <li>
            Subject to the terms and conditions of this License Agreement (including
            Section 2(a)(2) above), the Licensor hereby grants You a limited,
            worldwide, royalty-free, non-sublicensable, non-exclusive license
            in the Proprietary Material to install, copy and use the
            Proprietary Material solely in conjunction with and as part of
            the Covered Material.  Additionally, You agree that the Licensor may
            use any information collected through the Proprietary Material for
            its own business purposes without restriction and without compensation to you.
          </li>
          <li>
            <span style={{ textDecoration: 'underline' }}>Attribution</span>. If
            you make any reproduction or copy of the Covered Materials or Share
            the Licensed Material, You must include the same and maintain the
            prominence of the copyright notices, references to this License
            Agreement, and identification and branding of the Licensor as
            contained in the original copy of the Licensed Material.
          </li>
          <li>
            <span style={{ textDecoration: 'underline' }}>Term</span>. The term
            of this License Agreement is specified in Section 5(a).
          </li>
          <li>
            <span style={{ textDecoration: 'underline' }}>
              Media and formats; technical modifications allowed
            </span>
            . The Licensor authorizes You to exercise the Licensed Rights in all
            media and formats whether now known or hereafter created, and to
            make technical modifications necessary to do so. For purposes of
            this License Agreement, simply making modifications authorized by
            this Section 2(a)(6) never produces Adapted Material.
          </li>
          <li>
            <span style={{ textDecoration: 'underline' }}>No endorsement</span>.
            Nothing in this License Agreement constitutes or may be construed as
            permission to assert or imply that You are, or that Your use of the
            Licensed Material is, connected with, or sponsored, endorsed, or
            granted official status by, the Licensor or others.
          </li>
          <li>
            <span style={{ textDecoration: 'underline' }}>Commercial Use</span>.
            The Licensed Rights are limited to Your Non-Commercial Use of the
            Covered Materials. You may only use the Covered Materials for
            Commercial Use if that use is pursuant to a separately negotiated
            commercial license agreement entered into between You and the
            Licensor.
          </li>
          <li>
            <span style={{ textDecoration: 'underline' }}>
              Licensor reserves all rights not expressly granted in this License
              Agreement
            </span>
            .
          </li>
        </ol>
      </li>
      <li>
        <b>Other rights.</b>
        <ol type="1">
          <li>
            Moral rights, such as the right of integrity, are not licensed under
            this License Agreement, nor are publicity, privacy, and/or other
            similar personality rights; however, to the extent possible, the
            Licensor waives and/or agrees not to assert any such rights held by
            the Licensor to the limited extent necessary to allow You to
            exercise the Licensed Rights in accordance with this License
            Agreement, but not otherwise.
          </li>
          <li>
            Patent and trademark rights are not licensed under this License
            Agreement.
          </li>
          <li>
            Subject to your compliance with this License Agreement, to the
            extent possible, the Licensor waives any right to collect royalties
            from You for the exercise of the Licensed Rights, whether directly
            or through a collecting society under any voluntary or waivable
            statutory or compulsory licensing scheme. In all other cases,
            subject to your compliance with this License Agreement, the Licensor
            expressly reserves any right to collect such royalties.
          </li>
        </ol>
      </li>
      <li>
        <b>Privacy</b>
        <ol type="1">
          <li>
            You agree to the terms of the Pinnacle 21 SIP Privacy Statement,
            as may be updated from time to time, currently available at&nbsp;
            <a
                href="https://www.pinnacle21.com/sip-privacy"
            >
              https://www.pinnacle21.com/sip-privacy
            </a>
            .
          </li>
        </ol>
      </li>
    </ol>
    <h4>Section 3 – Sui Generis Database Rights.</h4>
    <p>
      Where the Licensed Rights include Sui Generis Database Rights that apply
      to Your use of the Licensed Material:
    </p>
    <ol type="a">
      <li>
        for the avoidance of doubt, Section 2(a)(1) grants You the right to
        extract, reuse, and reproduce all or a substantial portion of the
        contents of the database, provided Your extraction, reuse, and
        reproduction is solely for Your own Non-Commercial Use, not for any
        purposes prohibited by Section 2(a)(2) and otherwise in compliance with
        this License Agreement; and
      </li>
      <li>
        if You include all or a substantial portion of the database contents in
        a database in which You have Sui Generis Database Rights, then the
        database in which You have Sui Generis Database Rights (but not its
        individual contents) is Adapted Material.
      </li>
    </ol>
    <p>
      For the avoidance of doubt, this Section 3 supplements and does not
      replace Your obligations under this License Agreement where the Licensed
      Rights include other Copyright and Similar Rights.
    </p>{' '}
    <h4>Section 4 – Disclaimer of Warranties and Limitation of Liability.</h4>
    <ol type="a">
      <b>
        <li>
          Unless otherwise separately undertaken by the Licensor in writing, to
          the extent possible, the Licensor offers the Licensed Material as-is
          and as-available, and makes no representations or warranties of any
          kind concerning the Licensed Material, whether express, implied,
          statutory, or other. This includes, without limitation, warranties of
          title, merchantability, fitness for a particular purpose,
          non-infringement, absence of latent or other defects, accuracy, or the
          presence or absence of errors, whether or not known or discoverable.
          The Licensor does not guarantee any results with respect to use of
          the Licensed Materials. Where disclaimers of warranties are not
          allowed in full or in part, this disclaimer may not apply to You.
        </li>
      </b>{' '}
      <b>
        <li>
          To the extent possible, in no event will the Licensor be liable to You
          on any legal theory (including, without limitation, negligence) or
          otherwise for any direct, special, indirect, incidental,
          consequential, punitive, exemplary, or other losses, costs, expenses,
          or damages arising out of this License Agreement or use of the
          Licensed Material, even if the Licensor has been advised of the
          possibility of such losses, costs, expenses, or damages. For the
          avoidance of doubt, the Licensor will have no liability to You or
          to any third party with respect to any data validation or data
          submission, including, without limitation, for delays in obtaining
          acceptance of submitted data. Where a limitation of liability is
          not allowed in full or in part, this limitation may not apply to You.
        </li>
      </b>
      <li>
        The disclaimer of warranties and limitation of liability provided above
        shall be interpreted in a manner that, to the extent possible, most
        closely approximates an absolute disclaimer and waiver of all liability.
      </li>
    </ol>
    <h4>Section 5 – Term and Termination.</h4>
    <ol type="a">
      <li>
        This License Agreement applies for the term of the Copyright and Similar
        Rights licensed here. However, if You fail to comply with this License
        Agreement, then Your rights under this License Agreement terminate
        automatically.
      </li>
      <li>
        Where Your right to use the Licensed Material has terminated under
        Section 5(a), it reinstates:
        <ol type="1">
          <li>
            automatically as of the date the violation is cured, provided it is
            cured within 30 days of Your discovery of the violation; or
          </li>
          <li>upon express reinstatement by the Licensor.</li>
        </ol>
        <p>
          For the avoidance of doubt, this Section 5(b) does not affect any
          right the Licensor may have to seek remedies for Your violations of
          this License Agreement.
        </p>
      </li>
      <li>
        For the avoidance of doubt, the Licensor may also offer the Licensed
        Material under separate terms or conditions or stop distributing the
        Licensed Material at any time; however, doing so will not terminate this
        License Agreement.
      </li>
      <li>
        Sections 1, 4, 5, 6, and 7 survive termination of this License
        Agreement.
      </li>
    </ol>
    <h4>Section 6 – Other Terms and Conditions.</h4>
    <ol type="a">
      <li>
        The Licensor shall not be bound by any additional or different terms or
        conditions communicated by You unless expressly agreed in writing.
      </li>
      <li>
        Any arrangements, understandings, or agreements regarding the Licensed
        Material not stated herein are separate from and independent of the
        terms and conditions of this License Agreement.
      </li>
      <li>
        The laws of the Commonwealth of Pennsylvania shall govern the validity,
        construction, and performance of this Agreement, without giving effect
        to the conflict of laws rules thereof to the extent that the application
        of the laws of another jurisdiction would be required thereby. You
        consent to the sole and exclusive jurisdiction of the federal and state
        courts located in or embracing Montgomery County, PA in all actions
        arising under this License Agreement and hereby irrevocably waive any
        claim that any such court lacks jurisdiction or that such venue or forum
        is inconvenient. In the event of an action or proceeding by Licensor to
        enforce or exercise its rights under this License Agreement, the
        Licensor shall be entitled to be reimbursed for its reasonable
        attorneys', other professionals', and experts' fees and out-of-pocket
        legal costs.
      </li>
      <li>
        You agree that any breach of your obligations or representations under
        this License Agreement would result in irreparable injury to the
        Licensor, and that in the event of any breach or threatened breach
        hereof, the Licensor will be entitled to seek injunctive relief in
        addition to any other remedies to which it may be entitled, without the
        necessity of posting bond.
      </li>
    </ol>
    <h4>Section 7 – Interpretation.</h4>
    <ol type="a">
      <li>
        For the avoidance of doubt, this License Agreement does not, and shall
        not be interpreted to, reduce, limit, restrict, or impose conditions on
        any use of the Licensed Material that could lawfully be made without
        permission under this License Agreement.
      </li>
      <li>
        To the extent possible, if any provision of this License Agreement is
        deemed unenforceable, it shall be automatically reformed to the minimum
        extent necessary to make it enforceable. If the provision cannot be
        reformed, it shall be severed from this License Agreement without
        affecting the enforceability of the remaining terms and conditions.
      </li>
      <li>
        No term or condition of this License Agreement will be waived and no
        failure to comply consented to unless expressly agreed to by the
        Licensor.
      </li>
      <li>
        Nothing in this License Agreement constitutes or may be interpreted as a
        limitation upon, or waiver of, any privileges and immunities that apply
        to the Licensor or You, including from the legal processes of any
        jurisdiction or authority.
      </li>
    </ol>
    <p>The following is not part of the License Agreement:</p>
    <p>
      Pinnacle 21 Open Source Software License Copyright Pinnacle 21 LLC. The
      Pinnacle 21 Open Source Software License was adapted from the Creative
      Commons Attribution-NoDerivatives 4.0 International License, which was
      obtained from the Creative Commons Corporation. The original, unmodified
      version the license agreement can be found&nbsp;
      <a
        rel="noopener noreferrer"
        target="_blank"
        href="http://creativecommons.org/licenses/by-nd/4.0/"
      >
        here
      </a>
      .
    </p>
  </div>
);

export default License;
