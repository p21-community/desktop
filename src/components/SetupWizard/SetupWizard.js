/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { connect } from 'react-redux';

import PinnacleID from './components/PinnacleID/PinnacleID';
import Terms from './components/Terms';
import Complete from './components/Complete';
import AboutPinnacleId from './components/PinnacleID/components/AboutPinnacleId';
import { loadKey, savePreferences } from '../../utils/localStorage';
import {
  PINNACLE_ID_SCREENS,
  completeSetup,
  setScreen,
  registerNext,
  toggleShowAboutPinnacleId,
  login,
  register,
  verify,
  generateAppId,
  forgotPasswordNext,
  confirmForgotPasswordNext,
  resetSetupWizardReducer,
  setIsVerifyingBecauseOfRegistration
} from './setupWizardRedux';

class SetupWizard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentStep: 0
    };

    this.defaultSuccessButton = (action = this.incrementStep) => (
      <Button bsStyle="primary" onClick={action}>
        Next <i className="fa fa-arrow-right" />
      </Button>
    );

    this.steps = [
      {
        name: 'Terms',
        successButton: () => (
          <Button bsStyle="primary" onClick={this.incrementStep}>
            I Accept
          </Button>
        )
      },
      {
        name: 'Pinnacle ID',
        successButton: pinnacleIdScreen => {
          switch (pinnacleIdScreen) {
            case PINNACLE_ID_SCREENS.LOGIN: {
              return (
                <Button
                  bsStyle="primary"
                  onClick={this.handleLogin}
                  disabled={!!this.props.isLoggingIn}
                >
                  {this.props.isLoggingIn ? (
                    <span>
                      Logging in <i className="fa fa-spinner fa-pulse fa-fw" />
                    </span>
                  ) : (
                    <span>
                      Login <i className="fa fa-arrow-right" />
                    </span>
                  )}
                </Button>
              );
            }
            case PINNACLE_ID_SCREENS.REGISTER: {
              return (
                <div className="next-button-container">
                  <Button
                    bsStyle="primary"
                    onClick={() =>
                      this.props.registerNext(
                        this.props.forms.register,
                        this.props.nonWorkEmail
                      )
                    }
                  >
                    Next <i className="fa fa-arrow-right" />
                  </Button>
                </div>
              );
            }
            case PINNACLE_ID_SCREENS.FORGOT_PASSWORD: {
              return (
                <Button
                  bsStyle="primary"
                  onClick={() => {
                    this.props.forgotPasswordNext(
                      this.props.forms.forgotPassword
                    );
                  }}
                  disabled={!!this.props.isRequestingForgotPassword}
                >
                  {this.props.isRequestingForgotPassword ? (
                    <span>
                      Requesting Reset Code{' '}
                      <i className="fa fa-spinner fa-pulse fa-fw" />
                    </span>
                  ) : (
                    <span>
                      Next <i className="fa fa-arrow-right" />
                    </span>
                  )}
                </Button>
              );
            }
            case PINNACLE_ID_SCREENS.CONFIRM_FORGOT_PASSWORD: {
              return (
                <Button
                  bsStyle="primary"
                  onClick={() => {
                    this.props.confirmForgotPasswordNext(
                      this.props.forms.confirmForgotPassword
                    );
                  }}
                  disabled={!!this.props.isConfirmingForgotPassword}
                >
                  {this.props.isConfirmingForgotPassword ? (
                    <span>
                      Verifying <i className="fa fa-spinner fa-pulse fa-fw" />
                    </span>
                  ) : (
                    <span>
                      Next <i className="fa fa-arrow-right" />
                    </span>
                  )}
                </Button>
              );
            }
            case PINNACLE_ID_SCREENS.COMMUNICATION_PREFS: {
              return (
                <Button
                  bsStyle="primary"
                  onClick={() => {
                    this.props.register(
                      this.props.forms.register,
                      this.props.nonWorkEmail
                    );
                  }}
                  disabled={!!this.props.isRegistering}
                >
                  {this.props.isRegistering ? (
                    <span>
                      Registering <i className="fa fa-spinner fa-pulse fa-fw" />
                    </span>
                  ) : (
                    <span>
                      Next <i className="fa fa-arrow-right" />
                    </span>
                  )}
                </Button>
              );
            }
            case PINNACLE_ID_SCREENS.ACCOUNT_VERIFICATION: {
              return (
                <Button
                  bsStyle="primary"
                  onClick={this.handleVerification}
                  disabled={
                    !!this.props.isVerifying || !!this.props.isResending
                  }
                >
                  {this.props.isVerifying ? (
                    <span>
                      Verifying <i className="fa fa-spinner fa-pulse fa-fw" />
                    </span>
                  ) : (
                    <span>
                      Continue <i className="fa fa-arrow-right" />
                    </span>
                  )}
                </Button>
              );
            }
            default:
              return this.defaultSuccessButton();
          }
        }
      },
      {
        name: 'Complete',
        successButton: () => (
          <Button className="finish" bsStyle="success" onClick={this.onFinish}>
            Finish
          </Button>
        )
      }
    ];
  }

  componentDidMount() {
    if (this.props.cameFromPreferences) {
      this.setState({ currentStep: 2 });
    }
  }

  handleLogin = async () => {
    const loginResult = await this.props.login(this.props.forms.login);
    if (loginResult.success) {
      this.incrementStep();
    }
  };

  handleVerification = async () => {
    let email;
    let password;
    if (this.props.isVerifyingBecauseOfRegistration) {
      email = this.props.forms.register.email;
      password = this.props.forms.register.password;
    } else {
      // We're here because they tried logging in with
      // an unconfirmed account
      email = this.props.forms.login.email;
      password = this.props.forms.login.password;
    }
    const verificationResult = await this.props.verify(
      email,
      password,
      this.props.forms.verification.verificationDigits
    );
    if (verificationResult.success) {
      this.incrementStep();
    }
  };

  hijackDecrementStepForPinnacleId = () => {
    switch (this.props.pinnacleIdScreen) {
      case PINNACLE_ID_SCREENS.LOGIN: {
        this.setState({ currentStep: this.state.currentStep - 1 });
        break;
      }
      case PINNACLE_ID_SCREENS.COMMUNICATION_PREFS: {
        this.props.setScreen(PINNACLE_ID_SCREENS.REGISTER);
        break;
      }
      case PINNACLE_ID_SCREENS.ACCOUNT_VERIFICATION: {
        this.props.setScreen(PINNACLE_ID_SCREENS.LOGIN);
        break;
      }
      default: {
        this.props.setScreen(PINNACLE_ID_SCREENS.LOGIN);
        break;
      }
    }
  };

  decrementStep = () => {
    if (this.state.currentStep === 2) {
      this.hijackDecrementStepForPinnacleId();
    } else {
      this.setState({ currentStep: this.state.currentStep - 1 });
    }
  };

  incrementStep = () => {
    const { currentStep } = this.state;
    this.setState({ currentStep: currentStep + 1 });
  };

  onFinish = () => {
    if (!loadKey('app-id')) {
      generateAppId().then(() => null);
    }
    savePreferences();
    this.props.completeSetup();
    this.props.onFinish();
  };

  renderCurrentStep = props => {
    switch (this.state.currentStep) {
      case 1:
        return <Terms />;
      case 2:
        return <PinnacleID {...props} login={this.handleLogin} />;
      case 3:
        return <Complete />;
      default:
        return this.initialPage();
    }
  };

  requestIsBeingMade = () => {
    return (
      !!this.props.isLoggingIn ||
      !!this.props.isRegistering ||
      !!this.props.isResending ||
      !!this.props.isVerifying ||
      !!this.props.isRequestingForgotPassword ||
      !!this.props.isConfirmingForgotPassword
    );
  };

  renderFooterButtons = props => {
    const { currentStep } = this.state;
    const typicalBackButton = (
      <Button
        bsStyle="default"
        onClick={this.decrementStep}
        disabled={this.requestIsBeingMade()}
      >
        <i className="fa fa-arrow-left" />
        Back
      </Button>
    );
    let backButton;
    if (currentStep === 2) {
      switch (props.pinnacleIdScreen) {
        case PINNACLE_ID_SCREENS.REGISTER: {
          backButton = (
            <span>
              <a
                className="skip-for-now"
                onClick={() => props.toggleShowAboutPinnacleId(true, true)}
              >
                Skip for Now
              </a>
              {typicalBackButton}
            </span>
          );
          break;
        }
        case PINNACLE_ID_SCREENS.ACCOUNT_VERIFICATION: {
          backButton = (
            <Button
              bsStyle="default"
              onClick={this.decrementStep}
              disabled={this.requestIsBeingMade()}
            >
              Cancel
            </Button>
          );
          break;
        }
        default: {
          backButton = typicalBackButton;
          break;
        }
      }
    } else if (currentStep > 0) {
      backButton = typicalBackButton;
    } else {
      return this.defaultSuccessButton();
    }

    return (
      <div>
        {backButton}
        {this.steps[currentStep - 1].successButton(props.pinnacleIdScreen)}
      </div>
    );
  };

  initialPage = () => (
    <div>
      <h3>Pinnacle 21 Community Setup</h3>
      <p>
        This wizard will guide you through the setup process of Pinnacle 21
        Community.
      </p>

      <p>
        To continue, click <b>Next</b>.
      </p>
    </div>
  );

  renderSidebar = () => (
    <ul>
      <li>
        <span className="fa-stack fa-4x validator">
          <i className="fa fa-circle fa-stack-2x" />
          <i className="fa fa-check fa-stack-1x fa-inverse" />
        </span>
      </li>
      <li>
        <span className="fa-stack fa-4x define">
          <i className="fa fa-circle fa-stack-2x" />
          <i className="fa fa-pencil fa-stack-1x fa-inverse" />
        </span>
      </li>
      <li>
        <span className="fa-stack fa-4x converter">
          <i className="fa fa-circle fa-stack-2x" />
          <i className="fa fa-cogs fa-stack-1x fa-inverse" />
        </span>
      </li>
      <li>
        <span className="fa-stack fa-4x clinical-trials">
          <i className="fa fa-circle fa-stack-2x" />
          <i className="fa fa-flask fa-stack-1x fa-inverse" />
        </span>
      </li>
    </ul>
  );

  setStep = stepNumber => {
    if (stepNumber < this.state.currentStep) {
      if (stepNumber === 2) {
        this.props.resetSetupWizardReducer();
      }
      this.setState({ currentStep: stepNumber });
    }
  };

  renderStepProgress = () => {
    const { currentStep } = this.state;

    const renderSteps = () => {
      return this.steps.map(({ name }, i, arr) => {
        let className;
        let badge = 'badge';

        if (i + 1 === this.state.currentStep) {
          className = 'active';
          badge = 'badge badge-info';
        } else if (i + 1 < this.state.currentStep) {
          className = 'complete';
          badge = 'badge badge-success';
        }

        return (
          <li
            key={name}
            data-target="target"
            className={className}
            onClick={() => this.setStep(i + 1)}
          >
            <span className={badge}>{i + 1}</span>
            {name}
            <span className="chevron" />
          </li>
        );
      });
    };

    if (currentStep > 0) {
      return (
        <div className="wizard">
          <ul className="steps">{renderSteps()}</ul>
        </div>
      );
    }
  };

  skipPinnacleId = () => {
    this.props.toggleShowAboutPinnacleId(false);
    this.incrementStep();
    this.props.setScreen(PINNACLE_ID_SCREENS.LOGIN);
  };

  render() {
    const {
      showAboutPinnacleId,
      skipPinnacleId,
      toggleShowAboutPinnacleId
    } = this.props;

    return (
      <Modal className="setup-wizard" show={true}>
        <Modal.Header>
          <Modal.Title>Pinnacle 21 Community Setup</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          {showAboutPinnacleId ? (
            <AboutPinnacleId
              skipping={skipPinnacleId}
              cancel={() => toggleShowAboutPinnacleId(false)}
              skip={this.skipPinnacleId}
            />
          ) : (
            <Row className="row-eq-height">
              <Col className="wizard-sidebar" sm={3}>
                {this.renderSidebar()}
              </Col>
              <Col sm={9}>
                {this.renderStepProgress()}
                {this.renderCurrentStep(this.props)}
              </Col>
            </Row>
          )}
        </Modal.Body>
        {!showAboutPinnacleId && (
          <Modal.Footer>{this.renderFooterButtons(this.props)}</Modal.Footer>
        )}
      </Modal>
    );
  }
}

const mapStateToProps = ({ setup }) => ({ ...setup });
const mapDispatchToProps = dispatch => ({
  completeSetup: () => dispatch(completeSetup()),
  setScreen: screen => dispatch(setScreen(screen)),
  resetSetupWizardReducer: () => dispatch(resetSetupWizardReducer()),
  registerNext: (form, nonWorkEmail) =>
    dispatch(registerNext(form, nonWorkEmail)),
  forgotPasswordNext: form => dispatch(forgotPasswordNext(form)),
  confirmForgotPasswordNext: form => dispatch(confirmForgotPasswordNext(form)),
  toggleShowAboutPinnacleId: (show, skip) =>
    dispatch(toggleShowAboutPinnacleId(show, skip)),
  login: form => dispatch(login(form)),
  register: (form, nonWorkEmail) => dispatch(register(form, nonWorkEmail)),
  verify: (email, password, verificationDigits) =>
    dispatch(verify(email, password, verificationDigits)),
  setIsVerifyingBecauseOfRegistration: isVerifyingBecauseOfRegistration =>
    dispatch(
      setIsVerifyingBecauseOfRegistration(isVerifyingBecauseOfRegistration)
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SetupWizard);
