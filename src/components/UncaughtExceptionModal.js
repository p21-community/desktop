/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { Button, Modal, FormGroup, FormControl } from 'react-bootstrap';

export default class UncaughtExceptionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      message: ''
    };
  }

  restartApp = () => {
    this.props.ipcRenderer.send('restart-app');
  };

  onMessage = e => {
    this.setState({ message: e.target.value });
  };

  sendBugReport = () => {
    const { error, meta } = this.props;
    const message = this.state.message;
    this.props.ipcRenderer.send(
      'bug-report',
      error.stack
        ? {
            message: error.message,
            stack: error.stack
          }
        : error,
      message,
      meta
    );
  };

  render() {
    return (
      <Modal show={this.props.show}>
        <Modal.Header>
          <Modal.Title>Uh oh, a serious error has occurred...</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <p>
            To ensure proper functionality, the application must be restarted.
          </p>
          <p>
            We apologize for the inconvenience, and would like to prevent this
            from happening in the future.
          </p>
          <p>
            To help us with diagnosing what went wrong, please send us the error
            by clicking <b>Send Bug Report</b>
          </p>
          <p>
            Any additional information you could share with us would be great!
            <br />
            Just enter it here:
          </p>
          <FormGroup>
            <FormControl
              componentClass="textarea"
              rows="8"
              cols="50"
              placeholder="What were you trying to do that caused this error to occur?"
              onChange={this.onMessage}
              value={this.state.message}
            />
          </FormGroup>
        </Modal.Body>

        <Modal.Footer>
          <Button onClick={this.restartApp}>Restart Application</Button>
          <Button bsStyle="primary" onClick={this.sendBugReport}>
            Send Bug Report
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
