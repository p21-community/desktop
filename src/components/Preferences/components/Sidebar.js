/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

const sidebarPropTypes = {
  activePreference: PropTypes.string.isRequired,
  onOptionClick: PropTypes.func.isRequired
};

export const Sidebar = ({ activePreference, children, onOptionClick }) => {
  return (
    <div className="col-md-3 preferences-sidebar">
      <ul className="main-menu">
        {React.Children.map(children, child => {
          if (child) {
            const isActive = activePreference === child.props.name;
            return React.cloneElement(child, {
              onClick: onOptionClick,
              isActive
            });
          } else {
            return '';
          }
        })}
      </ul>
    </div>
  );
};

Sidebar.propTypes = sidebarPropTypes;

const sidebarOptionPropTypes = {
  name: PropTypes.string.isRequired
};

export const SidebarOption = ({ isActive, name, onClick, disabled }) => (
  <li>
    <div
      className={
        isActive ? 'active' : disabled ? 'disabled-sidebar-option' : ''
      }
      onClick={onClick.bind(null, name)}
    >
      <span className="text">{name}</span>
    </div>
  </li>
);

SidebarOption.propTypes = sidebarOptionPropTypes;
