/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';

const propTypes = {
  onClick: PropTypes.func.isRequired
};

const ResetSettings = props => (
  <div>
    <h2 className="preference-title">Reset to Default Settings</h2>
    <div className="preference-option-item">
      <Button className="btn btn-primary" onClick={props.onClick}>
        Reset to Default Settings
      </Button>
    </div>
  </div>
);

ResetSettings.propTypes = propTypes;

export default ResetSettings;
