/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { VerificationForm } from '../../SetupWizard/components/PinnacleID/components/VerifyAccount';

const PinnacleIdVerification = props => {
  return (
    <div className="pinnacle-id-settings">
      <h2 className="preference-title">Pinnacle ID Verification</h2>
      <div className="pinnacle-id-confirmation">
        <div className="password-recovery verify-account">
          <div className="main-content">
            <VerificationForm {...props} autoFocus />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PinnacleIdVerification;
