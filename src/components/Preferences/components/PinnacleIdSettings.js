/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import {
  Checkbox,
  Col,
  FormControl,
  FormGroup,
  HelpBlock,
  Modal,
  Row
} from 'react-bootstrap';
import Select from 'react-select';
import { JOB_OPTIONS } from '../../SetupWizard/setupWizardRedux';
import { COUNTRIES } from '../../../utils/formValidationConstants';
import PasswordInput from '../../SetupWizard/components/PinnacleID/components/PasswordInput';

export const NON_WORK_EMAIL_ERROR =
  'Non-work email detected. To help prevent fake accounts, we kindly ask that you provide your work email.';

const SelfEmployedModal = props => (
  <Modal
    show={props.show}
    container={props.container}
    backdrop={false}
    keyboard
    autoFocus
    restoreFocus
    enforceFocus={false}
    bsSize="small"
    dialogClassName="self-employed-modal"
    onHide={props.close}
  >
    <Modal.Header closeButton>
      <h4>Self Employed?</h4>
    </Modal.Header>
    <Modal.Body>
      <p>
        Please email{' '}
        <a href="mailto:support@pinnacle21.com">support@pinnacle21.com</a> for
        assistance with changing your email address.
      </p>
    </Modal.Body>
  </Modal>
);

const PinnacleIdInput = props => (
  <Row className="pinnacle-id-input">
    <Col md={2}>{props.label && <label>{props.label}</label>}</Col>
    <Col md={6}>
      <FormControl
        type={props.type}
        placeholder={props.placeholder}
        value={props.state[props.property]}
        onChange={props.onChange(props.property)}
      />
      {props.state.errors[props.property] && (
        <HelpBlock>{props.state.errors[props.property]}</HelpBlock>
      )}
    </Col>
    <Col md={4}>{props.thirdColumn}</Col>
  </Row>
);

const PinnacleIdSelect = props => (
  <Row className="pinnacle-id-input">
    <Col md={2}>{props.label && <label>{props.label}</label>}</Col>
    <Col md={6}>
      <Select
        options={props.options}
        value={props.state[props.property]}
        onChange={props.onChange(props.property)}
        clearable={false}
        backspaceRemoves={false}
      />
      {props.state.errors[props.property] && (
        <HelpBlock>{props.state.errors[props.property]}</HelpBlock>
      )}
    </Col>
    <Col md={4} />
  </Row>
);

export default class PinnacleIdSettings extends Component {
  renderThirdColumnOfEmailField = () => {
    if (
      !this.props.pinnacleIdSettings.emailVerified &&
      this.props.pinnacleIdSettings.email ===
        this.props.ogPinnacleIdSettings.email
    ) {
      if (this.props.sendingEmailVerificationCode) {
        return (
          <div className="one-line">
            <i className="fa fa-spinner fa-pulse fa-fw" />
          </div>
        );
      } else {
        return (
          <div className="two-line">
            <span className="error">Unverified</span>
            <br />
            <a onClick={e => this.props.requestVerificationCodeForEmail()}>
              Click to verify
            </a>
          </div>
        );
      }
    } else if (
      this.props.pinnacleIdSettings.errors.email === NON_WORK_EMAIL_ERROR
    ) {
      return (
        <div className="one-line">
          <a onClick={() => this.props.toggleShowSelfEmployedModal(true)}>
            Self Employed?
          </a>
        </div>
      );
    }
  };

  renderChangePassword = props => {
    return (
      <div className="prefs-password-fields">
        <p>Please set your password.</p>
        <FormGroup
          controlId="formHorizontalPassword"
          className="current-password"
          validationState={props.form.errors.currentPassword && 'error'}
        >
          <FormControl
            autoFocus
            value={props.form.currentPassword}
            onChange={e =>
              props.setChangePasswordField('currentPassword', e.target.value)
            }
            type="password"
            disabled={props.disabled}
            placeholder="Current Password"
          />
          {props.form.errors.currentPassword && (
            <HelpBlock>{props.form.errors.currentPassword}</HelpBlock>
          )}
        </FormGroup>
        <PasswordInput
          disabled={props.disabled}
          password={props.form.newPassword}
          placeholder="New Password"
          error={props.form.errors.newPassword}
          onChange={e =>
            props.setChangePasswordField('newPassword', e.target.value)
          }
        />
        <FormGroup
          controlId="formHorizontalPassword"
          className="verify-password"
          validationState={props.form.errors.verifyNewPassword && 'error'}
        >
          <FormControl
            value={props.form.verifyNewPassword}
            onChange={e =>
              props.setChangePasswordField('verifyNewPassword', e.target.value)
            }
            type="password"
            disabled={props.disabled}
            placeholder="Verify New Password"
          />
          {props.form.errors.verifyNewPassword && (
            <HelpBlock>{props.form.errors.verifyNewPassword}</HelpBlock>
          )}
        </FormGroup>
        {props.form.errors.form &&
          props.form.errors.form !== 'PASSWORD_COMPLEXITY' && (
            <div className="error">{props.form.errors.form}</div>
          )}
      </div>
    );
  };

  renderSettings = showChangePassword => {
    if (showChangePassword) {
      const form = this.props.changePasswordForm;
      const disabled = this.props.requestBeingMade;
      return this.renderChangePassword({
        disabled,
        form,
        setChangePasswordField: this.props.setChangePasswordField
      });
    } else {
      return (
        <div>
          <Row className="pinnacle-id-input pinnacle-id-name-input">
            <Col md={2}>
              <label>Name</label>
            </Col>
            <Col md={6}>
              <FormControl
                placeholder="First"
                value={this.props.pinnacleIdSettings.firstName}
                onChange={this.props.handleInputChange('firstName')}
              />
              <FormControl
                placeholder="Last"
                value={this.props.pinnacleIdSettings.lastName}
                onChange={this.props.handleInputChange('lastName')}
              />
              {this.props.pinnacleIdSettings.errors['name'] && (
                <HelpBlock>
                  {this.props.pinnacleIdSettings.errors['name']}
                </HelpBlock>
              )}
            </Col>
            <Col md={4} className="change-my-password">
              <div className="one-line">
                <a onClick={() => this.props.toggleChangePassword(true)}>
                  Change my password
                </a>
              </div>
            </Col>
          </Row>
          <PinnacleIdInput
            state={this.props.pinnacleIdSettings}
            onChange={this.props.handleInputChange}
            label="Email"
            property="email"
            placeholder="Email"
            type="email"
            thirdColumn={this.renderThirdColumnOfEmailField()}
          />
          <PinnacleIdSelect
            state={this.props.pinnacleIdSettings}
            onChange={this.props.handleSelectChange}
            label="Job"
            property="job"
            options={JOB_OPTIONS}
          />
          <PinnacleIdSelect
            state={this.props.pinnacleIdSettings}
            onChange={this.props.handleSelectChange}
            label="Country"
            property="country"
            options={COUNTRIES}
          />
          <Checkbox
            checked={this.props.pinnacleIdSettings.newsletter}
            onChange={this.props.toggleNewsletter}
          >
            Receive Email Communications
          </Checkbox>
          {this.props.pinnacleIdSettings.errors.form && (
            <div className="error">
              {this.props.pinnacleIdSettings.errors.form}
            </div>
          )}
          <SelfEmployedModal
            show={this.props.pinnacleIdSettings.showSelfEmployedModal}
            container={this}
            close={() => this.props.toggleShowSelfEmployedModal(false)}
          />
        </div>
      );
    }
  };
  render() {
    const { showChangePassword } = this.props;
    return (
      <div className="pinnacle-id-settings">
        <h2 className="preference-title">Pinnacle ID Settings</h2>
        {this.renderSettings(showChangePassword)}
      </div>
    );
  }
}
