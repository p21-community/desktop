/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';
import JWTDecode from 'jwt-decode';
import { isEmpty, omit, keys, tail, trim } from 'lodash';

import PinnacleIdSettings, {
  NON_WORK_EMAIL_ERROR
} from './components/PinnacleIdSettings';
import { Sidebar, SidebarOption } from './components/Sidebar';
import ValidatorSettings from './components/ValidatorSettings';
import { loadPreferences, savePreferences } from '../../utils/localStorage';
import { Benefits } from '../SetupWizard/components/PinnacleID/components/AboutPinnacleId';
import {
  hasNumber,
  isEmptyOrHasNullValues,
  isBlank,
  validateEmail,
  validatePassword
} from '../../utils/utils';
import { DISALLOWED_EMAILS } from '../../utils/formValidationConstants';
import PinnacleIdVerification from './components/PinnacleIdVerification';

const PREFERENCES = {
  VALIDATOR: 'Validator',
  PINNACLE_ID: 'Pinnacle ID'
};

export default class Preferences extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultState(loadPreferences());
  }

  getDefaultState = preferences => {
    const stateObj = {
      preferences,
      requestBeingMade: false,
      isResending: false,
      sendingEmailVerificationCode: false,
      activePreference: PREFERENCES.VALIDATOR,
      goingToWizard: false,
      verifications: [],
      showChangePassword: false,
      changePasswordForm: {
        currentPassword: '',
        newPassword: '',
        verifyNewPassword: '',
        errors: {}
      }
    };
    if (preferences && preferences.identity) {
      try {
        const decodedJwt = JWTDecode(preferences.identity.idToken);
        stateObj.pinnacleIdSettings = {
          email: decodedJwt['email'],
          emailVerified: decodedJwt['email_verified'],
          firstName: decodedJwt['given_name'],
          lastName: decodedJwt['family_name'],
          job: decodedJwt['custom:job'],
          country: decodedJwt['custom:iso_country'],
          newsletter: decodedJwt['custom:newsletter'] === '1',
          errors: {},
          showSelfEmployedModal: false
        };
        stateObj.ogPinnacleIdSettings = stateObj.pinnacleIdSettings;
      } catch (err) {
        // InvalidTokenError
        this.props.logger.error(err.toString());
      }
    }
    return stateObj;
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.show && !this.props.show) {
      // Allow modal to fade out before resetting activePreference
      setTimeout(() => {
        this.setState({ activePreference: PREFERENCES.VALIDATOR });
      }, 150);
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (!nextProps.show) {
      // Go back to default state when hiding the modal
      const stateObj = this.getDefaultState(loadPreferences());
      stateObj.goingToWizard = this.state.goingToWizard;
      this.setState(stateObj);
    }
  }

  onExcelMessageLimitChange = event => {
    this.setState({
      preferences: {
        ...this.state.preferences,
        excelMessageLimit: Number(event.target.value)
      }
    });
  };

  toggleShowSelfEmployedModal = show => {
    const pinnacleIdSettings = { ...this.state.pinnacleIdSettings };
    pinnacleIdSettings.showSelfEmployedModal = show;
    this.setState({ pinnacleIdSettings });
  };

  toggleNewsletter = e => {
    const pinnacleIdSettings = { ...this.state.pinnacleIdSettings };
    pinnacleIdSettings.newsletter = e.target.checked;
    this.setState({ pinnacleIdSettings });
  };

  validatePinnacleIdSettings = state => {
    const errors = {};
    const { firstName, lastName, email, job, country } =
      state || this.state.pinnacleIdSettings;

    if (isBlank(firstName) || isBlank(lastName)) {
      errors.name = 'Both first and last name are required';
    }

    if (typeof email !== 'string' || isBlank(email)) {
      errors.email = 'Email is required';
    } else if (!validateEmail(email)) {
      errors.email = 'Please enter a valid email address';
    } else {
      const emailDomain = email.substring(email.lastIndexOf('@') + 1);
      if (DISALLOWED_EMAILS.has(emailDomain.toLowerCase())) {
        errors.email = NON_WORK_EMAIL_ERROR;
      }
    }

    if (isBlank(job)) {
      errors.job = 'Job is a required field';
    }

    if (isBlank(country)) {
      errors.job = 'Country is a required field';
    }

    const pinnacleIdSettings = { ...this.state.pinnacleIdSettings };
    pinnacleIdSettings.errors = errors;
    this.setState({ pinnacleIdSettings });

    if (!isEmpty(errors)) {
      return errors;
    }

    return null;
  };

  handlePinnacleIdInputChange = property => {
    return e => {
      const stateObj = { ...this.state.pinnacleIdSettings };
      stateObj[property] = e.target.value;

      const errObj = stateObj.errors;
      if (property === 'firstName' || property === 'lastName') {
        errObj.name = null;
      } else {
        errObj[property] = null;
      }
      errObj.form = null;
      stateObj.errors = errObj;

      this.setState({
        pinnacleIdSettings: stateObj
      });
    };
  };

  handlePinnacleIdSelectChange = property => {
    return opt => {
      if (opt && opt.value) {
        const stateObj = { ...this.state.pinnacleIdSettings };
        stateObj[property] = opt.value;

        let errObj = stateObj.errors;
        errObj[property] = null;
        errObj.form = null;

        stateObj.errors = errObj;

        this.setState({
          pinnacleIdSettings: stateObj
        });
      }
    };
  };

  extractPinnacleIdAttributesFromState = pinnacleIdSettingsState => {
    return omit(pinnacleIdSettingsState, ['errors', 'showSelfEmployedModal']);
  };

  getPinnacleIdAttributeDifferences = () => {
    const originalAttributes = this.extractPinnacleIdAttributesFromState(
      this.state.ogPinnacleIdSettings
    );
    const currentAttributes = this.extractPinnacleIdAttributesFromState(
      this.state.pinnacleIdSettings
    );
    const differentAttributes = {};

    const attributeList = keys(originalAttributes);
    for (let attr of attributeList) {
      let originalAttr = originalAttributes[attr];
      let currentAttr = currentAttributes[attr];
      if (attr === 'email') {
        if (originalAttr) {
          originalAttr = originalAttr.toLowerCase();
        }
        if (currentAttr) {
          currentAttr = currentAttr.toLowerCase();
        }
      }
      if (originalAttr !== currentAttr) {
        differentAttributes[attr] = currentAttr === '' ? null : currentAttr;
      }
    }
    if (differentAttributes.newsletter !== undefined) {
      differentAttributes.newsletter = differentAttributes.newsletter ? 1 : 0;
    }
    if (differentAttributes.firstName) {
      differentAttributes.firstName = trim(differentAttributes.firstName);
    }
    if (differentAttributes.lastName) {
      differentAttributes.lastName = trim(differentAttributes.lastName);
    }

    return differentAttributes;
  };

  toggleRequestBeingMade = requestBeingMade => {
    this.setState({ requestBeingMade });
  };

  savePinnacleIdSettings = async () => {
    const res = {};
    if (this.state.pinnacleIdSettings) {
      const pinnacleIdErrors = this.validatePinnacleIdSettings();
      const hasErrors = !isEmptyOrHasNullValues(pinnacleIdErrors);
      if (hasErrors) {
        // Send them the Pinnacle ID Settings page
        // to see the errors
        res.success = false;
        this.setState({ activePreference: PREFERENCES.PINNACLE_ID });
        return res;
      } else {
        const attributeDifferences = this.getPinnacleIdAttributeDifferences();
        if (attributeDifferences) {
          const tokens = this.state.preferences.identity;
          const res = await this.props.pinnacleId.update(
            tokens,
            attributeDifferences
          );
          if (res.success) {
            // Their idToken has changed, we need to refresh!
            await this.props.refreshPinnacleId();
            return res;
          } else {
            return res;
          }
        }
      }
    }
    // There wasn't anything to save, let's keep this
    // show moving...
    return {
      success: true
    };
  };

  onSavePreferences = async retrying => {
    const failWithError = err => {
      this.toggleRequestBeingMade(false);
      const pinnacleIdSettings = { ...this.state.pinnacleIdSettings };
      pinnacleIdSettings.errors = { form: err };
      this.setState({ pinnacleIdSettings });
    };

    // Save the preferences that do not require a remote call first
    savePreferences(this.state.preferences);

    // Now let's save the preferences that require calls out to the internet...
    this.toggleRequestBeingMade(true);
    const res = await this.savePinnacleIdSettings();
    if (res.success) {
      this.toggleRequestBeingMade(false);
      if (res.data) {
        // Verification is required
        this.setState({
          verifications: this.createVerificationObjectsFromMetadata(res.data)
        });
      } else {
        // No verification required
        this.props.hide();
      }
    } else if (res.error) {
      if (res.error.indexOf('Access Token') > -1) {
        if (!retrying) {
          // Their access token has expired, let's try to recover
          await this.props.refreshPinnacleId();
          await this.onSavePreferences(true);
        } else {
          // We can't recover, but they should be
          // seeing the option to login again at this point
          failWithError('Your Pinnacle ID could not be validated');
        }
      } else {
        failWithError(res.error);
      }
    }
    // else there are errors on the screen that were
    // set during the form validation in this.savePinnacleIdSettings()
  };

  onSidebarOptionClick = preference => {
    if (!this.state.requestBeingMade) {
      this.setState({
        activePreference: preference,
        showChangePassword: false,
        changePasswordForm: {
          currentPassword: '',
          newPassword: '',
          verifyNewPassword: '',
          errors: {}
        }
      });
    }
  };

  setupPinnacleId = () => {
    this.props.launchLogin();
    this.props.hide();
  };

  verifying = () => this.state.verifications.length;

  verify = async () => {
    this.setState({ requestBeingMade: true });
    const tokens = this.state.preferences.identity;
    // We're always only going to be working with the first
    // obj in the array, and we can't call this unless the array
    // has at least one item
    const verificationObj = this.state.verifications[0];
    const code = verificationObj.form.verificationDigits.join('');
    const res = await this.props.pinnacleId.verify(tokens, 'email', code);
    if (res.success) {
      // Let's refresh their PinnacleID
      // tokens to reflect the verification
      await this.props.refreshPinnacleId();
      this.setState({ requestBeingMade: false });
      // Remove the verification object from state
      // if the verifications array is empty, close the modal
      const verificationsLeftToVerify = this.removeCurrentVerificationObjectFromState();
      if (verificationsLeftToVerify.length === 0) {
        this.props.hide();
      }
    } else {
      this.setState({ requestBeingMade: false });
      verificationObj.form.errors = { form: res.error };
      this.updateCurrentVerificationObject(verificationObj);
    }
  };

  skipVerification = () => {
    const verificationsLeftToVerify = this.removeCurrentVerificationObjectFromState();
    if (verificationsLeftToVerify.length === 0) {
      // We're done here, let's refresh their PinnacleID
      // tokens to reflect the verification (or lack their of)
      // and close the modal!
      this.props.refreshPinnacleId().then(() => null);
      this.props.hide();
    }
  };

  resendVerification = async verificationObject => {
    if (!this.state.requestBeingMade) {
      return await this.requestVerificationCodeForEmail(verificationObject);
    }
  };

  setErrorFor = (setting, err) => {
    const pinnacleIdSettings = { ...this.state.pinnacleIdSettings };
    const errors = pinnacleIdSettings.errors;
    errors[setting] = err;
    this.setState({ pinnacleIdSettings });
  };

  requestVerificationCode = async (type, verificationObject) => {
    if (!this.state.requestBeingMade) {
      this.setState({ requestBeingMade: true, isResending: true });
      const tokens = this.state.preferences.identity;
      const res = await this.props.pinnacleId.requestVerification(tokens, type);
      this.setState({ requestBeingMade: false, isResending: false });
      if (res.success) {
        if (!verificationObject) {
          this.setState({
            verifications: this.createVerificationObjectsFromMetadata(res.data)
          });
        }
      } else {
        if (verificationObject) {
          verificationObject.form.errors = {
            ...verificationObject.form.errors,
            form: res.error
          };
          this.updateCurrentVerificationObject(verificationObject);
        } else {
          switch (type) {
            case 'email':
              this.setErrorFor('email', res.error);
              break;
            default:
              this.setErrorFor('form', res.error);
              break;
          }
        }
      }
      return res;
    }
  };

  requestVerificationCodeForEmail = async verificationObject => {
    this.setErrorFor('email', null);
    this.setState({ sendingEmailVerificationCode: true });
    const res = await this.requestVerificationCode('email', verificationObject);
    this.setState({ sendingEmailVerificationCode: false });
    return res;
  };

  removeCurrentVerificationObjectFromState = () => {
    const newVerificationsArray = tail(this.state.verifications);
    this.setState({ verifications: newVerificationsArray });
    return newVerificationsArray;
  };

  updateCurrentVerificationObject = verificationObject => {
    const newVerificationArray = [
      verificationObject,
      ...tail(this.state.verifications)
    ];
    this.setState({ verifications: newVerificationArray });
  };

  createVerificationObjectsFromMetadata = metadata => {
    function createVerificationObj(destination) {
      return {
        form: {
          codeDeliveredTo: destination,
          verificationDigits: ['', '', '', '', '', ''],
          errors: {}
        }
      };
    }
    const verificationObjs = [];
    if (metadata.smsDestination) {
      verificationObjs.push(createVerificationObj(metadata.smsDestination));
    }
    if (metadata.emailDestination) {
      verificationObjs.push(createVerificationObj(metadata.emailDestination));
    }
    if (metadata.codeDeliveredTo) {
      verificationObjs.push(
        createVerificationObj(this.state.ogPinnacleIdSettings.email)
      );
    }
    return verificationObjs;
  };

  renderSubmitButtonText = () => {
    if (this.verifying()) {
      if (this.state.requestBeingMade) {
        return (
          <span>
            Verifying <i className="fa fa-spinner fa-pulse fa-fw" />
          </span>
        );
      } else {
        return <span>Verify</span>;
      }
    } else {
      if (this.state.requestBeingMade) {
        if (this.state.isResending) {
          return (
            <span>
              Requesting Verification{' '}
              <i className="fa fa-spinner fa-pulse fa-fw" />
            </span>
          );
        } else {
          return (
            <span>
              Saving <i className="fa fa-spinner fa-pulse fa-fw" />
            </span>
          );
        }
      } else {
        return <span>Save</span>;
      }
    }
  };

  toggleChangePassword = show => {
    this.setState({ showChangePassword: show });
  };

  setChangePasswordField = (fieldName, value) => {
    const changePasswordForm = { ...this.state.changePasswordForm };
    if (
      fieldName !== 'errors' &&
      changePasswordForm.errors[fieldName] != null
    ) {
      changePasswordForm.errors[fieldName] = null;
      if (changePasswordForm.errors.form != null) {
        changePasswordForm.errors.form = null;
      }
    }
    changePasswordForm[fieldName] = value;
    this.setState({ changePasswordForm });
  };

  validateChangePasswordForm = form => {
    const errors = {};
    const { currentPassword, newPassword, verifyNewPassword } = form;
    const FIELD_IS_REQUIRED_ERR_MSG = 'This field is required';

    if (isBlank(currentPassword)) {
      errors.currentPassword = FIELD_IS_REQUIRED_ERR_MSG;
    }
    if (isBlank(newPassword)) {
      errors.newPassword = FIELD_IS_REQUIRED_ERR_MSG;
    } else if (currentPassword === newPassword) {
      errors.newPassword =
        'New password cannot be the same as current password.';
    } else {
      const passwordError = validatePassword(newPassword);
      if (passwordError) {
        // An error message is already displayed, so we use this to not trigger another one
        // but also to fail validation and not send the request
        errors.form = 'PASSWORD_COMPLEXITY';
      }
    }
    if (isBlank(verifyNewPassword)) {
      errors.verifyNewPassword = FIELD_IS_REQUIRED_ERR_MSG;
    } else if (newPassword !== verifyNewPassword) {
      errors.verifyNewPassword = 'This field must match your new password.';
    }

    if (!isEmpty(errors)) {
      return errors;
    }

    return null;
  };

  changePassword = async () => {
    const form = this.state.changePasswordForm;
    this.setState({ requestBeingMade: true });
    const errors = this.validateChangePasswordForm(form);
    if (errors) {
      this.setState({ requestBeingMade: false });
      this.setChangePasswordField('errors', errors);
    } else {
      const tokens = this.state.preferences.identity;
      const res = await this.props.pinnacleId.changePassword(
        tokens,
        form.currentPassword,
        form.newPassword,
        form.verifyNewPassword
      );
      this.setState({ requestBeingMade: false });
      if (res.success) {
        this.setState({
          requestBeingMade: false,
          showChangePassword: false,
          changePasswordForm: {
            currentPassword: '',
            newPassword: '',
            verifyNewPassword: '',
            errors: {}
          }
        });
      } else {
        this.setState({ requestBeingMade: false });
        this.setChangePasswordField('errors', {
          form: res.error
        });
      }
    }
  };

  renderPinnacleIdComponent = () => {
    const { identity } = this.state.preferences;
    if (identity) {
      if (this.verifying()) {
        const verificationData = this.state.verifications[0];
        const setVerificationFormDigit = (digit, keyPressed) => {
          const newVerificationData = { ...verificationData };
          const form = newVerificationData.form;
          const digits = form.verificationDigits;
          if (hasNumber(keyPressed)) {
            digits[digit - 1] = keyPressed;
            form.errors = {};
          }
          if (keyPressed === 'Backspace' || keyPressed === 'Delete') {
            digits[digit - 1] = '';
            form.errors = {};
          }

          this.updateCurrentVerificationObject(newVerificationData);
        };
        return (
          <PinnacleIdVerification
            {...verificationData}
            setVerificationFormDigit={setVerificationFormDigit}
            isResending={this.state.isResending}
            resend={() => this.resendVerification(verificationData)}
          />
        );
      } else {
        return (
          <PinnacleIdSettings
            requestBeingMade={this.state.requestBeingMade}
            pinnacleIdSettings={this.state.pinnacleIdSettings}
            ogPinnacleIdSettings={this.state.ogPinnacleIdSettings}
            toggleShowSelfEmployedModal={this.toggleShowSelfEmployedModal}
            toggleNewsletter={this.toggleNewsletter}
            handleInputChange={this.handlePinnacleIdInputChange}
            handleSelectChange={this.handlePinnacleIdSelectChange}
            sendingEmailVerificationCode={
              this.state.sendingEmailVerificationCode
            }
            requestVerificationCodeForEmail={
              this.requestVerificationCodeForEmail
            }
            toggleChangePassword={this.toggleChangePassword}
            setChangePasswordField={this.setChangePasswordField}
            showChangePassword={this.state.showChangePassword}
            changePasswordForm={this.state.changePasswordForm}
          />
        );
      }
    } else {
      return (
        <div className="pinnacle-id-benefits-prefs">
          <h2 className="preference-title">Setup Your Pinnacle ID!</h2>
          <p>
            Your Pinnacle ID is the account you use to access all Pinnacle 21
            services. With a single email and password you have access to the
            following:
          </p>
          <Benefits />
          <div className="actions">
            <Button onClick={this.setupPinnacleId} bsStyle="success">
              Setup Your Pinnacle ID
            </Button>
          </div>
        </div>
      );
    }
  };

  renderValidatorComponent = () => {
    const { excelMessageLimit } = this.state.preferences;
    return (
      <ValidatorSettings
        excelMessageLimit={excelMessageLimit}
        onExcelMessageLimitChange={this.onExcelMessageLimitChange}
      />
    );
  };

  render() {
    const { VALIDATOR, PINNACLE_ID } = PREFERENCES;

    const preferenceComponents = {};
    preferenceComponents[VALIDATOR] = this.renderValidatorComponent;
    preferenceComponents[PINNACLE_ID] = this.renderPinnacleIdComponent;
    return (
      <Modal dialogClassName="preferences-modal" show={this.props.show}>
        <Modal.Header>
          <Modal.Title>Preferences</Modal.Title>
        </Modal.Header>

        <Modal.Body className="preferences-body">
          <Sidebar
            activePreference={this.state.activePreference}
            onOptionClick={this.onSidebarOptionClick}
          >
            <SidebarOption
              name={VALIDATOR}
              disabled={this.state.requestBeingMade || this.verifying()}
            />
            <SidebarOption name={PINNACLE_ID} />
          </Sidebar>

          <div className="col-md-9 preferences-content">
            {preferenceComponents[this.state.activePreference]()}
          </div>
        </Modal.Body>

        <Modal.Footer className="preferences-footer">
          <Button
            bsStyle="primary"
            onClick={() => {
              if (this.verifying()) {
                this.verify().then(() => null);
              } else if (this.state.showChangePassword) {
                this.changePassword().then(() => null);
              } else {
                this.onSavePreferences().then(() => null);
              }
            }}
            disabled={this.state.requestBeingMade}
          >
            {this.renderSubmitButtonText()}
          </Button>
          <Button
            onClick={this.verifying() ? this.skipVerification : this.props.hide}
            disabled={this.state.requestBeingMade}
          >
            {this.verifying() ? 'Skip' : 'Cancel'}
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
