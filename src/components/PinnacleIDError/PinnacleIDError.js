/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { Button, Modal } from 'react-bootstrap';

export default props => {
  return (
    <Modal dialogClassName="about-modal" show={props.show}>
      <Modal.Header>
        <Modal.Title>Your Pinnacle ID Could Not Be Validated</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <p>
          Please login again to continue receiving automatic Controlled
          Terminology updates.
        </p>
      </Modal.Body>

      <Modal.Footer>
        <Button className="btn" onClick={props.close}>
          Continue
        </Button>
        <Button className="btn btn-primary" onClick={props.goToLogin}>
          Login
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
