/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { ProgressBar } from 'react-bootstrap';

import './ProgressDialog.css';

const { ipcRenderer } = window;

export class ProgressDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      summary: '',
      detail: '',
      percent: 0,
      isComplete: false,
      isFailed: false
    };
    ipcRenderer.removeAllListeners('progress-dialog-summary');
    ipcRenderer.on('progress-dialog-summary', (sender, { value }) => {
      this.setState({ summary: value });
    });
    ipcRenderer.removeAllListeners('progress-dialog-detail');
    ipcRenderer.on('progress-dialog-detail', (sender, { value }) => {
      this.setState({ detail: value });
    });
    ipcRenderer.removeAllListeners('progress-dialog-progress');
    ipcRenderer.on('progress-dialog-progress', (sender, { percent }) => {
      this.setState({ percent: Math.min(100, percent) });
    });
    ipcRenderer.removeAllListeners('progress-dialog-complete');
    ipcRenderer.on('progress-dialog-complete', (sender, { isComplete }) => {
      this.setState({ isComplete: isComplete });
    });
    ipcRenderer.removeAllListeners('progress-dialog-failed');
    ipcRenderer.on('progress-dialog-failed', (sender, { isFailed }) => {
      this.setState({ isFailed: isFailed });
    });
    ipcRenderer.removeAllListeners('progress-dialog-ready');
    ipcRenderer.send('progress-dialog-ready');
  }

  render() {
    return (
      <div className="progress-container">
        <div className="summary">{this.state.summary}</div>
        <ProgressBar
          striped
          active
          bsStyle={
            this.state.isFailed
              ? 'danger'
              : this.state.isComplete
              ? 'success'
              : null
          }
          now={this.state.percent}
        />
        <div className="detail">{this.state.detail}</div>
      </div>
    );
  }
}
