/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { omit } from 'lodash';

import clinicalTrialsMinerRedux from './screens/ClinicalTrialsMiner/clinicalTrialsMinerRedux';
import connectToEnterpriseRedux from './screens/ConnectToEnterprise/connectToEnterpriseRedux';
import converterRedux from './screens/Converter/converterRedux';
import createSpecRedux from './screens/CreateSpec/createSpecRedux';
import setupWizardRedux from './components/SetupWizard/setupWizardRedux';
import validatorRedux from './screens/Validator/validatorRedux';
import { loadState, saveState } from './utils/localStorage';

const loadedState = loadState();

const reducer = combineReducers({
  clinicalTrialsMiner: clinicalTrialsMinerRedux,
  converter: converterRedux,
  createSpec: createSpecRedux,
  enterprise: connectToEnterpriseRedux,
  setup: setupWizardRedux,
  validator: validatorRedux
});

let store;
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
if (loadedState) {
  store = createStore(
    reducer,
    loadedState,
    composeEnhancers(applyMiddleware(thunk))
  );
} else {
  store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
  saveState(omit(store.getState(), 'setup'));
}

store.subscribe(() => {
  saveState(omit(store.getState(), 'setup'));
});

export default store;
