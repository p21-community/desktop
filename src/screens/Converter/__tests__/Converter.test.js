/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import ConverterRenderer, {
  Converter,
  OUTPUT_FORMAT,
  SOURCE_FORMATS
} from '../Converter';

test('ConverterRenderer renders correctly', () => {
  const ShallowConverterRenderer = new ReactShallowRenderer();

  ShallowConverterRenderer.render(
    <ConverterRenderer electronAPI={{ ipcRenderer: {} }} />
  );

  expect(ShallowConverterRenderer.getRenderOutput()).toMatchSnapshot();
});

test('Converter renders correctly', () => {
  const shallowRenderer = new ReactShallowRenderer();

  shallowRenderer.render(<Converter electronAPI={{}} />);

  expect(shallowRenderer.getRenderOutput()).toMatchSnapshot();
});

test('convert shows modal and sends the correct event', () => {
  const sendSpy = sinon.spy();
  const showModalSpy = sinon.spy();
  const electronAPI = {
    ipcRenderer: {
      send: sendSpy
    }
  };

  const ConverterComponent = shallow(
    <Converter electronAPI={electronAPI} showModal={showModalSpy} />
  );

  const state = {
    definePath: 'My Define Path',
    outputPath: 'My Output Path',
    outputFormatId: 3,
    paths: ['Path'],
    sourceFormatId: 1
  };

  const sentValues = {
    definePath: state.definePath,
    outputFormat: OUTPUT_FORMATS[state.outputFormatId - 1].name,
    outputPath: state.outputPath,
    paths: state.paths,
    sourceFormat: SOURCE_FORMATS[state.sourceFormatId - 1].shortName
  };

  ConverterComponent.setState(state);
  ConverterComponent.instance().convert({
    preventDefault: jest.fn()
  });

  expect(showModalSpy.calledOnce).toBe(true);
  expect(sendSpy.calledWith('convert', sentValues)).toBe(true);
});

test("convert doesn't send the definePath if it's not needed", () => {
  const sendSpy = sinon.spy();
  const showModalSpy = sinon.spy();
  const electronAPI = {
    ipcRenderer: {
      send: sendSpy
    }
  };

  const ConverterComponent = shallow(
    <Converter electronAPI={electronAPI} showModal={showModalSpy} />
  );

  const state = {
    definePath: 'My Define Path',
    outputPath: 'My Output Path',
    outputFormatId: 2,
    paths: ['Path'],
    sourceFormatId: 1
  };

  const sentValues = {
    definePath: '',
    outputFormat: OUTPUT_FORMATS[state.outputFormatId - 1].name,
    outputPath: state.outputPath,
    paths: state.paths,
    sourceFormat: SOURCE_FORMATS[state.sourceFormatId - 1].shortName
  };

  ConverterComponent.setState(state);
  ConverterComponent.instance().convert({
    preventDefault: jest.fn()
  });

  expect(showModalSpy.calledOnce).toBe(true);
  expect(sendSpy.calledWith('convert', sentValues)).toBe(true);
});

test('onDefinePathChange updates the state correctly', () => {
  const ConverterComponent = shallow(<Converter electronAPI={{}} />);

  const definePath = 'My Path';

  const eventObj = {
    preventDefault: jest.fn(),
    target: {
      files: [{ path: definePath }]
    }
  };

  expect(ConverterComponent.state('definePath')).toEqual('');

  ConverterComponent.instance().onDefinePathChange(eventObj);

  expect(ConverterComponent.state('definePath')).toEqual(definePath);
});

test("onDefinePathChange clears the definePath if a file isn't provided", () => {
  const ConverterComponent = shallow(<Converter electronAPI={{}} />);

  const definePath = 'My Path';

  const eventObj = {
    preventDefault: jest.fn(),
    target: {
      files: []
    }
  };

  ConverterComponent.setState({ definePath: definePath });

  expect(ConverterComponent.state('definePath')).toEqual(definePath);

  ConverterComponent.instance().onDefinePathChange(eventObj);

  expect(ConverterComponent.state('definePath')).toEqual('');
});

test('onFileChange updates the state correctly', () => {
  const ConverterComponent = shallow(<Converter electronAPI={{}} />);

  const paths = ['My Path'];

  expect(ConverterComponent.state('paths')).toEqual([]);

  ConverterComponent.instance().onFileChange(paths);

  expect(ConverterComponent.state('paths')).toEqual(paths);
});

test('onOutputFormatChange updates the state correctly', () => {
  const ConverterComponent = shallow(<Converter electronAPI={{}} />);

  const selection = {
    value: 2
  };

  expect(ConverterComponent.state('outputFormatId')).toEqual(1);

  ConverterComponent.instance().onOutputFormatChange(selection);

  expect(ConverterComponent.state('outputFormatId')).toEqual(selection.value);
});

test('onOutputPathChange updates the state correctly', () => {
  const ConverterComponent = shallow(<Converter electronAPI={{}} />);

  const path = 'My Path';

  expect(ConverterComponent.state('outputPath')).toEqual('');

  ConverterComponent.instance().onOutputPathChange(path);

  expect(ConverterComponent.state('outputPath')).toEqual(path);
});

test('onSourceFormatChange updates the state correctly', () => {
  const ConverterComponent = shallow(<Converter electronAPI={{}} />);

  const selection = {
    value: 2
  };

  expect(ConverterComponent.state('sourceFormatId')).toEqual(1);

  ConverterComponent.instance().onSourceFormatChange(selection);

  expect(ConverterComponent.state('sourceFormatId')).toEqual(selection.value);
});
