/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { Button, Col, ControlLabel, Form, FormGroup } from 'react-bootstrap';

import ToolPage from '../shared/ToolPage';
import { DropZone, FileSelector } from '../shared/FormUtils';
import { loadKey, loadPreferences } from '../../utils/localStorage';
import { OUTPUT_FORMAT, ACTION } from './converterRedux';

export const SOURCE_FORMATS = [
  {
    name: 'SAS® Transport (XPORT)',
    shortName: 'SasTransport'
  }
];

const ConverterPropTypes = {
  electronAPI: PropTypes.object.isRequired,
  outputFormat: PropTypes.string.isRequired
};

export class Converter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      definePath: '',
      outputPath: '',
      paths: [],
      sourceFormatId: 1
    };
  }

  convert = event => {
    event.preventDefault();
    this.props.showModal();

    const outputFormat = this.props.outputFormat,
      { ipcRenderer } = this.props.electronAPI;

    const parameters = {
      definePath:
        outputFormat === OUTPUT_FORMAT.DATASET_XML ? this.state.definePath : '',
      outputFormat,
      outputPath: this.state.outputPath,
      paths: this.state.paths,
      sourceFormat: SOURCE_FORMATS[this.state.sourceFormatId - 1].shortName,
      preferences: {
        ...loadPreferences(),
        userAgent: window.navigator.userAgent,
        anonymousId: loadKey('app-id')
      }
    };

    ipcRenderer.removeAllListeners('getToolParameters');
    ipcRenderer.once('getToolParameters', () => {
      const toolParameters = {
        'Source Format': parameters.sourceFormat,
        'Source Data': parameters.paths,
        'Output Format': parameters.outputFormat,
        'Output Path': parameters.outputPath
      };
      ipcRenderer.send('toolParameters', 'Converter', toolParameters);
    });

    ipcRenderer.send('convert', parameters);
  };

  onDefinePathChange = definePath => {
    this.setState({ definePath });
  };

  onFileChange = paths => {
    this.setState({ paths });
  };

  onOutputFormatChange = ({ value }) => {
    this.props.dispatch(ACTION.setOutputFormat(value));
  };

  onOutputPathChange = outputPath => {
    this.setState({ outputPath });
  };

  onSourceFormatChange = selection => {
    this.setState({ sourceFormatId: selection.value });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.convert}>
        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            Source Format
          </Col>
          <Col sm={3}>
            <Select
              options={SOURCE_FORMATS.map((format, index) => ({
                value: index + 1,
                label: format.name
              }))}
              value={this.state.sourceFormatId}
              onChange={this.onSourceFormatChange}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            Source Data
          </Col>
          <Col sm={10}>
            <DropZone
              accept=".xpt"
              onChange={this.onFileChange}
              paths={this.state.paths}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            Output Format
          </Col>
          <Col sm={3}>
            <Select
              options={Object.values(OUTPUT_FORMAT).map(outputFormat => ({
                value: outputFormat,
                label: outputFormat
              }))}
              value={this.props.outputFormat}
              onChange={this.onOutputFormatChange}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </FormGroup>

        <FileSelector
          label="Output Path"
          path={this.state.outputPath}
          directory={true}
          onChange={this.onOutputPathChange}
          electronAPI={this.props.electronAPI}
        />

        {this.props.outputFormat === OUTPUT_FORMAT.DATASET_XML && (
          <FileSelector
            label="Define.xml"
            accept=".xml"
            path={this.state.definePath}
            onChange={this.onDefinePathChange}
            electronAPI={this.props.electronAPI}
          />
        )}

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button
              type="submit"
              bsStyle="primary"
              disabled={
                !(
                  this.state.paths.length &&
                  this.state.outputPath &&
                  (this.props.outputFormat === OUTPUT_FORMAT.DATASET_XML
                    ? this.state.definePath
                    : true)
                )
              }
            >
              Convert
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

Converter.propTypes = ConverterPropTypes;

const mapStateToProps = ({ converter }) => ({
  outputFormat: converter.outputFormat
});

const ConnectedConverter = connect(mapStateToProps)(Converter);

const rendererPropTypes = {
  electronAPI: PropTypes.object.isRequired
};

function Renderer(props) {
  return (
    <ToolPage
      ipcRenderer={props.electronAPI.ipcRenderer}
      title="Data Converter"
      description={
        'convert data between SAS XPORT, Excel, CSV, and Dataset-XML'
      }
      widgetTitle="Convert Data"
      widgetIcon="fa fa-cogs"
      modalTitle="Convert Data"
      openText="Open Folder"
    >
      <ConnectedConverter electronAPI={props.electronAPI} />
    </ToolPage>
  );
}

Renderer.propTypes = rendererPropTypes;

export default Renderer;
