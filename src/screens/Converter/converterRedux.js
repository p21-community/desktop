/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

// CONSTANTS
export const OUTPUT_FORMAT = {
  EXCEL: 'Excel',
  CSV: 'CSV',
  DATASET_XML: 'Dataset XML'
};
const SET_OUTPUT_FORMAT = 'SET_OUTPUT_FORMAT';

// ACTIONS
export const ACTION = {
  setOutputFormat: outputFormat => ({
    type: SET_OUTPUT_FORMAT,
    outputFormat
  })
};

// INITIAL STATE
const initialState = {
  outputFormat: OUTPUT_FORMAT.EXCEL
};

const converterRedux = (state = initialState, { type, outputFormat }) => {
  switch (type) {
    case SET_OUTPUT_FORMAT:
      return {
        outputFormat
      };
    default:
      return state;
  }
};

export default converterRedux;
