/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ClipLoader } from 'halogenium';
import { Redirect, withRouter } from 'react-router-dom';

class Environment extends Component {
  static propTypes = {
    connectTo: PropTypes.string.isRequired,
    environment: PropTypes.object.isRequired,
    onLogin: PropTypes.func.isRequired,
    onLogout: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    if (props.connectTo === 'local') {
      this.url = `http://localhost:9000`;
    } else {
      this.url = `https://${props.connectTo}.pinnacle21.net`;
    }
  }

  componentDidMount() {
    const webview = document.querySelector('webview');

    // TODO: The did-get-response-details event might not work anymore.
    // TODO: See https://github.com/electron/electron/pull/12477. And electron 3.0.0 release notes
    webview.addEventListener('did-get-response-details', details => {
      const { resourceType, referrer, httpResponseCode, originalURL } = details;
      if (resourceType === 'mainFrame') {
        if (
          referrer.startsWith(`${this.url}/login?`) &&
          httpResponseCode === 200
        ) {
          this.props.onLogin();
        } else if (
          originalURL === `${this.url}/logout?` &&
          httpResponseCode === 200
        ) {
          this.props.onLogout();
        }
      }
    });
  }

  render() {
    let reportURL;

    if (this.props.location.search) {
      reportURL = this.props.location.search.substring(5);
    }

    return this.props.location.pathname === '/connect' &&
      this.props.environment.loggedIn ? (
      <Redirect to="/" />
    ) : (
      <div className="environment">
        <div className="spinner">
          <ClipLoader size="80px" color="#ccc" />
        </div>
        <webview src={reportURL || this.url} className="webview" />
      </div>
    );
  }
}

export default withRouter(Environment);
