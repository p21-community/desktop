/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

// CONSTANTS
const SET_ENVIRONMENT = 'SET_ENVIRONMENT';

// ACTIONS
export const setEnvironment = environment => ({
  type: SET_ENVIRONMENT,
  environment
});

// INITIAL STATE
const initialState = {
  environment: {
    name: '',
    loggedIn: false
  }
};

const connectToEnterpriseReducer = (
  state = initialState,
  { type, environment }
) => {
  switch (type) {
    case SET_ENVIRONMENT:
      const newEnvironment = { ...state.environment, ...environment };
      return {
        environment: newEnvironment
      };
    default:
      return state;
  }
};

export default connectToEnterpriseReducer;
