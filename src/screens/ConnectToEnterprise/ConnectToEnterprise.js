/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Form, FormControl, FormGroup } from 'react-bootstrap';

import Environment from './components/Environment';
import { setEnvironment } from './connectToEnterpriseRedux';

class ConnectToEnterprise extends Component {
  constructor(props) {
    super(props);
    const { name } = props.environment;
    this.state = {
      connectTo: name ? name : '',
      loadEnvironment: name !== ''
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.loadEnvironment && !this.props.environment.name) {
      this.setState({ loadEnvironment: false });
    }
  }

  onChange = event => {
    this.setState({ connectTo: event.target.value });
  };

  onConnect = event => {
    event.preventDefault();
    this.props.onConnect(this.state.connectTo);
    this.setState({ loadEnvironment: true });
  };

  render() {
    if (this.state.loadEnvironment) {
      return (
        <Environment
          connectTo={this.state.connectTo}
          environment={this.props.environment}
          onLogin={this.props.onLogin}
          onLogout={this.props.onLogout}
        />
      );
    } else {
      return (
        <div>
          <div className="main-header center">
            <h3>Log in with your Enterprise account</h3>
            <p>
              to experience the productivity of Enterprise in the privacy of
              your local environment
            </p>
          </div>

          <div className="main-content connect">
            <div className="widget">
              <div className="widget-header center">
                <h3>Enter your company's URL</h3>
              </div>
              <div className="widget-content">
                <Form horizontal onSubmit={this.onConnect}>
                  <FormGroup className="center">
                    <Col sm={12}>
                      <FormControl
                        placeholder="your-company-url"
                        value={this.state.connectTo}
                        onChange={this.onChange}
                      />
                      <span>.pinnacle21.net</span>
                    </Col>
                  </FormGroup>
                  <FormGroup className="center">
                    <Button type="submit" bsStyle="primary">
                      Continue <i className="fa fa-angle-right" />
                    </Button>
                  </FormGroup>
                </Form>
              </div>
            </div>

            <div className="upgrade center">
              <h3>Not an Enterprise user?</h3>
              <a
                rel="noopener noreferrer"
                target="_blank"
                href="https://www.pinnacle21.com"
              >
                Learn why you should upgrade
              </a>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = ({ enterprise }) => ({
  environment: enterprise.environment
});

const mapDispatchToProps = dispatch => ({
  onConnect: name => {
    dispatch(setEnvironment({ name }));
  },
  onLogin: () => {
    window.require('electron').ipcRenderer.send('maximizeWindow');
    setTimeout(() => {
      dispatch(setEnvironment({ loggedIn: true }));
    }, 1000);
  },
  onLogout: () => {
    window.require('electron').ipcRenderer.send('unmaximizeWindow');
    setTimeout(() => {
      dispatch(setEnvironment({ loggedIn: false }));
    }, 1000);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectToEnterprise);
