/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert, AlertContainer } from 'react-bs-notifier';
import { isEqual } from 'lodash';

import { loadKey } from '../../utils/localStorage';
import ToolPage from '../shared/ToolPage';
import XptExtraction from './components/XptExtraction';
import XmlExtraction from './components/XmlExtraction';
import { ACTION } from './createSpecRedux';

const CreateSpecPropTypes = {
  ipcRenderer: PropTypes.object.isRequired,
  config: PropTypes.string.isRequired
};

export class CreateSpec extends Component {
  constructor(props) {
    super(props);

    this.state = { configs: [], showAlert: false };
  }

  componentDidMount() {
    if (loadKey('configs')) {
      this.pullConfigs();
    }
    this.props.ipcRenderer.on(
      'configsUpdated',
      (event, remoteConfigsUpdated) => {
        this.pullConfigs(remoteConfigsUpdated);
      }
    );
  }

  componentWillUnmount() {
    this.props.ipcRenderer.removeAllListeners('configsUpdated');
  }

  hideAlert = () => {
    this.setState({ showAlert: false });
  };

  pullConfigs = remoteConfigsUpdated => {
    this.props.ipcRenderer.once('uniqueConfigs', (event, configs) => {
      this.setState({
        configs,
        showAlert: remoteConfigsUpdated && !isEqual(this.state.configs, configs)
      });
    });
    this.props.ipcRenderer.send('getUniqueConfigs');
  };

  render() {
    if (!this.state.configs.length) {
      return false;
    }
    return (
      <div>
        {this.state.showAlert && (
          <AlertContainer>
            <Alert type="info" onDismiss={this.hideAlert}>
              Configs were updated
            </Alert>
          </AlertContainer>
        )}
        <XptExtraction
          ipcRenderer={this.props.ipcRenderer}
          showModal={this.props.showModal}
          configs={this.state.configs}
          config={this.props.config}
          setConfig={this.props.setConfig}
        />
        <XmlExtraction
          ipcRenderer={this.props.ipcRenderer}
          config={this.props.config}
          showModal={this.props.showModal}
        />
      </div>
    );
  }
}

CreateSpec.propTypes = CreateSpecPropTypes;

const mapStateToProps = ({ createSpec }) => ({
  config: createSpec.config
});

const mapDispatchToProps = {
  setConfig: ACTION.setConfig
};

const ConnectedCreateSpec = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateSpec);

const rendererPropTypes = {
  ipcRenderer: PropTypes.object.isRequired
};

function Renderer(props) {
  return (
    <ToolPage
      ipcRenderer={props.ipcRenderer}
      title="Define.xml Generator"
      description={
        'create compliant Define.xml 2.0 for SDTM, SEND, and ADaM datasets'
      }
      widgetTitle="Create Excel Specification"
      widgetIcon="fa fa-pencil"
      modalTitle="Generating define.xml Excel spec"
      openText="Open Spec"
    >
      <ConnectedCreateSpec ipcRenderer={props.ipcRenderer} />
    </ToolPage>
  );
}

Renderer.propTypes = rendererPropTypes;

export default Renderer;
