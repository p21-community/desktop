/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

// CONSTANTS
const SET_CONFIG = 'SET_CONFIG';

// ACTIONS
export const ACTION = {
  setConfig: config => ({
    type: SET_CONFIG,
    config
  })
};

// INITIAL STATE
const initialState = {
  config: ''
};

const createSpecRedux = (state = initialState, action) => {
  switch (action.type) {
    case SET_CONFIG:
      return {
        ...state,
        config: action.config
      };
    default:
      return state;
  }
};

export default createSpecRedux;
