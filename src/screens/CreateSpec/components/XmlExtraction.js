/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Col, Form, FormGroup } from 'react-bootstrap';
import { FileSelector } from '../../shared/FormUtils';

import { loadKey, loadPreferences } from '../../../utils/localStorage';

const propTypes = {
  showModal: PropTypes.func.isRequired,
  ipcRenderer: PropTypes.object.isRequired,
  // TODO: Update CLI to not require config for spec creation from define.xml
  config: PropTypes.string.isRequired
};

export default class XmlExtraction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      path: ''
    };
  }

  onFileChange = path => {
    this.setState({ path });
  };

  generateExcelSpec = event => {
    event.preventDefault();

    this.props.showModal();
    this.props.ipcRenderer.send('generateExcelSpec', {
      config: null,
      paths: [this.state.path],
      preferences: {
        ...loadPreferences(),
        userAgent: window.navigator.userAgent,
        anonymousId: loadKey('app-id')
      },
      type: 'Define'
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.generateExcelSpec}>
        <fieldset>
          <legend>Import Define.xml</legend>
          <FileSelector
            label="Define.xml"
            path={this.state.path}
            accept=".xml"
            onChange={this.onFileChange}
          />
          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button
                type="submit"
                bsStyle="primary"
                disabled={!this.state.path.length}
              >
                Create
              </Button>
            </Col>
          </FormGroup>
        </fieldset>
      </Form>
    );
  }
}

XmlExtraction.propTypes = propTypes;
