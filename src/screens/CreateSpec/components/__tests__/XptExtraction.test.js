/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import sinon from 'sinon';
import { mount } from 'enzyme';
import XptExtraction from '../XptExtraction';

test('XptExtraction renders correctly', () => {
  const ipcRenderer = {
    sendSync: args => [
      {
        id: 'id',
        displayName: 'displayName'
      }
    ]
  };

  const renderer = new ReactShallowRenderer();
  renderer.render(<XptExtraction ipcRenderer={ipcRenderer} />);

  expect(renderer.getRenderOutput()).toMatchSnapshot();
});

test('generateExcelSpec shows modal and sends the correct event', () => {
  const sendSpy = sinon.spy();
  const showModalSpy = sinon.spy();
  const ipcRenderer = {
    send: sendSpy,
    sendSync: arg => []
  };
  const XptExtractionComponent = mount(
    <XptExtraction ipcRenderer={ipcRenderer} showModal={showModalSpy} />
  );

  const state = {
    configId: 1,
    paths: ['My Path']
  };

  XptExtractionComponent.setState(state);

  XptExtractionComponent.instance().generateExcelSpec({
    preventDefault: sinon.stub()
  });

  expect(showModalSpy.calledOnce).toBe(true);
  expect(sendSpy.calledWith('generateExcelSpec', state)).toBe(true);
});

test('onConfigChange updates the configId state value correctly', () => {
  const ipcRenderer = {
    sendSync: arg => []
  };

  const XptExtractionComponent = mount(
    <XptExtraction ipcRenderer={ipcRenderer} />
  );

  expect(XptExtractionComponent.state('configId')).toEqual(1);

  const configId = 2;

  XptExtractionComponent.instance().onConfigChange({
    value: configId
  });

  expect(XptExtractionComponent.state('configId')).toEqual(configId);
});

test('onFileChange updates the paths state value correctly', () => {
  const ipcRenderer = {
    sendSync: arg => []
  };

  const XptExtractionComponent = mount(
    <XptExtraction ipcRenderer={ipcRenderer} />
  );

  expect(XptExtractionComponent.state('paths')).toEqual([]);

  const state = {
    paths: ['My Path']
  };

  XptExtractionComponent.instance().onFileChange(state.paths);

  expect(XptExtractionComponent.state('paths')).toEqual(state.paths);
});
