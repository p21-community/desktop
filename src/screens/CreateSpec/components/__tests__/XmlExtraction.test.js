/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import XmlExtraction from '../XmlExtraction';

test('generateExcelSpec shows modal and sends the correct event', () => {
  const sendSpy = sinon.spy();
  const showModalSpy = sinon.spy();
  const ipcRenderer = {
    send: sendSpy
  };
  const XmlExtractionComponent = mount(
    <XmlExtraction ipcRenderer={ipcRenderer} showModal={showModalSpy} />
  );
  const state = {
    path: 'path'
  };

  XmlExtractionComponent.setState(state);
  XmlExtractionComponent.instance().generateExcelSpec({
    preventDefault: sinon.stub()
  });

  expect(showModalSpy.calledOnce).toBe(true);
  expect(sendSpy.calledWith('generateExcelSpec', { paths: [state.path] })).toBe(
    true
  );
});

test('onFileChange function updates the state correctly', () => {
  const spy = sinon.spy();
  const ipcRenderer = {
    send: spy
  };
  const XmlExtractionComponent = mount(
    <XmlExtraction ipcRenderer={ipcRenderer} />
  );
  const state = {
    path: 'my file path'
  };

  expect(XmlExtractionComponent.state('path')).toEqual('');

  XmlExtractionComponent.instance().onFileChange({
    preventDefault: jest.fn(),
    target: {
      files: [
        {
          path: state.path
        }
      ]
    }
  });

  expect(XmlExtractionComponent.state('path')).toEqual(state.path);

  XmlExtractionComponent.instance().onFileChange({
    preventDefault: jest.fn(),
    target: {
      files: []
    }
  });

  expect(XmlExtractionComponent.state('path')).toEqual('');
});
