/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import 'react-select/dist/react-select.css';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { Button, Col, ControlLabel, Form, FormGroup } from 'react-bootstrap';

import { DropZone } from '../../shared/FormUtils';
import { loadKey, loadPreferences } from '../../../utils/localStorage';

const propTypes = {
  ipcRenderer: PropTypes.object.isRequired,
  showModal: PropTypes.func.isRequired,
  configs: PropTypes.array.isRequired,
  config: PropTypes.string.isRequired,
  setConfig: PropTypes.func.isRequired
};

export default class XptExtraction extends Component {
  constructor(props) {
    super(props);

    this.state = { paths: [] };
  }

  componentDidMount() {
    const { config, configs } = this.props;
    const availableConfigs = configs.map(({ fileName }) => fileName);
    if (
      (!config && configs.length) ||
      (config && configs.length && !availableConfigs.includes(config))
    ) {
      this.props.setConfig(availableConfigs[0]);
    }
  }

  generateExcelSpec = event => {
    event.preventDefault();
    this.props.showModal();

    this.props.ipcRenderer.send('generateExcelSpec', {
      paths: this.state.paths,
      config: this.props.config,
      preferences: {
        ...loadPreferences(),
        userAgent: window.navigator.userAgent,
        anonymousId: loadKey('app-id')
      },
      type: 'SasTransport'
    });
  };

  getConfigs = () =>
    this.props.configs.map(({ fileName, displayName }) => ({
      value: fileName,
      label: displayName
    }));

  onConfigChange = selection => {
    this.props.setConfig(selection.value);
  };

  onFileChange = paths => {
    this.setState({ paths });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.generateExcelSpec}>
        <fieldset>
          <legend>Extract Metadata from SAS XPORT Datasets</legend>
          <FormGroup>
            <Col sm={2} componentClass={ControlLabel}>
              Source Data
            </Col>
            <Col sm={10}>
              <DropZone
                accept=".xpt"
                onChange={this.onFileChange}
                paths={this.state.paths}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col sm={2} componentClass={ControlLabel}>
              Configuration
            </Col>
            <Col sm={10}>
              <Select
                options={this.getConfigs()}
                value={this.props.config}
                onChange={this.onConfigChange}
                clearable={false}
                backspaceRemoves={false}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button
                type="submit"
                bsStyle="primary"
                disabled={!this.state.paths.length || !this.props.config}
              >
                Create
              </Button>
            </Col>
          </FormGroup>
        </fieldset>
      </Form>
    );
  }
}

XptExtraction.propTypes = propTypes;
