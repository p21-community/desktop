/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Col, Form, FormGroup } from 'react-bootstrap';

import ToolPage from '../shared/ToolPage';
import { FileSelector } from '../shared/FormUtils';
import { loadKey, loadPreferences } from '../../utils/localStorage';

const rendererPropTypes = {
  ipcRenderer: PropTypes.object.isRequired
};

function Renderer(props) {
  return (
    <ToolPage
      ipcRenderer={props.ipcRenderer}
      title="Define.xml Generator"
      description={
        'create compliant Define.xml 2.0 for SDTM, SEND, and ADaM datasets'
      }
      widgetTitle="Generate Define.xml"
      widgetIcon="fa fa-pencil"
      modalTitle="Generating Define.xml"
      openText="Open Define.xml"
    >
      <GenerateDefine ipcRenderer={props.ipcRenderer} />
    </ToolPage>
  );
}

Renderer.propTypes = rendererPropTypes;

export default Renderer;

const generateDefinePropTypes = {
  ipcRenderer: PropTypes.object.isRequired
};

export class GenerateDefine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      path: ''
    };
  }

  onFileChange = path => {
    this.setState({ path });
  };

  generateDefine = event => {
    event.preventDefault();
    this.props.showModal();

    this.props.ipcRenderer.send('generateDefine', {
      path: this.state.path,
      preferences: {
        ...loadPreferences(),
        userAgent: window.navigator.userAgent,
        anonymousId: loadKey('app-id')
      }
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.generateDefine}>
        <FileSelector
          label="Excel Spec"
          accept=".xls,.xlsx"
          path={this.state.path}
          onChange={this.onFileChange}
        />
        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button
              type="submit"
              bsStyle="primary"
              disabled={this.state.path ? false : true}
            >
              Generate
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

GenerateDefine.propTypes = generateDefinePropTypes;
