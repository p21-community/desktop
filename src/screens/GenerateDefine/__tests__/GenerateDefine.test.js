/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import renderer from 'react-test-renderer';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import GenerateDefineRenderer, { GenerateDefine } from '../GenerateDefine';

const ipcRenderer = {
  send: jest.fn(),
  on: jest.fn()
};

test('GenerateDefine renders correctly', () => {
  expect(
    renderer
      .create(<GenerateDefineRenderer ipcRenderer={ipcRenderer} />)
      .toJSON()
  ).toMatchSnapshot();
});

test("<input /> onChange event will change the state's path", () => {
  const generateDefineComponent = mount(
    <GenerateDefine ipcRenderer={ipcRenderer} />
  );
  const mockEvent = {
    target: {
      files: [
        {
          path: 'My Path'
        }
      ]
    }
  };

  generateDefineComponent
    .find('input[type="file"]')
    .simulate('change', mockEvent);
  expect(generateDefineComponent.state('path')).toEqual('My Path');
});

test(`<input /> onChange event with an empty value will set the state\'s 
      path to an empty string`, () => {
  const generateDefineComponent = mount(
    <GenerateDefine ipcRenderer={ipcRenderer} />
  );

  generateDefineComponent.setState({ path: 'My file' });
  expect(generateDefineComponent.state('path')).toEqual('My file');

  generateDefineComponent.find('input[type="file"]').simulate('change');
  expect(generateDefineComponent.state('path')).toEqual('');
});

test('generateDefine shows modal and sends the correct event', () => {
  const sendSpy = sinon.spy();
  const showModalSpy = sinon.spy();
  const ipcRenderer = {
    send: sendSpy
  };
  const generateDefineComponent = shallow(
    <GenerateDefine ipcRenderer={ipcRenderer} showModal={showModalSpy} />
  );
  const state = {
    path: 'path'
  };

  generateDefineComponent.setState(state);
  generateDefineComponent.instance().generateDefine({
    preventDefault: sinon.stub()
  });

  expect(showModalSpy.calledOnce).toBe(true);
  expect(sendSpy.calledWith('generateDefine', state)).toBe(true);
});
