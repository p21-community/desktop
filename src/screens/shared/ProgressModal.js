/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ProgressBar } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

const propTypes = {
  ipcRenderer: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  openText: PropTypes.string.isRequired,
  status: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  hide: PropTypes.func.isRequired,
  open: PropTypes.func.isRequired,
  reportURL: PropTypes.string
};

class ProgressModal extends Component {
  mapAdditionalInfo(additionalInfo) {
    if (!additionalInfo) {
      return '';
    }

    return additionalInfo.map((info, i) => {
      if (Array.isArray(info)) {
        return (
          <div key={i} className="group">
            {this.mapAdditionalInfo(info)}
          </div>
        );
      } else {
        return <div key={i}>{info}</div>;
      }
    });
  }

  sendBugReport = () => {
    let exceptionMessage = this.props.status
      ? this.props.status.text
      : 'undefined';
    this.props.ipcRenderer.send(
      'bug-report',
      exceptionMessage,
      null,
      this.props.status
    );
  };

  renderButtons = () => {
    if (this.props.status.isDone) {
      return (
        <div>
          <Button bsStyle="default" onClick={this.props.hide}>
            Close
          </Button>
          <Button bsStyle="primary" onClick={this.props.open}>
            {this.props.openText}
          </Button>
          {this.props.status.reportURL && (
            <Button
              bsStyle="view-report"
              onClick={() => {
                this.props.history.push(`/?url=${this.props.status.reportURL}`);
              }}
            >
              View Report
            </Button>
          )}
        </div>
      );
    } else {
      return (
        <div>
          {this.props.status.isError && (
            <Button
              bsStyle="default"
              onClick={this.sendBugReport}
              disabled={!this.props.status.isError}
            >
              Send Bug Report
            </Button>
          )}
          <Button
            bsStyle="primary"
            onClick={this.props.hide}
            disabled={!this.props.status.isError}
          >
            OK
          </Button>
        </div>
      );
    }
  };

  renderException = exception => {
    const localizedMessages = [];
    let cause = exception.cause;
    while (cause) {
      if (cause.localizedMessage) {
        localizedMessages.push(cause.localizedMessage);
      }
      cause = cause.cause;
    }

    return (
      <pre className="pre-scrollable">
        {(localizedMessages.length > 0 
          && <ul>{localizedMessages.map((message, index) => <li key={index}>{message}</li>)}</ul>)
          || JSON.stringify(exception)
        }
      </pre>
    )
  };


  render() {
    const { status, show, title } = this.props;
    const progressStyle =
      (status.isDone && 'success') || (status.isError && 'danger') || null;
    return (
      <Modal show={show}>
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>

        <Modal.Body style={{ height: status.isDone || status.isError ? 'auto' : '172px' }}>
          <ProgressBar
            striped
            active={!status.isDone}
            bsStyle={progressStyle}
            now={status.progress}
          />
          <p>{status.text}</p>
          {this.mapAdditionalInfo(status.additionalInfo)}
          {this.props.status.isError 
            && this.props.status.exception 
            && this.renderException(this.props.status.exception)
          }
        </Modal.Body>

        <Modal.Footer>{this.renderButtons()}</Modal.Footer>
      </Modal>
    );
  }
}

ProgressModal.propTypes = propTypes;

export default withRouter(ProgressModal);
