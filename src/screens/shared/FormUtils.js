/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Col,
  ControlLabel,
  FormControl,
  FormGroup,
  InputGroup,
  Table
} from 'react-bootstrap';
import Dropzone from 'react-dropzone';
import { isEqual } from 'lodash';

const DropZonePropTypes = {
  accept: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  paths: PropTypes.array.isRequired
};

const SortOrder = {
  ASC: 'ASC',
  DESC: 'DESC'
};

export class DropZone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paths: [],
      sortOrder: SortOrder.ASC
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state !== prevState) {
      this.props.onChange(this.state.paths);
    } else if (!isEqual(this.props.paths, this.state.paths)) {
      this.setState({ paths: this.props.paths });
    }
  }

  sort = (paths, sortOrder = SortOrder.ASC) => {
    const sortedPaths = paths
      .slice()
      .sort((a, b) => (a.toLowerCase() > b.toLowerCase() ? 1 : -1));
    return sortOrder === SortOrder.ASC ? sortedPaths : sortedPaths.reverse();
  };

  onAdd = event => {
    const files = Array.prototype.slice.call(event.target.files);
    event.target.value = null;
    const newPaths = files
      .map(({ path }) => (!this.state.paths.includes(path) ? path : undefined))
      .filter(path => path !== undefined);
    const paths = this.sort([...this.state.paths, ...newPaths]);

    this.setState({ paths });
  };

  onDrop = files => {
    const paths = this.sort(files.map(({ path }) => path));

    this.setState({ paths });
  };

  onRemove = filePath => {
    const paths = this.sort(this.state.paths.filter(path => path !== filePath));

    this.setState({ paths });
  };

  onRemoveAll = () => {
    const paths = [];
    this.setState({ paths });
  };

  reverseSortOrder = () => {
    const { ASC, DESC } = SortOrder;
    this.setState({
      sortOrder: this.state.sortOrder === ASC ? DESC : ASC
    });
  };

  render() {
    if (this.state.paths.length) {
      return (
        <div>
          <Table bordered condensed>
            <thead>
              <tr>
                <th>
                  File
                  <i
                    className={
                      this.state.sortOrder === SortOrder.ASC
                        ? 'fa fa-caret-up'
                        : 'fa fa-caret-down'
                    }
                    onClick={this.reverseSortOrder}
                  />
                </th>
                <th>Remove</th>
              </tr>
            </thead>
            <tbody>
              {this.sort(this.state.paths, this.state.sortOrder).map(
                (path, index) => (
                  <tr key={index}>
                    <td>{path}</td>
                    <td>
                      <i
                        className="fa fa-trash"
                        onClick={this.onRemove.bind(null, path)}
                      />
                    </td>
                  </tr>
                )
              )}
            </tbody>
            <tfoot>
              <tr>
                <td>
                  <strong>{this.state.paths.length} files</strong>
                  <label>
                    <a>Add more files</a>
                    <input
                      type="file"
                      accept={this.props.accept}
                      multiple
                      style={{ display: 'none' }}
                      onChange={this.onAdd}
                    />
                  </label>
                </td>
                <td>
                  <a onClick={this.onRemoveAll}>Remove all</a>
                </td>
              </tr>
            </tfoot>
          </Table>
        </div>
      );
    } else {
      return (
        <Dropzone
          className="drop-zone"
          accept={this.props.accept}
          onDrop={this.onDrop}
        >
          Drop your files here to select them <br />
          or
          <br />
          <Button className="btn btn-primary">Browse...</Button>
        </Dropzone>
      );
    }
  }
}

DropZone.propTypes = DropZonePropTypes;

const FileSelectorPropTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  path: PropTypes.string.isRequired,
  accept: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  electronAPI: PropTypes.object,
  directory: PropTypes.bool,
  clearable: PropTypes.bool,
  onPathClear: PropTypes.func
};

export class FileSelector extends Component {
  constructor(props) {
    super(props);
    this.onDirectoryChange = this.onDirectoryChange.bind(this);

    this.state = {
      isDialogOpen: false
    };
  }

  addDirectory = event => {
    if (this.props.directory) {
      event.preventDefault();

      if (!this.state.isDialogOpen) {
        this.props.electronAPI.dialog.showOpenDialog(
          {
            properties: ['openDirectory', 'createDirectory']
          },
          this.onDirectoryChange
        );
        this.setState({ isDialogOpen: true });
      }
    }
  };

  onDirectoryChange(directoryPaths) {
    this.props.onChange(directoryPaths ? directoryPaths[0] : '');
    this.setState({ isDialogOpen: false });
  }

  onFileChange = event => {
    const { files } = event.target;
    const actualPath = files.length ? files[0].path : '';
    const path = actualPath.toLowerCase();
    for (const extension of this.props.accept.toLowerCase().replace(/\s/g, '').split(',')) {
      if (path.endsWith(extension)) {
        this.props.onChange(actualPath);
        return;
      }
    }
  };

  renderLabel = () =>
    this.props.label && (
      <Col componentClass={ControlLabel} sm={2}>
        {this.props.label}
      </Col>
    );

  render() {
    return (
      <FormGroup>
        {this.renderLabel()}
        <Col sm={this.props.label ? 10 : 12}>
          <InputGroup>
            <FormControl
              type="text"
              value={this.props.path}
              placeholder={this.props.placeholder}
              readOnly
            />
            <InputGroup.Button>
              <label className="btn btn-primary">
                Browse...
                <FormControl
                  type="file"
                  accept={this.props.accept}
                  style={{ display: 'none' }}
                  onClick={this.addDirectory}
                  onChange={this.onFileChange}
                  value=""
                />
              </label>
            </InputGroup.Button>
            {this.props.clearable && this.props.path && (
              <InputGroup.Button>
                <Button
                  onClick={this.props.onPathClear}
                  title="Click here to clear your Define.xml"
                >
                  <i className="fa fa-trash"/>
                </Button>
              </InputGroup.Button>
            )}
          </InputGroup>
        </Col>
      </FormGroup>
    );
  }
}

FileSelector.propTypes = FileSelectorPropTypes;
FileSelector.defaultProps = {
  clearable: false
};
