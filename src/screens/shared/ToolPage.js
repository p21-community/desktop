/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProgressModal from './ProgressModal';

const propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  widgetTitle: PropTypes.string.isRequired,
  widgetIcon: PropTypes.string.isRequired,
  modalTitle: PropTypes.string.isRequired,
  openText: PropTypes.string.isRequired,
  ipcRenderer: PropTypes.object.isRequired
};

class ToolPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      modalStatus: {}
    };
  }

  componentDidMount() {
    this.props.ipcRenderer.on('status', this.updateStatusState);
  }

  updateStatusState = (event, modalStatus) => {
    this.setState({ modalStatus });
  };

  componentWillUnmount() {
    this.props.ipcRenderer.removeAllListeners('status');
  }

  showModal = () => {
    this.setState({
      showModal: true,
      modalStatus: {
        text: 'Starting'
      }
    });
  };

  hideModal = () => {
    this.setState({ showModal: false });
  };

  openFile = () => {
    this.props.ipcRenderer.send('open', this.state.modalStatus.target);
  };

  render() {
    const {
      title,
      description,
      widgetTitle,
      widgetIcon,
      children
    } = this.props;

    return (
      <div>
        <ProgressModal
          ipcRenderer={this.props.ipcRenderer}
          title={this.props.modalTitle}
          openText={this.props.openText}
          status={this.state.modalStatus}
          show={this.state.showModal}
          hide={this.hideModal}
          open={this.openFile}
        />

        <div className="main-header">
          <h2>{title}</h2>
          <em>{description}</em>
        </div>

        <div className="main-content">
          <div className="widget">
            <div className="widget-header">
              <h3>
                <i className={widgetIcon} />
                {widgetTitle}
              </h3>
            </div>
            <div className="widget-content">
              {React.Children.map(children, child =>
                React.cloneElement(child, {
                  showModal: this.showModal,
                  updateStatusState: this.updateStatusState
                })
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ToolPage.propTypes = propTypes;

export default ToolPage;
