/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import ToolPage from '../ToolPage';

const generateToolPage = ipcRenderer => {
  return (
    <ToolPage
      title="My Title"
      description="My Description"
      widgetTitle="My Widget Title"
      widgetIcon="My Widget Icon"
      modalTitle="My Modal Title"
      openText="My Open Text"
      ipcRenderer={ipcRenderer}
    />
  );
};

test('ToolPage renders correctly', () => {
  const ToolPageRenderer = new ReactShallowRenderer();
  ToolPageRenderer.render(
    <ToolPage
      title="My Title"
      description="My Description"
      widgetTitle="My Widget Title"
      widgetIcon="My Widget Icon"
      modalTitle="My Modal Title"
      openText="My Open Text"
      ipcRenderer={{}}
    >
      <div />
    </ToolPage>
  );
  expect(ToolPageRenderer.getRenderOutput()).toMatchSnapshot();
});

describe('ipcRenderer', () => {
  test('event listener is added when ToolPage is mounted', () => {
    const spy = sinon.spy();
    const ipcRenderer = {
      on: spy
    };
    const ToolPageComponent = mount(generateToolPage(ipcRenderer));
    expect(
      spy.calledWith('status', ToolPageComponent.instance().updateStatusState)
    ).toBe(true);
  });

  test('event listeners are removed when ToolPage is unmounted', () => {
    const spy = sinon.spy();
    const ipcRenderer = {
      on: sinon.stub(),
      removeAllListeners: spy
    };
    const ToolPageComponent = shallow(generateToolPage(ipcRenderer));
    ToolPageComponent.unmount();
    expect(spy.calledWith('status')).toBe(true);
  });
});

test('updateStatusState function updates the modalStatus state', () => {
  const ipcRenderer = {
    on: jest.fn()
  };
  const ToolPageComponent = shallow(generateToolPage(ipcRenderer));

  const modalStatus = {
    text: 'Processing',
    progress: 60
  };

  ToolPageComponent.instance().updateStatusState(null, modalStatus);

  expect(ToolPageComponent.state('modalStatus')).toEqual(modalStatus);
});

test('showModal function updates the state correctly', () => {
  const ipcRenderer = {
    on: jest.fn()
  };
  const ToolPageComponent = shallow(generateToolPage(ipcRenderer));

  ToolPageComponent.instance().showModal();

  expect(ToolPageComponent.state('showModal')).toBe(true);
  expect(ToolPageComponent.state('modalStatus')).toEqual({
    text: 'Starting'
  });
});

test('hideModal function updates the state correctly', () => {
  const ipcRenderer = {
    on: jest.fn()
  };
  const ToolPageComponent = shallow(generateToolPage(ipcRenderer));

  ToolPageComponent.instance().showModal();

  expect(ToolPageComponent.state('showModal')).toBe(true);

  ToolPageComponent.instance().hideModal();

  expect(ToolPageComponent.state('showModal')).toBe(false);
});

test('openFile function updates the state correctly', () => {
  const spy = sinon.spy();
  const ipcRenderer = {
    on: jest.fn(),
    send: spy
  };
  const ToolPageComponent = shallow(generateToolPage(ipcRenderer));

  ToolPageComponent.setState({
    modalStatus: {
      target: 'target'
    }
  });

  ToolPageComponent.instance().openFile();

  expect(spy.calledWith('open')).toBe(true);
});
