/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import { DropZone, FileSelector } from '../FormUtils';

test('DropZone renders correctly with no files selected', () => {
  const DropZoneRenderer = new ReactShallowRenderer();

  DropZoneRenderer.render(<DropZone accept="accept" onChange={jest.fn()} />);

  expect(DropZoneRenderer.getRenderOutput()).toMatchSnapshot();
});

test('DropZone file list renders correctly when sortOrder is ASC', () => {
  const DropZoneComponent = shallow(
    <DropZone accept="accept" onChange={jest.fn()} />
  );

  const paths = ['First', 'Second'];

  DropZoneComponent.setState({
    paths
  });

  expect(DropZoneComponent.html()).toMatchSnapshot();
});

test('DropZone file list renders correctly when sortOrder is DESC', () => {
  const DropZoneComponent = shallow(
    <DropZone accept="accept" onChange={jest.fn()} />
  );

  const paths = ['First', 'Second'];

  DropZoneComponent.setState({
    paths
  });
  DropZoneComponent.instance().reverseSortOrder();

  expect(DropZoneComponent.html()).toMatchSnapshot();
});

test('reverseSortOrder updates the state correctly', () => {
  const DropZoneComponent = shallow(
    <DropZone accept="accept" onChange={jest.fn()} />
  );

  expect(DropZoneComponent.state('sortOrder')).toEqual('ASC');

  DropZoneComponent.instance().reverseSortOrder();

  expect(DropZoneComponent.state('sortOrder')).toEqual('DESC');

  DropZoneComponent.instance().reverseSortOrder();

  expect(DropZoneComponent.state('sortOrder')).toEqual('ASC');
});

test('onAdd merges existing paths with new paths correctly', () => {
  const ipcRenderer = {
    sendSync: arg => []
  };

  const DropZoneComponent = shallow(
    <DropZone accept="accept" onChange={jest.fn()} />
  );

  const state = {
    paths: ['My First path', 'My Third path']
  };
  const otherFile = {
    path: 'My Second path'
  };

  DropZoneComponent.setState(state);

  expect(DropZoneComponent.state('paths')).toEqual(state.paths);

  DropZoneComponent.instance().onAdd({
    target: {
      files: [
        otherFile,
        {
          path: state.paths[0]
        },
        {
          path: state.paths[1]
        }
      ]
    }
  });

  expect(DropZoneComponent.state('paths')).toEqual([
    state.paths[0],
    otherFile.path,
    state.paths[1]
  ]);
});

test('onDrop updates the state correctly', () => {
  const ipcRenderer = {
    sendSync: arg => []
  };

  const DropZoneComponent = shallow(
    <DropZone accept="accept" onChange={jest.fn()} />
  );

  const files = [
    {
      path: 'My File path'
    }
  ];

  expect(DropZoneComponent.state('paths')).toEqual([]);

  DropZoneComponent.instance().onDrop(files);

  expect(DropZoneComponent.state('paths')).toEqual([files[0].path]);
});

test('onRemove removes the a file path from the state correctly', () => {
  const ipcRenderer = {
    sendSync: arg => []
  };

  const DropZoneComponent = shallow(
    <DropZone accept="accept" onChange={jest.fn()} />
  );

  const pathToRemove = 'My File path';

  const state = {
    paths: [pathToRemove, 'Other File path']
  };

  DropZoneComponent.setState(state);

  expect(DropZoneComponent.state('paths')).toEqual(state.paths);

  DropZoneComponent.instance().onRemove(pathToRemove);

  expect(DropZoneComponent.state('paths')).toEqual([state.paths[1]]);
});

test('onRemoveAll sets the paths property on the state to []', () => {
  const ipcRenderer = {
    sendSync: arg => []
  };

  const DropZoneComponent = shallow(
    <DropZone accept="accept" onChange={jest.fn()} />
  );

  const state = {
    paths: ['My File path']
  };

  DropZoneComponent.setState(state);

  expect(DropZoneComponent.state('paths')).toEqual(state.paths);

  DropZoneComponent.instance().onRemoveAll();

  expect(DropZoneComponent.state('paths')).toEqual([]);
});

describe('FileSelector', () => {
  test('renders correctly', () => {
    const FileSelectorRenderer = new ReactShallowRenderer();

    FileSelectorRenderer.render(
      <FileSelector
        label="label"
        path="path"
        accept="accept"
        onChange={jest.fn()}
        electronAPI={{}}
      />
    );

    expect(FileSelectorRenderer.getRenderOutput()).toMatchSnapshot();
  });

  test('showOpenDialog is called on click', () => {
    const spy = sinon.spy();

    const FileSelectorComponent = mount(
      <FileSelector
        label="label"
        path="path"
        accept="accept"
        directory={true}
        onChange={jest.fn()}
        electronAPI={{
          dialog: {
            showOpenDialog: spy
          }
        }}
      />
    );

    FileSelectorComponent.find('input[type="file"]').simulate('click');

    expect(spy.calledOnce).toBe(true);
  });

  test('showOpenDialog is not called on click if not a directory', () => {
    const spy = sinon.spy();

    const FileSelectorComponent = mount(
      <FileSelector
        label="label"
        path="path"
        accept="accept"
        onChange={jest.fn()}
        electronAPI={{
          dialog: {
            showOpenDialog: spy
          }
        }}
      />
    );

    FileSelectorComponent.find('input[type="file"]').simulate('click');

    expect(spy.calledOnce).toBe(false);
  });

  test('onDirectoryChange calls the onChange prop', () => {
    const spy = sinon.spy();

    const FileSelectorComponent = mount(
      <FileSelector
        label="label"
        path="path"
        accept="accept"
        directory={true}
        onChange={spy}
        electronAPI={{}}
      />
    );

    FileSelectorComponent.instance().onDirectoryChange(['My Directory']);

    expect(spy.calledOnce).toBe(true);

    FileSelectorComponent.instance().onDirectoryChange(null);

    expect(spy.calledOnce).toBe(true);
  });
});
