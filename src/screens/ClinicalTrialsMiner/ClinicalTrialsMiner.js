/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Select from 'react-select';
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormGroup,
  FormControl,
  OverlayTrigger,
  Popover
} from 'react-bootstrap';

import ToolPage from '../shared/ToolPage';
import { FileSelector } from '../shared/FormUtils';
import { loadKey, loadPreferences } from '../../utils/localStorage';
import { REPORT_TYPE, ACTION } from './clinicalTrialsMinerRedux';

const sampleMinerUrl = `https://clinicaltrials.gov/ct2/results?term="rheumatoid+arthritis"`;
const searchUrlTooltip = `Paste in any search URL from clinicaltrials.gov. Don't worry if the URL is lengthy. Try running the pre-filled query for Rheumatoid Arthritis!`;
const categoriesFileTooltip = `A two column CSV file only for use in Outcomes. In the example for Rheumatoid Arthritis, if the outcome has text in SEARCH_TERM, the outcome will be categorized as your custom DISPLAY_CATEGORY`;

const ClinicalTrialsMinerPropTypes = {
  electronAPI: PropTypes.object.isRequired,
  reportType: PropTypes.string.isRequired
};

export class ClinicalTrialsMiner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoriesFilePath: '',
      searchUrl: '',
      error: undefined
    };
  }

  mine = e => {
    e.preventDefault();
    let searchUrls;
    if (!this.state.searchUrl.length) {
      searchUrls = [sampleMinerUrl];
    } else {
      searchUrls = this.processSearchUrls(this.state.searchUrl);
    }
    if (!searchUrls.length) {
      this.setErrorMessage(
        'The Search URL input is invalid. Please review your input and try again.'
      );
      return;
    } else {
      if (this.state.error) {
        this.setErrorMessage();
      }
    }
    this.props.showModal();

    const reportType = this.props.reportType;
    const { ipcRenderer } = this.props.electronAPI;
    const categoriesFilePath =
      reportType === REPORT_TYPE.SITES
        ? undefined
        : this.state.categoriesFilePath || undefined;
    const parameters = {
      preferences: {
        ...loadPreferences(),
        userAgent: window.navigator.userAgent,
        anonymousId: loadKey('app-id')
      },
      reportType,
      searchUrls,
      categoriesFilePath
    };

    ipcRenderer.removeAllListeners('getToolParameters');
    ipcRenderer.once('getToolParameters', () => {
      const toolParameters = {
        preferences: parameters.preferences,
        reportType: parameters.reportType,
        searchUrls: parameters.searchUrls[0],
        categoriesFilePath: parameters.categoriesFilePath
      };
      ipcRenderer.send('toolParameters', 'miner', toolParameters);
    });

    ipcRenderer.send('mine', parameters);
  };

  onCategoriesFilePathChange = categoriesFilePath => {
    this.setState({ categoriesFilePath });
  };

  onReportTypeChange = ({ value }) => {
    this.props.dispatch(ACTION.setReportType(value));
  };

  onSearchUrlChange = e => {
    this.setState({ searchUrl: e.target.value });
  };

  processSearchUrls = searchUrlsText => {
    // Taken from: https://gist.github.com/dperini/729294
    const regex = RegExp(
      // protocol identifier
      '(?:(?:https?|ftp)://)' +
        // user:pass authentication
        '(?:\\S+(?::\\S*)?@)?' +
        '(?:' +
        // IP address exclusion
        // private & local networks
        '(?!(?:10|127)(?:\\.\\d{1,3}){3})' +
        '(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})' +
        '(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})' +
        // IP address dotted notation octets
        // excludes loopback network 0.0.0.0
        // excludes reserved space >= 224.0.0.0
        // excludes network & broacast addresses
        // (first & last IP address of each class)
        '(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])' +
        '(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}' +
        '(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))' +
        '|' +
        // host name
        '(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)' +
        // domain name
        '(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*' +
        // TLD identifier
        '(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))' +
        // TLD may end with dot
        '\\.?' +
        ')' +
        // port number
        '(?::\\d{2,5})?' +
        // resource path
        '(?:[/?#]\\S*)?',
      'gi'
    );
    let match;
    const arr = [];
    let separators = [',', '|'];
    while ((match = regex.exec(searchUrlsText)) !== null) {
      if (match.index === regex.lastIndex) {
        regex.lastIndex++;
      }
      let url = match[0];
      for (let separator of separators) {
        if (url.endsWith(separator)) {
          url = url.substring(0, url.length - 1);
        }
      }
      arr.push(url.trim());
    }
    return arr;
  };

  setErrorMessage = error => {
    this.setState({ error });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.mine}>
        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            Report Type
          </Col>
          <Col sm={3}>
            <Select
              options={Object.values(REPORT_TYPE).map(reportType => ({
                value: reportType,
                label: reportType
              }))}
              value={this.props.reportType}
              onChange={this.onReportTypeChange}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            Search URL <Tooltip message={searchUrlTooltip} />
          </Col>
          <Col sm={10}>
            <FormControl
              componentClass="textarea"
              placeholder={sampleMinerUrl}
              rows={5}
              cols={30}
              value={this.state.searchUrl}
              onChange={this.onSearchUrlChange}
            />
          </Col>
        </FormGroup>
        {this.props.reportType === REPORT_TYPE.OUTCOMES && (
          <FileSelector
            label={
              <span>
                Categories File <Tooltip message={categoriesFileTooltip} />
              </span>
            }
            accept=".csv"
            placeholder="Optional. An example is located at: /components/categories/RA-categories.csv"
            path={this.state.categoriesFilePath}
            onChange={this.onCategoriesFilePathChange}
            electronAPI={this.props.electronAPI}
          />
        )}
        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button type="submit" bsStyle="primary" disabled={false}>
              Mine
            </Button>
            <span className="form-error miner-error">
              {this.state.error ? this.state.error : ''}
            </span>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

ClinicalTrialsMiner.propTypes = ClinicalTrialsMinerPropTypes;

const mapStateToProps = ({ clinicalTrialsMiner }) => ({
  reportType: clinicalTrialsMiner.reportType
});

const ConnectedClinicalTrialsMiner = connect(mapStateToProps)(
  ClinicalTrialsMiner
);

export default function Renderer(props) {
  return (
    <ToolPage
      ipcRenderer={props.electronAPI.ipcRenderer}
      title="ClinicalTrials.gov Miner"
      description={
        'leverage the search capabilities of clinicaltrials.gov: https://clinicaltrials.gov/ct2/results/refine'
      }
      widgetTitle="Mine ClinicalTrials.gov"
      widgetIcon="fa fa-flask"
      modalTitle="Mining data"
      openText="Open Report"
    >
      <ConnectedClinicalTrialsMiner electronAPI={props.electronAPI} />
    </ToolPage>
  );
}

const Tooltip = ({ message }) => {
  const popover = <Popover id="popover-trigger-hover-focus">{message}</Popover>;
  return (
    <OverlayTrigger
      trigger={['hover', 'focus']}
      placement="top"
      overlay={popover}
    >
      <i className="fa fa-question-circle" />
    </OverlayTrigger>
  );
};
