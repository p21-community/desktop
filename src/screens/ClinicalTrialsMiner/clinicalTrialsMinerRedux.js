/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

// CONSTANTS
export const REPORT_TYPE = {
  OUTCOMES: 'Outcomes',
  SITES: 'Sites'
};
const SET_REPORT_TYPE = 'SET_REPORT_TYPE';

// ACTIONS
export const ACTION = {
  setReportType: reportType => ({
    type: SET_REPORT_TYPE,
    reportType
  })
};

// INITIAL STATE
const initialState = {
  reportType: REPORT_TYPE.OUTCOMES
};

const clinicalTrialsMinerRedux = (
  state = initialState,
  { type, reportType }
) => {
  switch (type) {
    case SET_REPORT_TYPE:
      return {
        reportType
      };
    default:
      return state;
  }
};

export default clinicalTrialsMinerRedux;
