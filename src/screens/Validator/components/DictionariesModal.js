/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormGroup,
  Modal
} from 'react-bootstrap';

const propTypes = {
  dictionaries: PropTypes.array.isRequired,
  disabled: PropTypes.bool.isRequired,
  show: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  setMedDRAModalState: PropTypes.func.isRequired,
  setSNOMEDModalState: PropTypes.func.isRequired
};

const DictionariesModal = props => {
  const renderSelects = (id, label, versions, onChange) => {
    return versions.length ? (
      <Select
        options={versions}
        value={id}
        onChange={onChange}
        disabled={props.disabled}
        clearable={false}
        backspaceRemoves={false}
      />
    ) : (
      <a
        className="control-inline cursor-pointer"
        onClick={() =>
          label === 'MedDRA'
            ? props.setMedDRAModalState(true)
            : props.setSNOMEDModalState(true)
        }
      >
        Install now
      </a>
    );
  };

  const renderAddDictionaryIcon = onClick => (
    <Col className="add-dictionary-icon" sm={1}>
      <i className="fa fa-lg fa-plus-circle" onClick={() => onClick(true)} />
    </Col>
  );

  const dictionaries = props.dictionaries.map(
    ({ id, label, versions, onChange }, i) => {
      let addDictionaryIcon;
      if (label === 'MedDRA' && versions.length) {
        addDictionaryIcon = renderAddDictionaryIcon(props.setMedDRAModalState);
      } else if (label === 'SNOMED' && versions.length) {
        addDictionaryIcon = renderAddDictionaryIcon(props.setSNOMEDModalState);
      }
      return (
        <FormGroup key={i}>
          <Col sm={3} componentClass={ControlLabel}>
            {label}
          </Col>
          <Col sm={3}>{renderSelects(id, label, versions, onChange)}</Col>
          {addDictionaryIcon}
        </FormGroup>
      );
    }
  );

  return (
    <Modal show={props.show}>
      <Modal.Header>
        <Modal.Title>Select Dictionaries</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form horizontal>{dictionaries}</Form>
      </Modal.Body>

      <Modal.Footer>
        <Button
          className="btn btn-primary"
          onClick={props.onClick.bind(null, false)}
        >
          OK
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

DictionariesModal.propTypes = propTypes;

export default DictionariesModal;
