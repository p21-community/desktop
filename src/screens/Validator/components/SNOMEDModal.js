/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ProgressBar } from 'react-bootstrap';
import { FileSelector } from '../../shared/FormUtils';
import Cleave from 'cleave.js/react';
import moment from 'moment';

const propTypes = {
  show: PropTypes.bool.isRequired,
  setModalState: PropTypes.func.isRequired
};

const versionFormat = 'YYYY-MM-DD';

export default class SNOMEDModal extends Component {
  constructor(props) {
    super(props);

    this.defaultState = this.state = {
      version: '',
      path: '',
      showProgressBar: false,
      progressBarStyle: null,
      progressStatusText: '',
      showOverwriteModal: false,
      importInProgress: false,
      importComplete: false
    };
  }

  componentDidMount() {
    window.ipcRenderer.on('foundDuplicateSNOMED', (event, version) => {
      this.setState({
        progressBarStyle: 'danger',
        progressStatusText: `Found duplicate SNOMED version: ${version}`,
        showOverwriteModal: true,
        importInProgress: false
      });
    });
    window.ipcRenderer.on('permissionErrorSNOMED', (event, version) => {
      this.setState({
        progressBarStyle: 'danger',
        progressStatusText: `Could not create the SNOMED folder: ${version}`,
        showOverwriteModal: false,
        importInProgress: false
      });
    });
    window.ipcRenderer.on('importedSNOMEDSuccessfully', (event, version) => {
      this.setState({
        progressBarStyle: 'success',
        progressStatusText: `Successfully installed SNOMED version: ${version}`,
        importInProgress: false,
        importComplete: true
      });
    });
  }

  componentWillUnmount() {
    window.ipcRenderer.removeAllListeners('foundDuplicateSNOMED');
    window.ipcRenderer.removeAllListeners('importedSNOMEDSuccessfully');
  }

  closeModal = () => {
    this.props.setModalState(false);
    this.setState(this.defaultState);
  };

  closeOverwriteModal = () => this.setState({ showOverwriteModal: false });

  importSNOMED = overwrite => {
    this.setState({
      showProgressBar: true,
      progressBarStyle: null,
      progressStatusText: `Installing SNOMED version: ${this.state.version}`,
      showOverwriteModal: false,
      importInProgress: true
    });
    window.ipcRenderer.send(
      'importSNOMED',
      this.state.version,
      this.state.path,
      overwrite
    );
  };

  onCleaveInit = cleave => {
    this.cleave = cleave;
  };

  onPathChange = path => {
    this.setState({ importComplete: false });
    if (!this.state.version) {
      const version = path
        .replace(/^.*[\\/]/, '') // Replace paths to get file name
        .match(/[12][0-9]{3}(0[1-9]|1([012]))(0[1-9]|1[0-9]|2[0-9]|3[0|1])/); // Date regex
      if (version) {
        const formattedVersion = moment(version[0]).format(versionFormat);
        if (moment(formattedVersion, versionFormat, true).isValid()) {
          this.cleave.setRawValue(formattedVersion);
          this.setState({
            path,
            version: formattedVersion,
            showProgressBar: false,
            progressBarStyle: null
          });
        }
        return;
      }
    }
    this.setState({
      path,
      showProgressBar: false,
      progressBarStyle: null
    });
  };

  onVersionChange = event => {
    this.setState({
      version: event.target.value,
      importComplete: false
    });
  };

  renderProgressBar = () => {
    return (
      this.state.showProgressBar && (
        <div>
          <b>{this.state.progressStatusText}</b>
          <ProgressBar
            bsStyle={this.state.progressBarStyle}
            active={this.state.progressBarStyle !== 'success'}
            now={100}
          />
        </div>
      )
    );
  };

  render() {
    return (
      <div>
        <Modal show={this.props.show}>
          <Modal.Header>
            <Modal.Title>Install SNOMED Dictionary</Modal.Title>
          </Modal.Header>

          <Modal.Body className="dictionary-modal-body">
            <p>
              Give us the version and location of your SNOMED file and we will
              install it for you.
              <a
                className="dictionary-learn-more"
                href="https://www.pinnacle21.com/projects/validator/configuring-snomed"
                rel="noopener noreferrer"
                target="_blank"
              >
                Learn more
              </a>
            </p>
            <div className="dictionary-form-item">
              <label>Version</label>
              <br />
              <Cleave
                className="form-control snomed-version"
                placeholder={moment().format(versionFormat)}
                options={{
                  date: true,
                  datePattern: ['Y', 'm', 'd'],
                  delimiter: '-'
                }}
                onInit={this.onCleaveInit}
                onChange={this.onVersionChange}
              />
            </div>
            <div className="dictionary-form-item">
              <label>Location of the dictionary file</label>
              <FileSelector
                label=""
                accept=".txt"
                path={this.state.path}
                onChange={this.onPathChange}
                electronAPI={{ dialog: window.dialog }}
              />
            </div>
            {this.renderProgressBar()}
          </Modal.Body>

          <Modal.Footer>
            <Button
              className="btn btn-primary"
              disabled={
                !moment(this.state.version, versionFormat, true).isValid() ||
                !this.state.path ||
                this.state.importInProgress ||
                this.state.importComplete
              }
              onClick={() => this.importSNOMED(false)}
            >
              Install
            </Button>
            <Button
              onClick={this.closeModal}
              disabled={this.state.importInProgress}
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={this.state.showOverwriteModal}>
          <Modal.Header>
            <Modal.Title>Found existing SNOMED version</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            SNOMED version {this.state.version} already exists! Do you want to
            overwrite it?
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={() => this.importSNOMED(true)}>Yes</Button>
            <Button
              className="btn btn-primary"
              onClick={this.closeOverwriteModal}
            >
              No
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

SNOMEDModal.propTypes = propTypes;
