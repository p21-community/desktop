/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, FormControl, Modal, ProgressBar } from 'react-bootstrap';
import { FileSelector } from '../../shared/FormUtils';

const propTypes = {
  show: PropTypes.bool.isRequired,
  setModalState: PropTypes.func.isRequired
};

export default class MedDRAModal extends Component {
  constructor(props) {
    super(props);

    this.defaultState = this.state = {
      version: '',
      path: '',
      showProgressBar: false,
      progressBarStyle: null,
      progressStatusText: '',
      showOverwriteModal: false,
      importInProgress: false,
      importComplete: false
    };
    this.previousVersion = '';
  }

  componentDidMount() {
    window.ipcRenderer.on('foundDuplicateMedDRA', (event, version) => {
      this.setState({
        progressBarStyle: 'danger',
        progressStatusText: `Found duplicate MedDRA version: ${version}`,
        showOverwriteModal: true,
        importInProgress: false
      });
    });
    window.ipcRenderer.on('permissionErrorMedDRA', (event, version) => {
      this.setState({
        progressBarStyle: 'danger',
        progressStatusText: `Could not create the MedDRA folder: ${version}`,
        showOverwriteModal: false,
        importInProgress: false
      });
    });
    window.ipcRenderer.on('importedMedDRASuccessfully', (event, version) => {
      this.setState({
        progressBarStyle: 'success',
        progressStatusText: `Successfully installed MedDRA version: ${version}`,
        importInProgress: false,
        importComplete: true
      });
    });
  }

  componentWillUnmount() {
    window.ipcRenderer.removeAllListeners('foundDuplicateMedDRA');
    window.ipcRenderer.removeAllListeners('importedMedDRASuccessfully');
  }

  closeModal = () => {
    this.props.setModalState(false);
    this.setState(this.defaultState);
  };

  closeOverwriteModal = () => this.setState({ showOverwriteModal: false });

  importMedDRA = overwrite => {
    this.setState({
      showProgressBar: true,
      progressBarStyle: null,
      progressStatusText: `Installing MedDRA version: ${this.state.version}`,
      showOverwriteModal: false,
      importInProgress: true
    });
    window.ipcRenderer.send(
      'importMedDRA',
      this.state.version,
      this.state.path,
      overwrite
    );
  };

  onPathChange = path => {
    this.setState({
      path,
      showProgressBar: false,
      progressBarStyle: null,
      importComplete: false
    });
  };

  onVersionChange = event => {
    this.setState({ importComplete: false });
    const { value } = event.target;
    if (!/^[0-9.]+$/.test(value)) {
      if (!value.length) {
        this.setState({ version: '' });
      }
      return;
    }

    if (value.endsWith('.')) {
      if (value.length === 2) {
        this.setState({
          version: this.state.version.endsWith('0')
            ? ''
            : value.charAt(0) + '.' + 0
        });
      } else if (value.length === 3) {
        this.setState({ version: value.charAt(0) + '.' + value.charAt(1) });
      } else if (value.length >= 4) {
        this.setState({
          version:
            /.[0-9].$/.test(value) && value.length === 5
              ? value.substring(0, 2) + '.' + 0
              : value.charAt(0) + '.' + 0
        });
      }
      this.previousVersion = value;
      return;
    }

    let intValue = value.replace(/\./g, '');
    if (intValue.length > 4) {
      intValue =
        intValue.substring(0, 3) + intValue.charAt(intValue.length - 1);
    }

    switch (intValue.length) {
      case 1:
        this.setState({ version: intValue + '.' + 0 });
        break;
      case 3:
        this.setState({
          version: /\.[0-9]\./.test(this.previousVersion)
            ? intValue.charAt(0) + '.' + intValue.charAt(2)
            : this.previousVersion.length === 1
            ? intValue.charAt(0) + intValue.charAt(2) + '.' + 0
            : intValue.substring(0, 2) + '.' + intValue.charAt(2)
        });
        break;
      case 4:
        this.setState({
          version:
            intValue.charAt(0) + intValue.charAt(1) + '.' + intValue.charAt(3)
        });
        break;
      default:
        break;
    }
    this.previousVersion = value;
  };

  renderProgressBar = () => {
    return (
      this.state.showProgressBar && (
        <div>
          <b>{this.state.progressStatusText}</b>
          <ProgressBar
            bsStyle={this.state.progressBarStyle}
            active={this.state.progressBarStyle !== 'success'}
            now={100}
          />
        </div>
      )
    );
  };

  render() {
    return (
      <div>
        <Modal show={this.props.show}>
          <Modal.Header>
            <Modal.Title>Install MedDRA Dictionary</Modal.Title>
          </Modal.Header>

          <Modal.Body className="dictionary-modal-body">
            <p>
              Give us the version and location of your MedDRA files and we will
              install them for you.
              <a
                className="dictionary-learn-more"
                href="https://www.pinnacle21.com/projects/validator/configuring-meddra"
                rel="noopener noreferrer"
                target="_blank"
              >
                Learn more
              </a>
            </p>
            <div className="dictionary-form-item">
              <label>Version</label>
              <br />
              <FormControl
                className="meddra-version"
                placeholder="18.1"
                onChange={this.onVersionChange}
                value={this.state.version}
              />
            </div>
            <div className="dictionary-form-item">
              <label>Location of the folder</label>
              <FileSelector
                label=""
                path={this.state.path}
                directory={true}
                onChange={this.onPathChange}
                electronAPI={{ dialog: window.dialog }}
              />
            </div>
            {this.renderProgressBar()}
          </Modal.Body>

          <Modal.Footer>
            <Button
              className="btn btn-primary"
              disabled={
                !this.state.version ||
                !this.state.path ||
                this.state.importInProgress ||
                this.state.importComplete
              }
              onClick={() => this.importMedDRA(false)}
            >
              Install
            </Button>
            <Button
              onClick={this.closeModal}
              disabled={this.state.importInProgress}
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={this.state.showOverwriteModal}>
          <Modal.Header>
            <Modal.Title>Found existing MedDRA version</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            MedDRA version {this.state.version} already exists! Do you want to
            overwrite it?
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={() => this.importMedDRA(true)}>Yes</Button>
            <Button
              className="btn btn-primary"
              onClick={this.closeOverwriteModal}
            >
              No
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

MedDRAModal.propTypes = propTypes;
