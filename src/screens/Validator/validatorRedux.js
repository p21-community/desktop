/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

// CONSTANTS
export const STANDARD = {
  SDTM: 'SDTM',
  ADaM: 'ADaM',
  DEFINE_XML: 'Define-XML',
  SEND: 'SEND'
};
export const SOURCE_FORMAT = {
  SasTransport: {
    id: 'SasTransport',
    name: 'SasTransport',
    label: 'SAS® Transport (XPORT)',
    extensions: ['.xpt']
  },
  Delimited: {
    id: 'Delimited',
    name: 'Delimited',
    label: 'Delimited',
    extensions: ['.csv', '.dlm'],
    hasOptions: true
  },
  DATASET_XML: {
    id: 'DATASET_XML',
    name: 'Dataset XML',
    label: 'Dataset XML',
    extensions: ['.xml'],
    isDatasetXML: true
  }
};

const SET_ADaM_CT = 'SET_ADaM_CT';
const SET_CONFIG = 'SET_CONFIG';
const SET_DEFINE_STANDARD = 'SET_DEFINE_STANDARD';
const SET_ENGINE = 'SET_ENGINE';
const SET_STANDARD = 'SET_STANDARD';
const SET_DELIMITER = 'SET_DELIMITER';
const SET_QUALIFIER = 'SET_QUALIFIER';
const SET_SOURCE_FORMAT_NAME = 'SET_SOURCE_FORMAT_NAME';
const SET_SDTM_CT = 'SET_SDTM_CT';
const SET_SEND_CT = 'SET_SEND_CT';
const SET_MEDDRA = 'SET_MEDDRA';
const SET_SNOMED = 'SET_SNOMED';
const SET_UNII = 'SET_UNII';
const SET_NDFRT = 'SET_NDFRT';

// ACTIONS
export const ACTION = {
  setADaM_CT: ADaM_CT => ({
    type: SET_ADaM_CT,
    ADaM_CT
  }),
  setConfig: (engine, standard, config) => ({
    type: SET_CONFIG,
    engine,
    standard,
    config
  }),
  setDefineStandard: value => ({
    type: SET_DEFINE_STANDARD,
    value
  }),
  setEngine: engine => ({
    type: SET_ENGINE,
    engine
  }),
  setDelimiter: delimiter => ({
    type: SET_DELIMITER,
    delimiter
  }),
  setMedDRA: (MedDRA = null) => ({
    type: SET_MEDDRA,
    MedDRA
  }),
  setNDF_RT: NDF_RT => ({
    type: SET_NDFRT,
    NDF_RT
  }),
  setQualifier: qualifier => ({
    type: SET_QUALIFIER,
    qualifier
  }),
  setSDTM_CT: SDTM_CT => ({
    type: SET_SDTM_CT,
    SDTM_CT
  }),
  setSEND_CT: SEND_CT => ({
    type: SET_SEND_CT,
    SEND_CT
  }),
  setSNOMED: (SNOMED = null) => ({
    type: SET_SNOMED,
    SNOMED
  }),
  setSourceFormatId: sourceFormatId => ({
    type: SET_SOURCE_FORMAT_NAME,
    sourceFormatId
  }),
  setStandard: standard => ({
    type: SET_STANDARD,
    standard
  }),
  setUNII: UNII => ({
    type: SET_UNII,
    UNII
  })
};

// INITIAL STATE
const initialState = {
  ADaM_CT: null,
  config: {},
  defineStandard: null,
  delimiter: ',',
  engine: null,
  MedDRA: null,
  NDF_RT: null,
  qualifier: '"',
  SDTM_CT: null,
  SEND_CT: null,
  SNOMED: null,
  sourceFormatId: SOURCE_FORMAT.SasTransport.id,
  standard: STANDARD.SDTM,
  UNII: null
};

const validatorRedux = (state = initialState, action) => {
  switch (action.type) {
    case SET_ADaM_CT:
      return {
        ...state,
        ADaM_CT: action.ADaM_CT
      };
    case SET_CONFIG:
      const config = { ...state.config };
      config[action.engine] = { ...state.config[action.engine] };
      config[action.engine][action.standard] = action.config;

      return {
        ...state,
        config
      };
    case SET_DEFINE_STANDARD:
      return {
        ...state,
        defineStandard: action.value
      };
    case SET_DELIMITER:
      return {
        ...state,
        delimiter: action.delimiter
      };
    case SET_ENGINE:
      if (action.engine !== state.engine) {
        return {
          ...state,
          engine: action.engine,
          defineStandard: null
        };
      }
      return { ...state };
    case SET_MEDDRA:
      return {
        ...state,
        MedDRA: action.MedDRA
      };
    case SET_NDFRT:
      return {
        ...state,
        NDF_RT: action.NDF_RT
      };
    case SET_QUALIFIER:
      return {
        ...state,
        qualifier: action.qualifier
      };
    case SET_SDTM_CT:
      return {
        ...state,
        SDTM_CT: action.SDTM_CT
      };
    case SET_SEND_CT:
      return {
        ...state,
        SEND_CT: action.SEND_CT
      };
    case SET_SNOMED:
      return {
        ...state,
        SNOMED: action.SNOMED
      };
    case SET_SOURCE_FORMAT_NAME:
      return {
        ...state,
        sourceFormatId: action.sourceFormatId
      };
    case SET_STANDARD:
      if (action.standard !== state.standard) {
        return {
          ...state,
          standard: action.standard,
          defineStandard: null
        };
      }
      return { ...state };
    case SET_UNII:
      return {
        ...state,
        UNII: action.UNII
      };
    default:
      return state;
  }
};

export default validatorRedux;
