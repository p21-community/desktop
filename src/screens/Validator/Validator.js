/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Select from 'react-select';
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormControl,
  FormGroup
} from 'react-bootstrap';
import axios from 'axios';
import { Alert, AlertContainer } from 'react-bs-notifier';

import DictionariesModal from './components/DictionariesModal';
import MedDRAModal from './components/MedDRAModal';
import ToolPage from '../shared/ToolPage';
import { DropZone, FileSelector } from '../shared/FormUtils';
import { loadKey, loadPreferences } from '../../utils/localStorage';
import SNOMEDModal from './components/SNOMEDModal';
import { ACTION, SOURCE_FORMAT, STANDARD } from './validatorRedux';

const validatorPropTypes = {
  electronAPI: PropTypes.object.isRequired,
  ADaM_CT: PropTypes.string,
  config: PropTypes.object,
  defineStandard: PropTypes.string,
  delimiter: PropTypes.string.isRequired,
  engine: PropTypes.string,
  MedDRA: PropTypes.string,
  NDF_RT: PropTypes.string,
  qualifier: PropTypes.string.isRequired,
  SDTM_CT: PropTypes.string,
  SEND_CT: PropTypes.string,
  SNOMED: PropTypes.string,
  sourceFormatId: PropTypes.string.isRequired,
  standard: PropTypes.string.isRequired,
  UNII: PropTypes.string
};

export class Validator extends Component {
  constructor(props) {
    super(props);

    this.state = this.initialState = {
      dataPackageId: -1,
      definePath: '',
      paths: [],
      showDictionariesModal: false,
      showMedDRAModal: false,
      showSNOMEDModal: false
    };
  }

  componentDidMount() {
    this.synchronizeState();
  }

  componentDidUpdate() {
    let nextState = {};
    if (!this.props.environment.loggedIn && this.state.dataPackageId !== -1) {
      nextState = this.initialState;
      this.setState(nextState);
    }
    this.synchronizeState();
  }

  synchronizeState() {
    const { dispatch } = this.props;
    Object.keys(this.props.configs).forEach(engineName => {
      Object.values(STANDARD).forEach(standard => {
        const availableConfigs = this.props.configs[engineName][standard];
        if (availableConfigs.length) {
          let engineConfigs = this.props.config[engineName];
          if (!engineConfigs) {
            engineConfigs = this.props.config[engineName] = {};
          }
          const currentValue = engineConfigs[standard];
          if (
            !currentValue ||
            (currentValue && !availableConfigs.map(config => config.displayName).includes(currentValue))
          ) {
            // the configs are sorted oldest to newest with custom configs
            // appended to the end. we want the default value to be the newest 
            // _standard_ config.
            const selectedConfig = availableConfigs.filter(config => !config.custom).pop();
            if (selectedConfig) {
              dispatch(
                ACTION.setConfig(engineName, standard, selectedConfig.displayName)
              );
            }
          }
        }
      });
    });

    const engineNames = this.props.engines.map(engine => engine.name);
    if (Validator.valueNeedsUpdate(this.props.engine, engineNames)) {
      if (this.props.engines.length) {
        dispatch(ACTION.setEngine(this.props.engines[0].name));
      }
    }

    const SDTMCTs = this.props.cdiscCTVersions[STANDARD.SDTM];
    if (Validator.valueNeedsUpdate(this.props.SDTM_CT, SDTMCTs)) {
      dispatch(ACTION.setSDTM_CT(SDTMCTs[0]));
    }

    const ADaMCTs = this.props.cdiscCTVersions[STANDARD.ADaM];
    if (Validator.valueNeedsUpdate(this.props.ADaM_CT, ADaMCTs)) {
      dispatch(ACTION.setADaM_CT(ADaMCTs[0]));
    }

    const SENDCTs = this.props.cdiscCTVersions[STANDARD.SEND];
    if (Validator.valueNeedsUpdate(this.props.SEND_CT, SENDCTs)) {
      dispatch(ACTION.setSEND_CT(SENDCTs[0]));
    }

    if (
      Validator.valueNeedsUpdate(this.props.NDF_RT, this.props.NDFRTVersions)
    ) {
      dispatch(ACTION.setNDF_RT(this.props.NDFRTVersions[0]));
    }
    if (Validator.valueNeedsUpdate(this.props.UNII, this.props.UNIIVersions)) {
      dispatch(ACTION.setUNII(this.props.UNIIVersions[0]));
    }
    if (
      Validator.valueNeedsUpdate(this.props.MedDRA, this.props.medDRAVersions)
    ) {
      dispatch(ACTION.setMedDRA(this.props.medDRAVersions[0]));
    }
    if (
      Validator.valueNeedsUpdate(this.props.SNOMED, this.props.SNOMEDVersions)
    ) {
      dispatch(ACTION.setSNOMED(this.props.SNOMEDVersions[0]));
    }
  }

  static valueNeedsUpdate(value, possibleValues) {
    return (
      (!value && possibleValues.length) ||
      (value &&
        (!possibleValues.length ||
          (possibleValues.length && !possibleValues.includes(value))))
    );
  }

  isFormValid = () => {
    let isValid = false;
    const defineIsChosen = this.state.definePath !== '';

    if (this.props.standard !== STANDARD.DEFINE_XML) {
      isValid = this.state.paths.length > 0;
      if (this.props.sourceFormatId === SOURCE_FORMAT.DATASET_XML.id) {
        isValid = defineIsChosen && isValid;
      }
    } else {
      isValid = defineIsChosen && this.props.defineStandard != null;
    }

    return isValid;
  };

  mapConfigs = () => {
    const isDefine = this.props.standard === STANDARD.DEFINE_XML;
    const engine = this.props.engines.find(
      ({ name }) => name === this.props.engine
    );
    if (
      !this.props.engine ||
      !this.props.configs[this.props.engine] ||
      !this.props.configs[this.props.engine][this.props.standard]
    ) {
      return [];
    }
    return this.props.configs[this.props.engine][this.props.standard]
      .filter(config => {
        // Never display custom configs for define validations
        if (isDefine) {
          return !Validator.isCustomConfig(engine, config.fileName);
        }
        // Filter custom configs by standard
        if (Validator.isCustomConfig(engine, config.fileName)) {
          // If the custom config contains the name of any of the standards,
          // make sure it matches the selected standard.
          const standardNames = ['SDTM', 'ADAM', 'SEND'];
          const configName = config.fileName.toUpperCase();
          const matchingStandards = standardNames.filter(n =>
            configName.includes(n)
          );
          // The custom config file name did not contain the name of any standard.
          // We do not apply any filtering in that case.
          if (matchingStandards.length === 0) {
            return true;
          }
          // The custom config contained the name of one or more standards (unlikely).
          // Make sure the currently selected standard is one of the standards that matched.
          const selectedStandardName = this.props.standard.toUpperCase();
          return matchingStandards.some(n => selectedStandardName.includes(n));
        }
        return true;
      })
      .sort((config1, config2) => {
        // Keep custom configs at the bottom
        const isConfig1Custom = Validator.isCustomConfig(
          engine,
          config1.fileName
        );
        const isConfig2Custom = Validator.isCustomConfig(
          engine,
          config2.fileName
        );
        if (isConfig1Custom !== isConfig2Custom) {
          return isConfig1Custom ? 1 : -1;
        }
        // Extract any version numbers from the names - normalize to 3 digits
        const getVersion = config => {
          const versionResult = /\d+(?:\.\d+(?:\.\d+)?)?/.exec(
            config.displayName
          );
          if (versionResult == null) {
            return ['0', '0', '0'];
          }
          const version = versionResult[0];
          const versionParts = version.split('.', 3);
          const oldLength = versionParts.length;
          versionParts.length = 3;
          return versionParts.fill('0', oldLength, 3);
        };
        // Compare each version component-wise, finding the first non-matching value.
        // At this point, each version should be an array of 3 digits (strings).
        // We want the larger component to come first, so reverse the results.
        const version1 = getVersion(config1);
        const version2 = getVersion(config2);
        for (let index = 0; index !== version1.length; ++index) {
          const result = version1[index].localeCompare(version2[index]);
          if (result !== 0) {
            return -result;
          }
        }
        return 0;
      })
      .map(c => {
        return {
          value: c.displayName,
          label: c.displayName
        };
      });
  };

  mapDefineStandards = () => {
    const standardsArray = [];
    const configs = this.props.configs[this.props.engine] || {};
    const engine = this.props.engines.find(
      ({ name }) => name === this.props.engine
    );
    for (let standardName of Object.getOwnPropertyNames(configs)) {
      if (standardName !== STANDARD.DEFINE_XML) {
        const standard = configs[standardName];
        for (let config of standard) {
          if (!Validator.isCustomConfig(engine, config.fileName)) {
            standardsArray.push({
              value: config.displayName,
              label: config.displayName
            });
          }
        }
      }
    }
    return standardsArray;
  };

  mapEngineNames = () =>
    this.props.engines.map(({ name }) => ({ value: name, label: name }));

  mapCDISCCTVersions = () => {
    const standardName =
      this.props.standard === STANDARD.DEFINE_XML
        ? this.safeCTName(this.props.defineStandard)
        : this.props.standard;
    if (standardName != null) {
      const versions = this.props.cdiscCTVersions[standardName];
      if (versions != null) {
        return versions.map(this.mapVersions);
      }
    }
    return null;
  };

  mapDictionaries = () => {
    const { dispatch } = this.props;
    if (this.props.standard === STANDARD.DEFINE_XML) {
      return [];
    }

    const cdiscCTValue =
      this.props.standard === STANDARD.SDTM
        ? this.props.SDTM_CT
        : this.props.SEND_CT;

    let dictionaries = [
      {
        id: cdiscCTValue,
        label: 'CDISC CT',
        versions: this.mapCDISCCTVersions(),
        onChange: this.onCDISCCTChange
      },
      {
        id: this.props.MedDRA,
        label: 'MedDRA',
        versions: this.mapMedDRAVersions(),
        onChange: this.onMedDRAChange
      }
    ];

    if ([STANDARD.SDTM, STANDARD.SEND].includes(this.props.standard)) {
      dictionaries = [
        ...dictionaries,
        {
          id: this.props.SNOMED,
          label: 'SNOMED',
          versions: this.props.SNOMEDVersions.map(this.mapVersions),
          onChange: ({ value }) => {
            dispatch(ACTION.setSNOMED(value));
          }
        },
        {
          id: this.props.UNII,
          label: 'UNII',
          versions: this.props.UNIIVersions.map(this.mapVersions),
          onChange: ({ value }) => {
            dispatch(ACTION.setUNII(value));
          }
        },
        {
          id: this.props.NDF_RT,
          label: 'NDF-RT',
          versions: this.props.NDFRTVersions.map(this.mapVersions),
          onChange: ({ value }) => {
            dispatch(ACTION.setNDF_RT(value));
          }
        }
      ];
    }
    return dictionaries;
  };

  mapMedDRAVersions = () => {
    return this.props.medDRAVersions.map(this.mapVersions);
  };

  mapVersions = label => ({
    value: label,
    label
  });

  onCDISCCTChange = ({ value }) => {
    const { dispatch } = this.props;
    const standard =
      this.props.standard !== STANDARD.DEFINE_XML
        ? this.props.standard
        : this.safeCTName(this.props.defineStandard);

    switch (standard) {
      case STANDARD.ADaM:
        dispatch(ACTION.setADaM_CT(value));
        break;
      case STANDARD.SEND:
        dispatch(ACTION.setSEND_CT(value));
        break;
      case STANDARD.SDTM:
      default:
        dispatch(ACTION.setSDTM_CT(value));
        break;
    }
  };

  onDefineStandardChange = ({ value }) => {
    this.props.dispatch(ACTION.setDefineStandard(value));
  };

  onConfigChange = ({ value }) => {
    this.props.dispatch(
      ACTION.setConfig(this.props.engine, this.props.standard, value)
    );
  };

  onEngineChange = ({ value }) => {
    const { dispatch } = this.props;
    dispatch(ACTION.setEngine(value));
    if (!this.props.config[value][this.props.standard]) {
      dispatch(
        ACTION.setStandard(
          Object.keys(STANDARD).find(
            standard => !!this.props.config[value][standard]
          )
        )
      );
    }
    this.props.electronAPI.ipcRenderer.send('getConfigs', value);
  };

  onDefinePathChange = definePath => {
    this.setState({ definePath });
  };

  onDelimiterChange = event => {
    this.props.dispatch(ACTION.setDelimiter(event.target.value));
  };

  onFileChange = paths => {
    this.setState({ paths });
  };

  onMedDRAChange = ({ value }) => {
    this.props.dispatch(ACTION.setMedDRA(value));
  };

  onQualifierChange = event => {
    this.props.dispatch(ACTION.setQualifier(event.target.value));
  };

  onSDTMCTChange = ({ value }) => {
    this.props.dispatch(ACTION.setSDTM_CT(value));
  };

  onSourceFormatChange = ({ value }) => {
    this.props.dispatch(ACTION.setSourceFormatId(value));
    this.setState({ paths: [] });
  };

  onStandardChange = ({ value }) => {
    const nextState = { definePath: '' };

    if (value === STANDARD.DEFINE_XML) {
      nextState.paths = [];
    }

    this.props.dispatch(ACTION.setStandard(value));
    this.setState(nextState);
  };

  safeCTName = name => (name != null ? name.split('-')[0] : null);

  renderCDISCCT = () => {
    const { standard } = this.props;
    const isDefine = standard === STANDARD.DEFINE_XML;
    const { defineStandard } = this.props;

    let value;
    switch (this.safeCTName(isDefine ? defineStandard : standard)) {
      case STANDARD.ADaM:
        value = this.props.ADaM_CT;
        break;
      case STANDARD.SEND:
        value = this.props.SEND_CT;
        break;
      case STANDARD.SDTM:
      default:
        value = this.props.SDTM_CT;
        break;
    }

    return (
      ((isDefine && this.props.defineStandard != null) || !isDefine) && (
        <div>
          <Col sm={2} componentClass={ControlLabel}>
            {this.safeCTName(isDefine ? defineStandard : standard)} CT
          </Col>
          <Col sm={3}>
            <Select
              options={this.mapCDISCCTVersions()}
              value={value}
              onChange={this.onCDISCCTChange}
              disabled={this.state.dataPackageId !== -1}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </div>
      )
    );
  };

  renderConfiguration = () => {
    return (
      this.props.standard !== STANDARD.DEFINE_XML && (
        <div>
          <Col
            smOffset={
              this.props.sourceFormatId === SOURCE_FORMAT.Delimited.id &&
              this.props.standard !== STANDARD.DEFINE_XML
                ? 0
                : this.props.standard === STANDARD.DEFINE_XML
                ? 7
                : 2
            }
            sm={2}
            className="right-label"
            componentClass={ControlLabel}
          >
            Configuration
          </Col>
          <Col sm={3}>
            <Select
              options={this.mapConfigs()}
              value={this.getSelectedConfiguration()}
              onChange={this.onConfigChange}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </div>
      )
    );
  };

  getSelectedConfiguration = () => {
    const config = this.props.config[this.props.engine];
    return config ? config[this.props.standard] : null;
  };

  renderEngine = () => (
    <div>
      <Col sm={2} componentClass={ControlLabel}>
        Engine
      </Col>
      <Col sm={3}>
        <Select
          options={this.mapEngineNames()}
          value={this.props.engine}
          onChange={this.onEngineChange}
          clearable={false}
          backspaceRemoves={false}
        />
      </Col>
    </div>
  );

  renderDefineStandard = () => {
    return (
      this.props.standard === STANDARD.DEFINE_XML && (
        <div>
          <Col
            sm={2}
            smOffset={7}
            className="right-label"
            componentClass={ControlLabel}
          >
            Data Standard
          </Col>
          <Col sm={3}>
            <Select
              options={this.mapDefineStandards()}
              value={this.props.defineStandard}
              onChange={this.onDefineStandardChange}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </div>
      )
    );
  };

  renderDataPackage = () => {
    if (this.props.environment.loggedIn) {
      const getDataPackages = async () => {
        const response = await axios.get(
          '', // URL to data packages
          {
            auth: {
              username: '',
              password: ''
            },
            headers: {
              Accept: 'application/json'
            }
          }
        );

        const options = [];

        for (const dataPackage of response.data.data) {
          const { id, project, study, name } = dataPackage;

          let standardRelease = STANDARD.SEND;

          if (dataPackage.standardRelease.includes(STANDARD.SDTM)) {
            standardRelease = STANDARD.SDTM;
          } else if (dataPackage.standardRelease.includes(STANDARD.ADaM)) {
            standardRelease = STANDARD.ADaM;
          }

          options.push({
            value: id,
            label: `${project} > ${study} > ${name}`,
            dataPackage: {
              ...dataPackage,
              standardRelease
            }
          });
        }

        return { options };
      };

      const onChange = obj => {
        if (!obj) {
          this.setState(this.initialState);
          return;
        }
        const { dataPackage } = obj;

        const standard = dataPackage.standardRelease;
        const nextState = {
          cdiscCT: {
            SDTM: dataPackage[`sdtmCtRelease`],
            ADaM: dataPackage[`adamCtRelease`],
            SEND: dataPackage[`sendCtRelease`]
          },
          dataPackageId: obj.value,
          definePath: '',
          dictionary: {
            MedDRA: dataPackage.meddraRelease,
            SNOMED: dataPackage.snomedRelease,
            UNII: dataPackage.uniiRelease,
            NDFRT: dataPackage.ndfRtRelease
          }
        };

        if (standard === STANDARD.DEFINE_XML) {
          nextState.paths = [];
        }

        this.props.dispatch(ACTION.setStandard(standard));
        this.setState(nextState);
      };

      return (
        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            Data Package
          </Col>
          <Col sm={4}>
            <Select.Async
              cache={false}
              loadOptions={getDataPackages}
              value={this.state.dataPackageId}
              placeholder="Search for a Data Package"
              onChange={onChange}
            />
          </Col>
        </FormGroup>
      );
    }
  };

  onPathClear = () => {
    this.setState({ definePath: ''})
  };

  renderDefineXML = () => {
    return (
      <FileSelector
        label="Define.xml"
        accept=".xml"
        placeholder={
          this.props.sourceFormatId === SOURCE_FORMAT.DATASET_XML.id ||
          this.props.standard === STANDARD.DEFINE_XML
            ? ''
            : 'Optional'
        }
        path={this.state.definePath}
        onChange={this.onDefinePathChange}
        electronAPI={this.props.electronAPI}
        clearable
        onPathClear={this.onPathClear}
      />
    );
  };

  renderDelimiterAndQualifier = () => {
    return (
      this.props.sourceFormatId === SOURCE_FORMAT.Delimited.id &&
      this.props.standard !== STANDARD.DEFINE_XML && (
        <Col sm={2}>
          <Col componentClass={ControlLabel}>Del</Col>
          <FormControl
            bsClass="delimiter"
            value={this.props.delimiter}
            maxLength={2}
            size={3}
            onChange={this.onDelimiterChange}
          />

          <Col componentClass={ControlLabel}>Qual</Col>
          <FormControl
            bsClass="qualifier"
            value={this.props.qualifier}
            maxLength={1}
            size={3}
            onChange={this.onQualifierChange}
          />
        </Col>
      )
    );
  };

  renderDictionariesSection = () => {
    const MedDRASelect = this.props.medDRAVersions.length ? (
      <Select
        options={this.mapMedDRAVersions()}
        value={this.props.MedDRA}
        onChange={this.onMedDRAChange}
        disabled={this.state.dataPackageId !== -1}
        clearable={false}
        backspaceRemoves={false}
      />
    ) : (
      <a
        className="cursor-pointer control-inline"
        onClick={() => this.setMedDRAModalState(true)}
      >
        Install now
      </a>
    );

    const moreDictionaries = this.props.standard === STANDARD.SDTM && (
      <Col sm={3}>
        <a
          className="control-inline cursor-pointer"
          onClick={() => this.setDictionariesModalState(true)}
        >
          More dictionaries
        </a>
      </Col>
    );

    return (
      ![STANDARD.DEFINE_XML, STANDARD.SEND, STANDARD.ADaM].includes(this.props.standard) && (
        <div>
          <Col smOffset={1} sm={1} componentClass={ControlLabel}>
            MedDRA
          </Col>
          <Col sm={2}>{MedDRASelect}</Col>

          {moreDictionaries}
        </div>
      )
    );
  };

  renderSupplementalSDTMCT = () => {
    const { standard, defineStandard } = this.props;
    const sdtmCtVersionOptions = this.props.cdiscCTVersions[STANDARD.SDTM].map(
      this.mapVersions
    );

    return (
      (standard === STANDARD.ADaM ||
        (standard === STANDARD.DEFINE_XML &&
          this.safeCTName(defineStandard) === STANDARD.ADaM)) && (
        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            {STANDARD.SDTM} CT
          </Col>
          <Col sm={3}>
            <Select
              options={sdtmCtVersionOptions}
              value={this.props.SDTM_CT}
              onChange={this.onSDTMCTChange}
              disabled={this.state.dataPackageId !== -1}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </FormGroup>
      )
    );
  };

  renderSourceData = () => {
    return (
      this.props.standard !== STANDARD.DEFINE_XML && (
        <FormGroup>
          <Col sm={2} componentClass={ControlLabel}>
            Source Data
          </Col>
          <Col sm={10}>
            <DropZone
              accept={SOURCE_FORMAT[
                this.props.sourceFormatId
              ].extensions.toString()}
              onChange={this.onFileChange}
              paths={this.state.paths}
            />
          </Col>
        </FormGroup>
      )
    );
  };

  renderSourceFormat = () => {
    const sourceFormatOptions = Object.values(SOURCE_FORMAT).map(
      ({ id, label }) => ({ value: id, label })
    );

    return (
      this.props.standard !== STANDARD.DEFINE_XML && (
        <div>
          <Col sm={2} componentClass={ControlLabel}>
            Source Format
          </Col>
          <Col sm={3}>
            <Select
              options={sourceFormatOptions}
              value={this.props.sourceFormatId}
              onChange={this.onSourceFormatChange}
              clearable={false}
              backspaceRemoves={false}
            />
          </Col>
        </div>
      )
    );
  };

  renderStandard = () => {
    const standardOptions = Object.values(STANDARD)
      .map(
        standard =>
          this.props.engine &&
          this.props.configs[this.props.engine] &&
          this.props.configs[this.props.engine][standard] &&
          this.props.configs[this.props.engine][standard].length && {
            value: standard,
            label: standard
          }
      )
      .filter(obj => !!obj);

    return (
      <div>
        <Col
          smOffset={2}
          sm={2}
          className="right-label"
          componentClass={ControlLabel}
        >
          Standard
        </Col>
        <Col sm={3}>
          <Select
            options={standardOptions}
            value={this.props.standard}
            onChange={this.onStandardChange}
            disabled={this.state.dataPackageId !== -1}
            clearable={false}
            backspaceRemoves={false}
          />
        </Col>
      </div>
    );
  };

  renderValidateButton = () => (
    <FormGroup>
      <Col smOffset={2} sm={10}>
        <Button type="submit" bsStyle="primary" disabled={!this.isFormValid()}>
          Validate
        </Button>
      </Col>
    </FormGroup>
  );

  setDictionariesModalState = show => {
    this.setState({ showDictionariesModal: show });
  };

  setMedDRAModalState = show => this.setState({ showMedDRAModal: show });

  setSNOMEDModalState = show => this.setState({ showSNOMEDModal: show });

  getFileNameForConfig = value => {
    const { engine } = this.props;
    return this.props.configs[engine][value].find(
      ({ displayName }) => displayName === this.props.config[engine][value]
    ).fileName;
  };

  getFileNameForDefineStandard = value => {
    const { engine } = this.props;
    return this.props.configs[engine][this.safeCTName(value)].find(
      ({ displayName }) => displayName === value
    ).fileName;
  };

  validate = event => {
    event.preventDefault();
    this.props.showModal();

    const { engine, standard } = this.props;

    const selectedEngine = this.props.engines.find(
      ({ name }) => name === engine
    );
    const config = this.getFileNameForConfig(standard);
    const parameters = {
      cdiscCT: {},
      config,
      define: this.state.definePath,
      delimiter: this.props.delimiter,
      dictionary: {
        MedDRA: this.props.MedDRA,
        SNOMED: this.props.SNOMED,
        UNII: this.props.UNII,
        NDFRT: this.props.NDF_RT
      },
      engineVersion: selectedEngine.version,
      isCustom: Validator.isCustomConfig(selectedEngine, config),
      isFromDataPackage: this.state.dataPackageId !== -1,
      qualifier: this.props.qualifier,
      paths: this.state.paths,
      preferences: {
        ...loadPreferences(),
        userAgent: window.navigator.userAgent,
        anonymousId: loadKey('app-id')
      },
      sourceFormatName: SOURCE_FORMAT[this.props.sourceFormatId].name,
      standard
    };

    const cdiscCTStandard =
      standard === STANDARD.DEFINE_XML
        ? this.safeCTName(this.props.defineStandard)
        : standard;

    switch (cdiscCTStandard) {
      case STANDARD.ADaM:
        parameters.cdiscCT[`CDISC ${STANDARD.ADaM} CT`] = this.props.ADaM_CT;
        break;
      case STANDARD.SEND:
        parameters.cdiscCT[`CDISC ${STANDARD.SEND} CT`] = this.props.SEND_CT;
        break;
      case STANDARD.SDTM:
      default:
        parameters.cdiscCT[`CDISC ${STANDARD.SDTM} CT`] = this.props.SDTM_CT;
        break;
    }

    if (this.props.defineStandard != null) {
      parameters.defineStandard = this.getFileNameForDefineStandard(
        this.props.defineStandard
      );
    }

    if (
      standard === STANDARD.ADaM ||
      (standard === STANDARD.DEFINE_XML &&
        this.safeCTName(this.props.defineStandard) === STANDARD.ADaM)
    ) {
      parameters.cdiscCT[`CDISC ${STANDARD.SDTM} CT`] = this.props.SDTM_CT;
    }

    const { ipcRenderer } = this.props.electronAPI;

    ipcRenderer.removeAllListeners('getToolParameters');
    ipcRenderer.once('getToolParameters', () => {
      const toolParameters = {
        Standard: standard,
        'Source Format': parameters.sourceFormatName,
        Configuration: parameters.config,
        Engine: parameters.engineVersion,
        'Source Data': parameters.paths,
        'Define.xml': parameters.define,
        'CDISC CT': parameters.cdiscCT,
        Dictionaries: parameters.dictionary
      };

      if (parameters.sourceFormatName === SOURCE_FORMAT.Delimited.name) {
        toolParameters.qualifier = parameters.qualifier;
        toolParameters.delimeter = parameters.delimiter;
      }
      ipcRenderer.send('toolParameters', 'Validator', toolParameters);
    });
    ipcRenderer.send('validate', parameters);
  };

  static isCustomConfig(engine, config) {
    // If a custom config has the same name as a standard config, do not treat it as custom
    if (!engine || engine.configs.includes(config)) {
      return false;
    }
    return engine.customConfigs.includes(config);
  }

  render() {
    if (!this.props.engine) {
      return false;
    }
    return (
      <div>
        {this.props.showAlert && (
          <AlertContainer>
            <Alert type="info" onDismiss={this.props.hideAlert}>
              Configs were updated
            </Alert>
          </AlertContainer>
        )}
        <DictionariesModal
          show={this.state.showDictionariesModal}
          onClick={this.setDictionariesModalState}
          setMedDRAModalState={this.setMedDRAModalState}
          setSNOMEDModalState={this.setSNOMEDModalState}
          dictionaries={this.mapDictionaries()}
          disabled={this.state.dataPackageId !== -1}
        />
        <MedDRAModal
          show={this.state.showMedDRAModal}
          setModalState={this.setMedDRAModalState}
        />
        <SNOMEDModal
          show={this.state.showSNOMEDModal}
          setModalState={this.setSNOMEDModalState}
        />
        <Form horizontal onSubmit={this.validate}>
          {this.renderDataPackage()}

          <FormGroup>
            {this.renderEngine()}
            {this.renderStandard()}
          </FormGroup>

          <FormGroup>
            {this.renderSourceFormat()}
            {this.renderDelimiterAndQualifier()}
            {this.renderConfiguration()}
            {this.renderDefineStandard()}
          </FormGroup>

          {this.renderSourceData()}

          {this.renderDefineXML()}

          <FormGroup>
            {this.renderCDISCCT()}
            {this.renderDictionariesSection()}
          </FormGroup>

          {this.renderSupplementalSDTMCT()}

          {this.renderValidateButton()}
        </Form>
      </div>
    );
  }
}

Validator.propTypes = validatorPropTypes;

const mapValidatorStateToProps = ({ validator }) => ({
  ADaM_CT: validator.ADaM_CT,
  config: validator.config,
  defineStandard: validator.defineStandard,
  delimiter: validator.delimiter,
  engine: validator.engine,
  MedDRA: validator.MedDRA,
  NDF_RT: validator.NDF_RT,
  qualifier: validator.qualifier,
  SDTM_CT: validator.SDTM_CT,
  SEND_CT: validator.SEND_CT,
  SNOMED: validator.SNOMED,
  sourceFormatId: validator.sourceFormatId,
  standard: validator.standard,
  UNII: validator.UNII
});

const ConnectedValidator = connect(mapValidatorStateToProps)(Validator);

const rendererPropTypes = {
  electronAPI: PropTypes.object.isRequired,
  configs: PropTypes.object.isRequired,
  engines: PropTypes.array.isRequired,
  cdiscCTVersions: PropTypes.object.isRequired,
  medDRAVersions: PropTypes.array.isRequired,
  NDFRTVersions: PropTypes.array.isRequired,
  SNOMEDVersions: PropTypes.array.isRequired,
  UNIIVersions: PropTypes.array.isRequired,
  showAlert: PropTypes.bool,
  hideAlert: PropTypes.func.isRequired
};

const Renderer = props => (
  <ToolPage
    ipcRenderer={props.electronAPI.ipcRenderer}
    title="Validator"
    description={'check compliance with SDTM, SEND, ADaM, and Define.xml'}
    widgetTitle="Validate Data"
    widgetIcon="fa fa-check"
    modalTitle="Validating Data"
    openText="Open Report"
  >
    <ConnectedValidator {...props} />
  </ToolPage>
);

Renderer.propTypes = rendererPropTypes;

const mapStateToProps = ({ enterprise }) => ({
  environment: enterprise.environment
});

export default connect(mapStateToProps)(Renderer);
