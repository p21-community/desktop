/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ClipLoader } from 'halogenium';

import { loadKey } from '../../utils/localStorage';
import Validator from './Validator';

const propTypes = {
  electronAPI: PropTypes.object.isRequired
};

export default class ValidatorContainer extends Component {
  constructor(props) {
    super(props);
    this.ipcRenderer = this.props.electronAPI.ipcRenderer;

    this.state = {
      isReady: false
    };
  }

  componentDidMount() {
    this.ipcRenderer.on('configsLoaded', () => {
      this.pullDependencyMetadata();
    });
    this.ipcRenderer.on('configsUpdated', (event, remoteConfigsUpdated) => {
      this.pullDependencyMetadata(remoteConfigsUpdated);
    });
    if (loadKey('configs')) {
      this.pullDependencyMetadata();
    } else {
      this.ipcRenderer.send('loadConfigs');
    }
  }

  componentWillUnmount() {
    this.ipcRenderer.removeAllListeners('configsLoaded');
    this.ipcRenderer.removeAllListeners('configsUpdated');
    /*
      Removing these listeners so setState doesn't get called after the
      component has been unmounted
     */
    this.ipcRenderer.removeAllListeners('engines');
    this.ipcRenderer.removeAllListeners('dictionaries');
    this.ipcRenderer.removeAllListeners('configs');
  }

  hideAlert = () => {
    this.setState({ showAlert: false });
  };

  pullDependencyMetadata = async remoteConfigsUpdated => {
    const metadata = await Promise.all([
      new Promise(resolve => {
        this.ipcRenderer.once('engines', (event, engines) => {
          resolve(engines);
        });
        this.ipcRenderer.send('getEngines');
      }),
      new Promise(resolve => {
        this.ipcRenderer.once('dictionaries', (event, dictionaries) => {
          resolve(dictionaries);
        });
        this.ipcRenderer.send('getDictionaryVersions');
      }),
      new Promise(resolve => {
        this.ipcRenderer.once('configs', (event, configs) => {
          resolve(configs);
        });
        this.ipcRenderer.send('getConfigs');
      })
    ]);
    const nextState = {
      engines: metadata[0],
      ...metadata[1],
      configs: metadata[2]
    };
    nextState.isReady = ValidatorContainer.hasConfigs(nextState.configs)
        && ValidatorContainer.hasEngines(nextState.engines);
    if (remoteConfigsUpdated) {
      nextState.showAlert = remoteConfigsUpdated;
    }
    this.setState(nextState);
  };

  static hasConfigs(configs) {
    return configs && Object.keys(configs).length;
  }

  static hasEngines(engines) {
    return engines && engines.length;
  }

  render() {
    const {
      isReady,
      configs,
      engines,
      cdiscCTVersions,
      medDRAVersions,
      SNOMEDVersions,
      UNIIVersions,
      NDFRTVersions,
      showAlert
    } = this.state;
    if (!isReady) {
      return (
        <div className="spinner-container">
          <div className="spinner">
            <ClipLoader size="80px" color="#ccc" />
            Loading engines and configurations
          </div>
        </div>
      );
    }
    return (
      <Validator
        electronAPI={this.props.electronAPI}
        configs={configs}
        engines={engines}
        cdiscCTVersions={cdiscCTVersions}
        medDRAVersions={medDRAVersions}
        SNOMEDVersions={SNOMEDVersions}
        UNIIVersions={UNIIVersions}
        NDFRTVersions={NDFRTVersions}
        showAlert={showAlert}
        hideAlert={this.hideAlert}
      />
    );
  }
}

ValidatorContainer.propTypes = propTypes;
