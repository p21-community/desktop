/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../images/pinnacle21-community.png';
import Tool from './components/Tool';
import NotificationWidget from './components/NotificationWidget';

const propTypes = {
  notifications: PropTypes.array.isRequired
};

const Home = props => {
  return (
    <div>
      <div className="main-header">
        <h2>
          <img src={logo} alt="Pinnacle 21 Community" />
        </h2>
        <em>an open source toolkit for the CDISC professional</em>
      </div>
      <div className="main-content">
        <div className="row">
          <h5 className="col-sm-12">Where would you like to begin?</h5>
          <Tool
            name="Validator"
            description={`
              Check compliance with SDTM, SEND, ADaM, and Define.xml
            `}
            to="/validator"
            toolClass="validator"
            icon="fa fa-check"
          />
          <Tool
            name="Define.xml Generator"
            description={`
              Create compliant Define.xml 2.0 for SDTM, SEND, and ADaM datasets
            `}
            to="/generate/excel"
            toolClass="define"
            icon="fa fa-pencil"
          />
          <Tool
            name="Data Converter"
            description={`
              Convert data between SAS XPORT, Excel, CSV, and Dataset-XML
            `}
            to="/converter"
            toolClass="converter"
            icon="fa fa-cogs"
          />
          <Tool
            name="ClinicalTrials.gov Miner"
            description={`
            Aggregate clinical outcomes and site info across trials and therapeutic areas
            `}
            to="/miner"
            toolClass="clinical-trials"
            icon="fa fa-flask"
          />
        </div>
        <div className="row">
          <NotificationWidget notifications={props.notifications} />
        </div>
      </div>
    </div>
  );
};

Home.propTypes = propTypes;

export default Home;
