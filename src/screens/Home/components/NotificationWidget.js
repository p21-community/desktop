/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import LiveTimeFromNow from './LiveTimeFromNow';

const propTypes = {
  notifications: PropTypes.array.isRequired
};

const NotificationWidget = ({ notifications }) => {
  const renderNotifications = () => {
    if (notifications) {
      return notifications.map(({ message, link_text, link, pub_date }, i) => {
        return (
          <li key={i}>
            <i className="fa fa-rss activity-icon" />
            {message}{' '}
            <a rel="noopener noreferrer" target="_blank" href={link}>
              {link_text ? link_text : ''}
            </a>
            <div className="update-timestamp pull-right">
              {pub_date && <LiveTimeFromNow timestamp={pub_date} />}
            </div>
          </li>
        );
      });
    }
  };

  return (
    <div className="widget recent-updates">
      <div className="widget-header">
        <h3>
          <i className="fa fa-rss" /> Recent Updates
        </h3>
      </div>
      <div className="widget-content">
        <div className="activity">
          <ul className="list-unstyled activity-list">
            {renderNotifications()}
          </ul>
        </div>
      </div>
    </div>
  );
};

NotificationWidget.propTypes = propTypes;

export default NotificationWidget;
