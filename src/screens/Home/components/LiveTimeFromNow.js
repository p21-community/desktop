/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const propTypes = {
  timestamp: PropTypes.string.isRequired
};

export default class LiveTimeFromNow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      timeFromNow: moment(props.timestamp).fromNow()
    };
  }

  componentDidMount() {
    this.interval = setInterval(
      () => this.updateTimeFromNow(this.props.timestamp),
      60000
    );
  }

  componentWillReceiveProps({ timestamp }) {
    this.updateTimeFromNow(timestamp);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  updateTimeFromNow = timestamp => {
    this.setState({
      timeFromNow: moment(timestamp).fromNow()
    });
  };

  render() {
    return (
      <div className="update-timestamp pull-right">
        {this.state.timeFromNow}
      </div>
    );
  }
}

LiveTimeFromNow.propTypes = propTypes;
