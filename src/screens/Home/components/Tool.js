/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  toolClass: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
};

function Tool(props) {
  return (
    <div className={`col-sm-3 tool-block ${props.toolClass}`}>
      <Link to={props.to}>
        <span className="fa-stack fa-4x">
          <i className="fa fa-circle fa-stack-2x" />
          <i className={`${props.icon} fa-stack-1x fa-inverse`} />
        </span>
        <h5>{props.name}</h5>
      </Link>
      <p>{props.description}</p>
    </div>
  );
}

Tool.propTypes = propTypes;

export default Tool;
