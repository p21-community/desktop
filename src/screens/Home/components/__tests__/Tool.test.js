/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import Tool from '../Tool';

test('Tool renders correctly with its given props', () => {
  const toolRenderer = new ReactShallowRenderer();
  toolRenderer.render(
    <Tool
      name="name"
      description="description"
      to="to"
      toolClass="toolClass"
      icon="icon"
    />
  );
  expect(toolRenderer.getRenderOutput()).toMatchSnapshot();
});
