/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import axios from 'axios';
import { isEmpty } from 'lodash';

const handleRequestError = (err, logger) => {
  let errorMessage;
  if (err && err.response && err.response.data && err.response.data.error) {
    errorMessage = err.response.data.error;
  } else {
    if (err.message === 'Network Error') {
      errorMessage = 'Unable to establish an internet connection.';
    } else {
      errorMessage = 'An unexpected error has occurred.';
    }
  }
  if (logger) {
    logger.error(errorMessage, err);
  }
  return {
    success: false,
    error: errorMessage
  };
};

const tokensToRequestData = (tokens, extra) => {
  let req = {};
  if (tokens) {
    req.id = tokens.idToken;
    req.access = tokens.accessToken;
    req.refresh = tokens.refreshToken;
  }
  if (extra) {
    return { ...req, ...extra };
  }
  return isEmpty(req) ? undefined : req;
};

export default class PinnacleID {
  constructor(logger, appVersion) {
    if (!appVersion) {
      appVersion = '';
    }
    if (!logger) {
      logger = console;
    }
    const getPinnacleIdUrl = () => {
      if (
        appVersion.indexOf('snapshot') > -1 ||
        appVersion.indexOf('pre') > -1
      ) {
        return 'https://fdhlz68j05.execute-api.us-east-1.amazonaws.com/snapshot/';
      }
      return 'https://id.pinnacle21.com/';
    };
    this.logger = logger;
    this.request = axios.create({
      baseURL: getPinnacleIdUrl(),
      headers: { 'Content-Type': 'application/json' }
    });
  }

  _genericPinnacleIdRequest = async (route, requestData, logger) => {
    try {
      const { data } = await this.request.post(route, requestData);
      return {
        success: true,
        data
      };
    } catch (e) {
      return handleRequestError(e, logger);
    }
  };

  _datalessPinnacleIdRequest = async (route, requestData, logger) => {
    try {
      const { status } = await this.request.post(route, requestData);
      return {
        success: status === 200
      };
    } catch (e) {
      return handleRequestError(e, logger);
    }
  };

  login = async requestData =>
    this._genericPinnacleIdRequest('/login', requestData, this.logger);

  register = async requestData =>
    this._genericPinnacleIdRequest('/register', requestData, this.logger);

  resend = async requestData =>
    this._genericPinnacleIdRequest('/resend', requestData, this.logger);

  refresh = async tokens =>
    this._genericPinnacleIdRequest(
      '/refresh',
      tokensToRequestData(tokens),
      this.logger
    );

  sendForgotPassword = async email =>
    this._genericPinnacleIdRequest('/password/forgot', { email }, this.logger);

  update = async (tokens, updatedAttributes) =>
    this._genericPinnacleIdRequest(
      '/update',
      tokensToRequestData(tokens, updatedAttributes),
      this.logger
    );

  requestVerification = async (tokens, attribute) =>
    this._genericPinnacleIdRequest(
      '/verify/request',
      tokensToRequestData(tokens, { attribute }),
      this.logger
    );

  generate = async tokens =>
    this._genericPinnacleIdRequest(
      '/generate',
      tokensToRequestData(tokens),
      this.logger
    );

  validate = async tokens =>
    this._genericPinnacleIdRequest(
      '/validate',
      tokensToRequestData(tokens),
      this.logger
    );

  assign = async (tokens, identityId) =>
    this._datalessPinnacleIdRequest(
      '/assign',
      tokensToRequestData(tokens, { identityId }),
      this.logger
    );

  confirmForgotPassword = async requestData =>
    this._datalessPinnacleIdRequest(
      '/password/confirm',
      requestData,
      this.logger
    );

  confirmRegistration = async requestData =>
    this._datalessPinnacleIdRequest(
      '/register/confirm',
      requestData,
      this.logger
    );

  changePassword = async (
    tokens,
    password,
    newPassword,
    confirmedNewPassword
  ) =>
    this._datalessPinnacleIdRequest(
      '/password',
      tokensToRequestData(
        tokens,
        {
          password,
          newPassword,
          confirmedNewPassword
        },
        this.logger
      )
    );

  verify = async (tokens, attribute, code) =>
    this._datalessPinnacleIdRequest(
      '/verify',
      tokensToRequestData(tokens, { attribute, code }),
      this.logger
    );
}
