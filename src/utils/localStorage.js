/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { ipcRenderer } = window;
// ---------Local Storage save/load functions---------
export const loadKey = key => {
  try {
    const serializedValue = localStorage.getItem(key);
    if (serializedValue === null) {
      return undefined;
    }
    return JSON.parse(serializedValue);
  } catch (err) {
    return undefined;
  }
};

export const saveKey = (key, value) => {
  try {
    const serializedValue = JSON.stringify(value);
    localStorage.setItem(key, serializedValue);
  } catch (err) {
    // Ignore writing
  }
};

// Send configs json from local storage
ipcRenderer.send('configs', loadKey('configs'));

ipcRenderer.on('get-from-local-storage', (event, key) => {
  ipcRenderer.send(key, loadKey(key));
});

ipcRenderer.on('save-to-local-storage', (event, key, value) => {
  saveKey(key, value);
  ipcRenderer.send(`saved-${key}`);
});

// ---------Redux State---------
export const loadState = () => loadKey('state');
export const saveState = state => {
  saveKey('state', state);
};

// ---------App Preferences---------
export const defaultPreferences = () => {
  return {
    excelMessageLimit: 1000
  };
};
export const clearPinnacleId = () => {
  const loadedPreferences = loadPreferences();
  saveKey('preferences', {
    excelMessageLimit: loadedPreferences.excelMessageLimit
  });
};
export const loadPreferences = () => {
  const prefs = loadKey('preferences');
  if (!prefs) {
    return defaultPreferences();
  }
  if (prefs.excelMessageLimit == null) {
    return { ...prefs, excelMessageLimit: 1000 };
  }
  return prefs;
};
export const savePreferences = preferences => {
  const loadedPreferences = loadPreferences();

  saveKey('preferences', {
    ...(loadedPreferences || defaultPreferences()),
    ...preferences
  });
};

ipcRenderer.on('get-preferences', () => {
  const preferences = {
    ...loadPreferences(),
    userAgent: window.navigator.userAgent,
    anonymousId: loadKey('app-id')
  };
  ipcRenderer.send('get-preferences', preferences);
});
