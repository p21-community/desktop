/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

export default class Logger {
  constructor(ipcHandler) {
    this.logger = function(logType, message, data) {
      ipcHandler.send('log-' + logType, message, data);
    };
  }

  log = (message, data) => {
    console.log(message, data);
    this.logger('info', message, data);
  };

  error = (message, err) => {
    console.error(message, err);
    this.logger('error', message, err);
  };
}
