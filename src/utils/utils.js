/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import { trim, isEmpty, values, mapValues } from 'lodash';

function testForRegex(regex, str) {
  return regex.test(String(str));
}

function hasSpecialCharacters(str) {
  // eslint-disable-next-line
  const re = /[~`!#$@%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g;
  return testForRegex(re, str);
}

function hasLowerCaseCharacter(str) {
  // eslint-disable-next-line
  const re = /[a-z]/g;
  return testForRegex(re, str);
}

function hasUpperCaseCharacter(str) {
  // eslint-disable-next-line
  const re = /[A-Z]/g;
  return testForRegex(re, str);
}

export function hasNumber(str) {
  // eslint-disable-next-line
  const re = /[0-9]/g;
  return testForRegex(re, str);
}

export function validateEmail(email) {
  // eslint-disable-next-line
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return testForRegex(re, String(email).toLowerCase());
}

export function validatePassword(password) {
  if (!password || !password.length) {
    return;
  }
  let issues = [];
  if (password.length < 8) {
    const charsToGo = 8 - password.length;
    issues.push(
      ` ${charsToGo} more ${charsToGo === 1 ? 'character' : 'characters'}`
    );
  }
  if (!hasSpecialCharacters(password)) {
    issues.push(' at least one special character (!, @, #, etc.)');
  }
  if (!hasLowerCaseCharacter(password)) {
    issues.push(' at least one lowercase letter');
  }
  if (!hasUpperCaseCharacter(password)) {
    issues.push(' at least one uppercase letter');
  }
  if (!hasNumber(password)) {
    issues.push(' at least one number');
  }
  if (!issues.length) {
    return;
  }
  if (issues.length === 2) {
    return 'Your password needs' + issues.join(' and ') + '.';
  }
  if (issues.length > 2) {
    issues[issues.length - 1] = ' and ' + issues[issues.length - 1];
    return 'Your password needs' + issues.join(',') + '.';
  } else {
    return 'Your password needs' + issues.join(',') + '.';
  }
}

export const isBlank = str => {
  return !str || !trim(str).length;
};

export const isEmptyOrHasNullValues = obj => {
  if (!obj || isEmpty(obj)) {
    return true;
  }
  const nonNullValues = values(obj).filter(x => x !== null);
  return nonNullValues.length === 0;
};
export const trimValues = (obj, keysNotToTrim) => {
  return mapValues(obj, (value, key) => {
    if (
      typeof value !== 'string' ||
      (keysNotToTrim && keysNotToTrim.includes(key))
    ) {
      return value;
    }
    return trim(value);
  });
};
