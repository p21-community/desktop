/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';
import { shallow } from 'enzyme';
import App from '../App';
import { Converter, CreateSpec, GenerateDefine } from '../screens/index';

const electronAPI = {
  ipcRenderer: {
    send: jest.fn(),
    on: jest.fn()
  }
};

test('App renders correctly', () => {
  const appRenderer = new ReactShallowRenderer();

  appRenderer.render(<App electronAPI={electronAPI} />);
  expect(appRenderer.getRenderOutput()).toMatchSnapshot();
});

test('converterComponent returns a Converter component', () => {
  const appRenderer = shallow(<App electronAPI={electronAPI} />);

  expect(appRenderer.instance().converterComponent()).toEqual(
    <Converter electronAPI={electronAPI} />
  );
});

test('createSpecComponent returns a CreateSpec component', () => {
  const appRenderer = shallow(<App electronAPI={electronAPI} />);

  expect(appRenderer.instance().createSpecComponent()).toEqual(
    <CreateSpec ipcRenderer={electronAPI.ipcRenderer} />
  );
});

test('generateDefineComponent returns a GenerateDefine component', () => {
  const appRenderer = shallow(<App electronAPI={electronAPI} />);

  expect(appRenderer.instance().generateDefineComponent()).toEqual(
    <GenerateDefine ipcRenderer={electronAPI.ipcRenderer} />
  );
});
