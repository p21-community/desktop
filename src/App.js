/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Link,
  Route,
  withRouter
} from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import JWTDecode from 'jwt-decode';

import About from './components/About/About';
import PinnacleIDError from './components/PinnacleIDError/PinnacleIDError';
import Footer from './components/Footer/Footer';
import Preferences from './components/Preferences/Preferences';
import SetupWizard from './components/SetupWizard/SetupWizard';
import Sidebar from './components/Sidebar/Sidebar';
import UncaughtExceptionModal from './components/UncaughtExceptionModal';
import Alerts from './components/Alerts/Alerts';
import {
  ClinicalTrialsMiner,
  ConnectToEnterprise,
  Converter,
  CreateSpec,
  GenerateDefine,
  Home,
  ValidatorContainer
} from './screens/index';
import { setEnvironment } from './screens/ConnectToEnterprise/connectToEnterpriseRedux';
import logo from './images/pinnacle21-enterprise.png';
import {
  clearPinnacleId,
  loadKey,
  loadPreferences,
  saveKey,
  savePreferences
} from './utils/localStorage';
import PinnacleID from './utils/pinnacleIdRequests';
import Logger from './utils/logger';
import { launchFromPreferences } from './components/SetupWizard/setupWizardRedux';

const { ipcRenderer, dialog, defaultSession, app } = window;

const electronAPI = {
  ipcRenderer,
  dialog
};

const PATHS = {
  HOME: '/',
  VALIDATOR: '/validator',
  CREATESPEC: '/generate/excel',
  GENERATEDEFINE: '/generate/xml',
  CONVERTER: '/converter',
  MINER: '/miner',
  CONNECT: '/connect'
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      online: true,
      showPreferences: false,
      onlineCheckIntervalId: undefined,
      pinnacleIdRefreshIntervalId: undefined,
      isSidebarMinified: false,
      notifications: loadKey('notifications') || {
        bannerNotifications: [],
        standardNotifications: []
      },
      setupComplete: loadKey('setup-complete'),
      pinnacleIdError: false
    };
    this.logger = new Logger(ipcRenderer);
    this.pinnacleId = new PinnacleID(this.logger, app.getVersion());
  }

  componentDidMount() {
    let loggedIn = false;
    const domain =
      this.props.environment.name === 'local'
        ? 'localhost'
        : `${this.props.environment.name}.pinnacle21.net`;

    /*
     * Syncing "environment.loggedIn" state when the app starts with whether or
     * not there is an active user session incase the app crashed or the user
     * didn't log out before closing the app.
     */
    defaultSession.cookies.get({ domain }, (error, cookies) => {
      loggedIn =
        cookies.length &&
        cookies[0].value.split('&').some(val => {
          // Check for a session id
          return val.startsWith('sid=');
        });
      this.props.setLoggedInState(loggedIn);
    });

    /*
     * Set "environment.loggedIn" state to false when the P21 Enterprise session
     * expires.
     * TODO: Currently does not work when the webview isn't loaded.
     */
    defaultSession.cookies.on('changed', (event, cookie, cause) => {
      if (cookie.domain === 'localhost' && cause === 'expired-overwrite') {
        this.props.setLoggedInState(false);
      }
    });

    ipcRenderer.on('notifications', (event, notifications) => {
      saveKey('notifications', notifications);
      this.setState({ notifications });
    });

    ipcRenderer.send('getLatestNotifications');

    setInterval(() => ipcRenderer.send('getLatestNotifications'), 600000);

    ipcRenderer.on('get-user-login-status', async () => {
      let userIsValid = false;
      if (this.state.online) {
        const newTokens = await this.refreshPinnacleId(true);
        if (newTokens) {
          const res = await this.pinnacleId.validate(newTokens);
          if (res.success) {
            const { data } = res;
            userIsValid = !!data.userId;
          }
        }
      }
      ipcRenderer.send('user-login-status', userIsValid);
    });

    ipcRenderer.on('connectionStatus', (event, online) => {
      this.setState({ online });
    });
    ipcRenderer.send('getConnectionStatus');
    const onlineCheckIntervalId = setInterval(() => {
      ipcRenderer.send('getConnectionStatus');
    }, 60000);
    this.setState({ onlineCheckIntervalId });

    const pinnacleIdRefreshIntervalId = setInterval(() => {
      if (this.state.online) {
        this.refreshPinnacleId().then(() => undefined);
      }
    }, 15000 * 60);
    this.setState({ pinnacleIdRefreshIntervalId });

    ipcRenderer.on('open-preferences', () => {
      if (!this.props.showWizard) {
        this.setState({ showPreferences: true });
      }
    });
  }

  componentDidUpdate() {
    /*
     * Clear the app cookies when disconnecting from an environment
     * to log the user out. We only call this when disconnecting from the
     * environment completely because we don't want to clear out the CSRF token
     * when the user only wants to log out and possibly switch to a different
     * account. Clearing out the CSRF token would not allow the user to log back
     * in.
     */
    const { name, loggedIn } = this.props.environment;
    if (!name && !loggedIn) {
      defaultSession.clearStorageData({
        storages: ['cookies']
      });
    }
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    if (this.state.online !== nextState.online) {
      if (nextState.online) {
        this.refreshPinnacleId(true).then(() => undefined);
      }
      ipcRenderer.send('loadConfigs');
    }
  }

  componentWillUnmount() {
    ipcRenderer.removeAllListeners('configs');
    ipcRenderer.removeAllListeners('notifications');
    ipcRenderer.removeAllListeners('configsUpdated');
    clearInterval(this.state.onlineCheckIntervalId);
    clearInterval(this.state.pinnacleIdRefreshIntervalId);
  }

  connectComponent = () => {
    return <ConnectToEnterprise />;
  };

  converterComponent = () => {
    return <Converter electronAPI={electronAPI} />;
  };

  createSpecComponent = () => {
    return <CreateSpec ipcRenderer={ipcRenderer} />;
  };

  generateDefineComponent = () => {
    return <GenerateDefine ipcRenderer={ipcRenderer} />;
  };

  homeComponent = () => {
    return this.props.environment.loggedIn ? (
      this.connectComponent()
    ) : (
      <Home notifications={this.state.notifications.standardNotifications} />
    );
  };

  validatorComponent = () => <ValidatorContainer electronAPI={electronAPI} />;

  minerComponent = () => <ClinicalTrialsMiner electronAPI={electronAPI} />;

  onSidebarMinifiedClick = () => {
    this.setState({ isSidebarMinified: !this.state.isSidebarMinified });
  };

  refreshPinnacleId = async forceRefresh => {
    const prefs = loadPreferences();
    if (prefs && prefs.identity) {
      const tokens = prefs.identity;
      const decodedJwt = JWTDecode(tokens.accessToken);
      const currentTime = Date.now().valueOf() / 1000;
      if (decodedJwt.exp < currentTime || forceRefresh) {
        const res = await this.pinnacleId.refresh(tokens);
        if (res.success) {
          const { data } = res;
          const newTokens = { ...tokens, ...data };
          savePreferences({ identity: newTokens });
          return newTokens;
        } else if (res.error && res.error.indexOf('Not authorized') > -1) {
          // Their Pinnacle ID is out of whack, give them the option to log back in
          this.setState({ pinnacleIdError: true });
        }
      }
    }
  };

  hidePreferences = () => this.setState({ showPreferences: false });

  forcePinnacleIdRefresh = () => this.refreshPinnacleId(true);

  onWizardFinish = () => {
    this.setState({ setupComplete: true });
    saveKey('setup-complete', true);
  };

  clearPinnacleIdError = () => {
    this.setState({ pinnacleIdError: false });
  };

  goToLogin = () => {
    this.clearPinnacleIdError();
    clearPinnacleId();
    this.props.launchLogin();
  };

  render() {
    if (this.props.showWizard || !this.state.setupComplete) {
      return <SetupWizard onFinish={this.onWizardFinish} />;
    } else {
      const { loggedIn } = this.props.environment;
      return (
        <Router>
          <div className={`${loggedIn ? 'enterprise' : ''}`}>
            <About ipcRenderer={ipcRenderer} />
            <PinnacleIDError
              show={this.state.pinnacleIdError}
              close={this.clearPinnacleIdError}
              goToLogin={this.goToLogin}
            />
            <Preferences
              pinnacleId={this.pinnacleId}
              show={this.state.showPreferences}
              hide={this.hidePreferences}
              refreshPinnacleId={this.forcePinnacleIdRefresh}
              launchLogin={this.props.launchLogin}
              logger={this.logger}
            />
            <div className="header">
              <Link to={PATHS.HOME}>
                <img src={logo} alt="Pinnacle 21 Enterprise" />
              </Link>
            </div>
            <div className={`privacy-mode ${loggedIn ? '' : 'hidden'}`}>
              <Button
                bsStyle="danger"
                className="dropdown-toggle"
                data-toggle="dropdown"
              >
                <i className="fa fa-user-secret" /> Privacy Mode
              </Button>
              <div className="dropdown-menu dropdown-menu-right">
                <p>
                  <i className="fa fa-user-secret" /> You are using Enterprise
                  locally in <strong>Privacy Mode</strong>.
                </p>
                <p>
                  <i className="fa fa-check" /> That means you can run your
                  validations privately.
                </p>
                <p>
                  <i className="fa fa-eye-slash" /> No one else will see your
                  work until you are ready to publish.
                </p>
              </div>
            </div>
            <Sidebar
              notifications={this.state.notifications.bannerNotifications}
              environment={this.props.environment}
              minified={this.state.isSidebarMinified}
              onClick={this.onSidebarMinifiedClick}
              onDisconnect={this.props.onDisconnect}
              onCancel={this.props.onCancel}
            />
            <Alerts
              online={this.state.online}
              ipcRenderer={ipcRenderer}
              standardNotifications={
                this.state.notifications.standardNotifications
              }
            />
            <ContentWrapperWithRouter
              loggedIn={loggedIn}
              isSidebarMinified={this.state.isSidebarMinified}
            >
              <Route exact path={PATHS.HOME} component={this.homeComponent} />
              <Route
                path={PATHS.VALIDATOR}
                component={this.validatorComponent}
              />
              <Route
                path={PATHS.CREATESPEC}
                component={this.createSpecComponent}
              />
              <Route
                path={PATHS.GENERATEDEFINE}
                component={this.generateDefineComponent}
              />
              <Route
                path={PATHS.CONVERTER}
                component={this.converterComponent}
              />
              <Route path={PATHS.MINER} component={this.minerComponent} />
              <Route path={PATHS.CONNECT} component={this.connectComponent} />
            </ContentWrapperWithRouter>
            <Footer />
          </div>
        </Router>
      );
    }
  }
}

const mapStateToProps = ({ enterprise, setup }) => {
  return {
    environment: enterprise.environment,
    loggedIn: enterprise.loggedIn,
    showWizard: setup.cameFromPreferences
  };
};
const mapDispatchToProps = dispatch => ({
  setLoggedInState: loggedIn => {
    dispatch(setEnvironment({ loggedIn }));
  },
  onCancel: () => {
    dispatch(setEnvironment({ name: '' }));
  },
  onDisconnect: () => {
    ipcRenderer.send('unmaximizeWindow');
    dispatch(setEnvironment({ name: '', loggedIn: false }));
  },
  launchLogin: () => dispatch(launchFromPreferences())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

class ContentWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentScreen: this.getCurrentScreen(this.props.location.pathname)
    };
  }

  componentDidMount() {
    this.props.history.push('/');
    ipcRenderer.on('redirect', (event, location) => {
      this.props.history.push(location);
    });
    ipcRenderer.on('re-renderApp', () => {
      this.forceUpdate();
    });
    ipcRenderer.on('set-error', (event, error, errorInfo) => {
      this.setState({ error, errorInfo });
    });

    ipcRenderer.on('get-exception-status', () => {
      ipcRenderer.send('exception-status', this.state.error != null);
    });
  }

  componentDidUpdate(prevProps) {
    const currentPath = this.props.location.pathname;
    if (prevProps.location.pathname === currentPath) {
      return;
    }

    const currentScreen = this.getCurrentScreen(currentPath);
    if (currentScreen) {
      this.setState({ currentScreen });
    }
  }

  componentWillUnmount() {
    ipcRenderer.removeAllListeners('configs');
    ipcRenderer.removeAllListeners('redirect');
    ipcRenderer.removeAllListeners('re-renderApp');
    ipcRenderer.removeAllListeners('setError');
  }

  getCurrentScreen = path => {
    switch (path) {
      case PATHS.HOME:
        return 'home';
      case PATHS.VALIDATOR:
        return 'validator';
      case PATHS.CREATESPEC:
        return 'create-spec';
      case PATHS.GENERATEDEFINE:
        return 'generate-define';
      case PATHS.CONVERTER:
        return 'converter';
      case PATHS.MINER:
        return 'miner';
      case PATHS.CONNECT:
        return 'connect';
      default:
        return undefined;
    }
  };

  errorSetter = (error, errorInfo) => {
    this.setState({ error, errorInfo });
  };

  render() {
    return (
      <div
        className={`content-wrapper ${this.state.currentScreen} ${
          this.props.isSidebarMinified ? 'expanded' : ''
        }`}
        style={{ height: window.innerHeight - (this.props.loggedIn ? 90 : 38) }}
      >
        {this.state.error ? (
          <UncaughtExceptionModal
            show={true}
            error={this.state.error}
            meta={this.state.errorInfo}
            ipcRenderer={ipcRenderer}
          />
        ) : (
          <AppErrorBoundary
            errorSetter={this.errorSetter}
            ipcRenderer={ipcRenderer}
          >
            {this.props.children}
          </AppErrorBoundary>
        )}
      </div>
    );
  }
}

class AppErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      errorInfo: null
    };
  }

  componentDidCatch(error, errorInfo) {
    this.props.errorSetter(error, errorInfo);
    this.setState({ error, errorInfo });
  }

  render() {
    return this.state.error ? (
      <UncaughtExceptionModal
        show={true}
        error={this.state.error}
        meta={this.state.errorInfo}
        ipcRenderer={this.props.ipcRenderer}
      />
    ) : (
      this.props.children
    );
  }
}

const ContentWrapperWithRouter = withRouter(ContentWrapper);
