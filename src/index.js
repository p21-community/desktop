/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

import './styles/font-awesome-4.7.0/css/font-awesome.css';
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';
import { ProgressDialog } from './components/AutoUpdate/ProgressDialog';
import store from './redux';
import { loadKey, loadPreferences } from './utils/localStorage';

const { ipcRenderer } = window;

window.addEventListener('error', event => {
  ipcRenderer.send('handle-unhandled-exception', {
    message: event.error.message,
    stack: event.error.stack
  });
});

ipcRenderer.on('getPreferences', () => {
  ipcRenderer.send('preferences', loadPreferences());
});

ipcRenderer.on('getAppId', () => {
  ipcRenderer.send('appId', loadKey('app-id'));
});

ReactDOM.render(
  window.location.search && window.location.search.includes('update=true') ? (
    <ProgressDialog />
  ) : (
    <Provider store={store}>
      <App />
    </Provider>
  ),
  document.getElementById('container')
);
