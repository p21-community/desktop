/*
 * Copyright © 2008-2019 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open Source Software License located at [http://www.pinnacle21.com/license] (the “License”).
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, and is distributed “AS IS,” “WITH ALL FAULTS,” and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the License for more details.
 *
 */

const { app, BrowserWindow, ipcMain, Menu, shell } = require('electron');
const electron = require('electron');
const { release, arch, platform } = require('os');
const fs = require('fs-extra');
const path = require('path');
const URL = require('url');
const AWS = require('aws-sdk');
const { debounce } = require('lodash');
const axios = require('axios');

const appPath = app.getAppPath();
const requireAsar = url => require(path.join(appPath, url));
const Settings = requireAsar('./lib/settings');
const checkInternetConnection = requireAsar('./lib/services/online-check');

const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
const isProd = Settings.isProduction();
const exportPath = Settings.getExportPath();
fs.mkdirpSync(exportPath);

const meta = { OS: `${platform()} ${arch()} ${release()}` };
let win = undefined;

// Do not allow multiple instances of community to opened at the same time.
// You cannot share local storage, so simply prevent it from being an issue.
if (!app.requestSingleInstanceLock()) {
  app.quit();
  return;
}
app.on('second-instance', (event, commandLine, workingDirectory) => {
  // Someone tried to run a second instance, we should focus our window.
  if (!win) {
    return;
  }
  if (win.isMinimized()) {
    win.restore();
  }
  win.focus();
});

const GenericEventEmitter = requireAsar('./lib/generic-event-emitter');
GenericEventEmitter.on('get-online-status', async () => {
  const isOnline = await checkInternetConnection();
  GenericEventEmitter.emit('online-status', isOnline);
});

// Services
const notifications = requireAsar('./lib/services/notifications');
const winston = requireAsar('./lib/services/logger');
function isLoggedIn(win) {
  return new Promise(resolve => {
    if (!win) {
      return resolve(false);
    }
    checkInternetConnection().then(isOnline => {
      if (win && isOnline) {
        ipcMain.once('user-login-status', (event, status) => {
          resolve(status);
        });
        win.webContents.send('get-user-login-status');
      } else {
        resolve(false);
      }
    });
  });
}

function getPreferences(win) {
  return new Promise((resolve) => {
    if (!win) {
      return resolve({});
    }
    ipcMain.once('get-preferences', (event, preferences) => {
      resolve(preferences);
    });
    win.webContents.send('get-preferences');
  });
}

const { Config } = requireAsar('./lib/services/config');
const { importDictionary } = requireAsar(
  './lib/services/dictionary/import-dictionary'
);
const { DuplicateDictionaryError } = requireAsar(
  './lib/services/dictionary/duplicate-dictionary-error'
);

const menuTemplate = requireAsar('./lib/menu-template');
const { UnknownExceptionRejectHandler, sendBugReport } = requireAsar(
  './lib/unknown-exception-rejection-handler'
);
UnknownExceptionRejectHandler(() => win, meta).listen();

const createWindow = () => {
  return new Promise(resolve => {
    const mainScreen = electron.screen.getPrimaryDisplay();
    let launchHeight = 740;
    if (mainScreen.size.height < launchHeight) {
      launchHeight = mainScreen.size.height;
    }
    win = new BrowserWindow({
      show: false,
      width: 1100,
      height: launchHeight,
      useContentSize: true,
      webPreferences: {
        webSecurity: false,
        nodeIntegration: false,
        preload: path.join(appPath, 'lib', 'preload.js')
      }
    });
    win.once('ready-to-show', () => {
      win.show();
      resolve();
    });

    win.loadURL(
      isProd
        ? URL.format({
            pathname: path.join(appPath, 'build', 'index.html'),
            protocol: 'file:',
            slashes: true
          })
        : 'http://localhost:3000'
    );

    // Set application menu
    Menu.setApplicationMenu(
      Menu.buildFromTemplate(menuTemplate(win, exportPath))
    );

    if (!isProd) {
      const {
        default: installExtension,
        REDUX_DEVTOOLS
      } = require('electron-devtools-installer');

      installExtension(REDUX_DEVTOOLS)
        .then(() => win.webContents.openDevTools())
        .catch(err => console.log('An error occurred: ', err));
    }

    win.webContents.on('new-window', (e, url) => {
      e.preventDefault();
      shell.openExternal(url);
    });

    win.on(
      'resize',
      debounce(() => {
        if (win) {
          win.webContents.send('re-renderApp');
        }
      }, 100)
    );

    win.on('page-title-updated', event => {
      event.preventDefault();
    });

    win.on('closed', () => {
      win = null;
    });
  });
};

const createProgressDialog = () => {
  const window = new BrowserWindow({
    width: 500,
    height: 175,
    resizable: false,
    minimizable: false,
    closable: false,
    title: 'Pinnacle 21 Community Update',
    useContentSize: false,
    backgroundColor: '#F0F0F0',
    webPreferences: {
      webSecurity: false,
      nodeIntegration: false,
      preload: path.join(appPath, 'lib', 'preload.js')
    }
  });
  Menu.setApplicationMenu(null);
  window.on('page-title-updated', event => {
    event.preventDefault();
  });
  const ProgressDialog = requireAsar('./lib/progress-dialog');
  const dialog = new ProgressDialog(window);
  window.loadURL(
    isProd
      ? URL.format({
          pathname: path.join(appPath, 'build', 'index.html'),
          protocol: 'file:',
          search: 'update=true',
          slashes: true
        })
      : 'http://localhost:3000?update=true'
  );
  return dialog;
};

const CLI = requireAsar('./lib/services/cli/CLI');
const Converter = require(path.join(CLI.PATH.JOBS, 'Converter'));
const GenerateDefine = require(path.join(CLI.PATH.JOBS, 'GenerateDefine'));
const CreateSpec = require(path.join(CLI.PATH.JOBS, 'CreateSpec'));
const Validator = require(path.join(CLI.PATH.JOBS, 'Validator'));
const Miner = require(path.join(CLI.PATH.JOBS, 'Miner'));

const initializeListeners = config => {
  Converter(CLI);
  GenerateDefine(CLI);
  Validator(CLI);
  Miner(CLI);
  CreateSpec(CLI);

  ipcMain.removeAllListeners('updateConfigs');
  ipcMain.on('updateConfigs', async () => {
    const loggedIn = await isLoggedIn(win);
    const preferences = await getPreferences(win);
    const jsonUpdated = await config.fetchConfig(loggedIn, preferences);
    if (win) {
      win.webContents.send('configsUpdated', jsonUpdated);
    }
  });

  ipcMain.removeAllListeners('getConfigs');
  ipcMain.on('getConfigs', event => {
    if (!event.sender.isDestroyed()) {
      event.sender.send('configs', config.formattedConfigs);
    }
  });

  ipcMain.removeAllListeners('getEngines');
  ipcMain.on('getEngines', event => {
    if (!event.sender.isDestroyed()) {
      event.sender.send('engines', config.engines);
    }
  });

  ipcMain.removeAllListeners('getDictionaryVersions');
  ipcMain.on('getDictionaryVersions', event => {
    if (!event.sender.isDestroyed()) {
      event.sender.send('dictionaries', config.dictionaries);
    }
  });

  ipcMain.removeAllListeners('getUniqueConfigs');
  ipcMain.on('getUniqueConfigs', event => {
    if (!event.sender.isDestroyed()) {
      event.sender.send('uniqueConfigs', config.uniqueConfigs);
    }
  });

  ipcMain.removeAllListeners('importMedDRA');
  ipcMain.on('importMedDRA', (event, version, medDRAPath, overwrite) => {
    const medDRADirectory = path.join(
      CLI.PATH.CONFIGS,
      'data',
      'MedDRA',
      version
    );
    importDictionary(medDRAPath, medDRADirectory, 'MEDDRA', overwrite)
      .then(async () => {
        const loggedIn = await isLoggedIn(win);
        const preferences = await getPreferences(win);
        const jsonUpdated = await config.fetchConfig(loggedIn, preferences);
        if (!event.sender.isDestroyed()) {
          event.sender.send('importedMedDRASuccessfully', version);
        }
        if (win) {
          win.webContents.send('configsUpdated', jsonUpdated);
        }
      })
      .catch(err => {
        if (err instanceof DuplicateDictionaryError) {
          if (!event.sender.isDestroyed()) {
            event.sender.send('foundDuplicateMedDRA', version);
          }
        } else {
          if (!event.sender.isDestroyed()) {
            event.sender.send('permissionErrorMedDRA', version);
          }
          winston.error(err.toString());
        }
      });
  });

  ipcMain.removeAllListeners('importSNOMED');
  ipcMain.on('importSNOMED', (event, version, SNOMEDPath, overwrite) => {
    const SNOMEDDirectory = path.join(
      CLI.PATH.CONFIGS,
      'data',
      'SNOMED',
      version
    );
    importDictionary(SNOMEDPath, SNOMEDDirectory, 'SNOMED', overwrite)
      .then(async () => {
        const loggedIn = await isLoggedIn(win);
        const preferences = await getPreferences(win);
        const jsonUpdated = await config.fetchConfig(loggedIn, preferences);
        if (!event.sender.isDestroyed()) {
          event.sender.send('importedSNOMEDSuccessfully', version);
        }
        if (win) {
          win.webContents.send('configsUpdated', jsonUpdated);
        }
      })
      .catch(err => {
        if (err instanceof DuplicateDictionaryError) {
          if (!event.sender.isDestroyed()) {
            event.sender.send('foundDuplicateSNOMED', version);
          }
        } else {
          if (!event.sender.isDestroyed()) {
            event.sender.send('permissionErrorSNOMED', version);
          }
          winston.error(err.toString());
        }
      });
  });
};

app.on('ready', async () => {
  winston.info('App starting...');

  async function autoUpdate() {
    // Only check for updates when in prod mode
    if (!isProd) {
      return;
    }
    const isOnline = await checkInternetConnection();
    if (!isOnline) {
      return;
    }
    const { autoUpdater } = require('electron-updater');
    let progressBar = null;

    autoUpdater.logger = winston;
    autoUpdater.autoDownload = true;
    autoUpdater.autoInstallOnAppQuit = false;
    autoUpdater.on('update-available', () => {
      winston.info('An update was found. Downloading...');
      progressBar = createProgressDialog();
      progressBar.summary =
        'Pinnacle 21 is installing your updates and will start in a few moments...';
      progressBar.detail = `Downloaded: 0%`;
    });
    let lastProgressTimestamp = new Date();
    autoUpdater.on('download-progress', ({ percent }) => {
      const percentInt = Math.trunc(percent);
      progressBar.progress = percentInt;
      progressBar.detail = `Downloaded: ${percentInt}%`;
      if (percent >= 100) {
        progressBar.isComplete = true;
      }
      lastProgressTimestamp = new Date();
    });
    let intervalId = null;
    const downloadComplete = new Promise((resolve, reject) => {
      autoUpdater.on('update-not-available', () => {
        winston.info('No update is available.');
        resolve(false);
      });
      autoUpdater.on('update-downloaded', () => {
        winston.info('Update download complete.');
        // In case download progress never fires 100%, just update the progress bar here.
        // We want to include it in the progress handler, as well, since there's a
        // delay between when 100% is reached and 'update-downlaoded' is fired.
        progressBar.isComplete = true;
        progressBar.progress = 100;
        resolve(true);
      });
      autoUpdater.on('error', error => {
        winston.error(error);
        reject(error);
      });
      // We need to cancel the update if too much time has elapsed without progress.
      // Every few seconds, check if the user is offline. If they're offline and no
      // progress has been made in a while, fail out the update.
      intervalId = setInterval(async () => {
        const isOnline = await checkInternetConnection();
        if (intervalId == null) {
          return;
        }
        const offlineDuration = new Date() - lastProgressTimestamp;
        if (
          (!isOnline && offlineDuration > 60000) ||
          offlineDuration > 120000
        ) {
          reject(new Error('Network connection interrupted.'));
        }
      }, 15000);
    });

    winston.info('Checking if update is available...');
    try {
      await autoUpdater.checkForUpdates();
      const isInstallPending = await downloadComplete;
      // Clear any handlers so errors don't occur after the auto-update completes
      autoUpdater.removeAllListeners('download-progress');
      autoUpdater.removeAllListeners('update-not-available');
      autoUpdater.removeAllListeners('update-downloaded');
      autoUpdater.removeAllListeners('error');
      if (intervalId != null) {
        clearInterval(intervalId);
        intervalId = null;
      }
      if (isInstallPending) {
        winston.info('Restarting to perform install...');
        app.removeAllListeners('window-all-closed'); // Prevent handlers from preventing shutdown
        autoUpdater.quitAndInstall();
        return true;
      }
      return false;
    } catch (ex) {
      winston.error(ex);
      if (progressBar) {
        progressBar.isFailed = true;
        progressBar.progress = 100;
        progressBar.detail =
          'An error occurred while downloading the update...';
        // If an error occurs, keep the display up long enough that the user can read the error message.
        await new Promise(resolve => setTimeout(resolve, 5000));
      }
      return false;
    } finally {
      if (progressBar) {
        winston.info('Closing the progress bar window.');
        progressBar.close();
      }
    }
  }

  try {
    if (await autoUpdate()) {
      // The app is restarting to install the update. Don't proceed.
      return;
    }
  } catch (ex) {
    winston.error(ex);
  }

  // Only register the window-all-closed after closing the auto-update progress bar.
  // Otherwise, this event will fire and the application will close unexpectedly.
  app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });

  await createWindow();
  const configJson =
    (win &&
      (await new Promise(resolve => {
        ipcMain.once('configs', (event, configsJson) => {
          resolve(configsJson);
        });
        win.send('get-from-local-storage', 'configs');
      }))) ||
    {};
  const config = new Config(CLI, win, configJson, winston);
  const loadConfigs = async () => {
    try {
      initializeListeners(config);
      const loggedIn = await isLoggedIn(win);
      const preferences = await getPreferences(win);
      const jsonUpdated = await config.fetchConfig(loggedIn, preferences);
      if (win) {
        if (jsonUpdated) {
          win.webContents.send('configsUpdated', true);
        } else {
          win.webContents.send('configsLoaded', false);
        }
      }
    } catch (err) {
      winston.error(err.toString());
    }
  };
  await loadConfigs();

  (async () => {
    if (win) {
      win.webContents.send('notifications', await notifications(s3));
    }
  })();

  // Pull new config.json every hour
  setInterval(async () => {
    const loggedIn = await isLoggedIn(win);
    const preferences = await getPreferences(win);
    if (loggedIn) {
      if (await config.fetchConfig(loggedIn, preferences)) {
        if (win) {
          win.webContents.send('configsUpdated', true);
        }
      }
    }
  }, 3600000);

  // Pull the changelog every hour
  const downloadAndWriteChangelog = async () => {
    try {
      const response = await axios.get(Settings.getCdnBaseUrl() + '/engine/configs/CHANGELOG.txt');
      if (response.status === 200) {
        await fs.writeFile(
          path.join(Settings.getExportPath(), 'configs', 'CHANGELOG.txt'),
          response.data
        );
      } else {
        winston.error(`Failed to download changelog, status: ${response.status}`);
      }
    } catch (err) {
      winston.error(err);
    }
  };
  downloadAndWriteChangelog().then(() => setInterval(async () => await downloadAndWriteChangelog(), 3600000));

  ipcMain.on('getConnectionStatus', async event => {
    const isOnline = await checkInternetConnection();
    if (!event.sender.isDestroyed()) {
      event.sender.send('connectionStatus', isOnline);
    }
  });

  ipcMain.on('open', (event, path) => {
    shell.openItem(path);
  });

  ipcMain.on('maximizeWindow', () => {
    win.maximize();
  });

  ipcMain.on('unmaximizeWindow', () => {
    win.unmaximize();
  });

  ipcMain.on('loadConfigs', async event => {
    try {
      await loadConfigs();
    } catch (e) {
      winston.error('Error while loading configs:', e);
    }
  });
});

ipcMain.on('log-error', (event, msg, err) => {
  winston.error(msg, err);
});

ipcMain.on('log-info', (event, msg, data) => {
  winston.info(msg, data);
});

ipcMain.on('restart-app', () => {
  app.relaunch();
  app.exit();
});

ipcMain.on('handle-unhandled-exception', (event, exception) => {
  const checkIfExceptionIsBeingHandled = new Promise(resolve => {
    ipcMain.once('exception-status', (event, status) => {
      resolve(status);
    });
    win.send('get-exception-status');
  });
  checkIfExceptionIsBeingHandled.then(beingHandled => {
    if (!beingHandled) {
      throw exception;
    }
  });
});

ipcMain.on('bug-report', (event, exception, message, meta) => {
  sendBugReport(exception, message, meta);
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});
